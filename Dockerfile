FROM  docker-registry.booking.com/jenkins-built/centos-7:latest

MAINTAINER Antonio Rabena <antonio.rabena@booking.com>

ENV NODE_ENV="production" \
    HTTP_PROXY="http://webproxy:3128/" \
    HTTPS_PROXY="http://webproxy:3128/" \
    http_proxy="http://webproxy:3128/" \
    https_proxy="http://webproxy:3128/" \
    no_proxy="localhost,127.0.0.1,localaddress,.booking.com"

RUN yum install -y nodejs nginx
ADD etc/  /etc

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Bundle app source
ADD app/ /usr/src/app
RUN npm install

COPY mod/requestUtil.js /usr/src/app/node_modules/node-line-bot-api/lib/utils
COPY startup.sh /root

CMD ["/bin/bash", "/root/startup.sh"]
