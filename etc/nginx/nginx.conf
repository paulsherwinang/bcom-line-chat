# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user  nginx;
worker_processes  1;
daemon off;

error_log  /var/log/nginx/error.log;

pid        /run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent $request_time/$upstream_response_time '
                      '"$http_referer" "$http_user_agent" $gzip_ratio $http_f5trackerid';
    log_not_found  off;


    access_log  /var/log/nginx/access.log  main;
    error_log  /var/log/nginx/error.log  warn;

    sendfile        on;
    tcp_nopush  on;
    tcp_nodelay  off;
    keepalive_timeout 300;
    server_tokens  off;

    # Allow bigger headers by default
    client_header_buffer_size  32k;
    large_client_header_buffers  4 32k;
    client_body_buffer_size  32k;

    # Get real IP address, note that the /8 is *intentional*
    real_ip_header  F5SourceIP;
    set_real_ip_from  10.0.0.0/8;

    #gzip  on;
    index   index.html index.htm;

    server {
        listen       80;

        access_log  /var/log/nginx/access.log;
        error_log  /var/log/nginx/error.log  notice;
        location /images/ {
            root /usr/src/app/;
            allow all;
        }
        location  / {
            allow 203.104.146.0/24;
            allow 10.0.0.0/8;
            deny all;
            proxy_set_header   X-Forwarded-For $remote_addr;
            proxy_set_header   Host $http_host;
            proxy_http_version 1.1;
            proxy_pass http://127.0.0.1:3000;
        }

    }
}
