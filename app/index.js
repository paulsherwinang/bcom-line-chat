'use strict';

const bot = require('./bot');
const constants = require('./constants');
const CONFIG = process.env.NODE_ENV === 'production' ? constants['PROD'] : constants['DEV'];

let PORT = CONFIG.CHATBOT_SERVER_PORT;

bot.listen(PORT, () => {
    console.log('LineBot listening on port %s', PORT);
});