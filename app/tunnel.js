'use strict';

const localtunnel = require('localtunnel');
const constants = require('./constants');
const bot = require('./bot');
const CONFIG = process.env.NODE_ENV === 'production' ? constants['PROD'] : constants['DEV'];

const PORT = CONFIG.CHATBOT_SERVER_PORT;

bot.listen(PORT, () => {
    localtunnel(PORT, {subdomain: 'linechatbot'},  function(err, tunnel){
        console.log('Access the bot API on the tunnel: %s', tunnel.url)
    });
});
