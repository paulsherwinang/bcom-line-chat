'use strict';

const line = require('node-line-bot-api');

module.exports  = (req, res, next) => {
    line.validator.validateSignature();
    next();
};