'use strict';

const _ = require('lodash');
const line = require('node-line-bot-api');
const constants = require('../constants');
const apiHelpers = require('../helpers/apiHelpers');
const debug = require('debug')('linebot:validateAccessToken');

let tokenRes = {};

const CONFIG = process.env.NODE_ENV === "production" ? constants['PROD'] : constants['DEV'];

module.exports = (req, res, next) => {
    req.visitor.set("userId", req.body.events[0].source.userId);

    res.status(200).end();

    debug('==============================================================================');

    if(!_.isEmpty(tokenRes)) req.tokenRes = tokenRes;

    let isExpiredToken = _.get(req, 'tokenRes.expires_in') < new Date().getTime();
    let isEmptyToken = !_.has(req, 'tokenRes');

    if(!isExpiredToken && !isEmptyToken) return next();

    debug('[PENDING] Getting new accesstoken / Channel ID: %s, Channel Secret: %s', CONFIG.CHANNEL_ID, CONFIG.CHANNEL_SECRET);

    let promise = apiHelpers.getNewAccessToken(CONFIG.CHANNEL_ID, CONFIG.CHANNEL_SECRET);

    promise.then((response) => {
        debug('[SUCCESS] Access Token API call SUCCESS: %j', response);
        req.tokenRes = tokenRes = response;

        line.init({ accessToken: response.access_token, channelSecret: CONFIG.CHANNEL_SECRET });

        next();
    }, (err) => {
        console.log('testres', err)

        debug('[FAILED] Access Token API call FAILED: %O', err);
    });
};