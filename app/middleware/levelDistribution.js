'use strict';

module.exports = (tree) => {
    return (req, res, next, storage) => {
        let userId = req.body.events[0].source.userId;
        let userData = storage.getUserData(userId);

        let event = req.body.events[0];
        let userBranch = tree[userData.conversationLevel];

        if (userBranch.isValidResponse(req, res, userData)) {
            userBranch.onValidResponseAction(event, storage, userId, req, res);
        } else {
            userBranch.onInvalidResponseAction(event, storage, userId, req, res);
        }
    }
};