'use strict';

// Middleware that handles which branch it should go

const TRACKS = require('../constants').TRACKS;
const debug = require('debug')('branchDistribution');
const chatHelpers = require('../helpers/chatHelpers');
const gaMapping = require('../gaMapping');

const levelDistribution = require('../middleware/levelDistribution');
const hotelBookingTree = require('../chatLogic/hotelBookingLogic.tree');
const discoverLogicTree = require('../chatLogic/discoverLogic.tree');

module.exports = (req, res, next, storage) => {
    let event = req.body.events[0];
    let userData = storage.getUserData(event.source.userId);

    switch (userData.track) {
        case TRACKS.LOCATION_DATE:
            debug('in %s track', TRACKS.LOCATION_DATE);
            levelDistribution(hotelBookingTree)(req, res, next, storage);
            break;

        case TRACKS.PASSION:
            debug('in %s track', TRACKS.PASSION);
            levelDistribution(discoverLogicTree)(req, res, next, storage);
            break;

        default:
            debug('no track specified. send message with choices to initiate');
            req.visitor.event('chat trigger', 'start message', gaMapping.INTRO_MESSAGE).send();

            let lineMessages = chatHelpers.initialMessage.getMessage(req);
            chatHelpers.sendReplyMessage(req, res, lineMessages);
            break;
    }
};