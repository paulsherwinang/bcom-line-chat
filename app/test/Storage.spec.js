'use strict';

const sinon = require('sinon');
const expect = require('expect');
const Storage = require('../Storage');
const CONSTANTS = require('../constants');

let sandbox;

describe('Storage', () => {

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('setUser', () => {
        it('should create new user when not in store', () => {
            let storage = new Storage(), userId = 'xxx';
            storage.setUser(userId);
            expect(storage.store[userId]).toEqual(Storage.getEmptyUser());
        });

        it('should not override existing user data', () => {
            let data =  { hi: '1' }, storage = new Storage({ 'id': data }), userId = 'id';
            storage.setUser(userId);
            expect(storage.store[userId]).toEqual(data);
        });

        it('should have a TTL of 10minutes and emit event when ttl expires', () => {
            let clock = sinon.useFakeTimers();
            let userId = 'userId', storage = new Storage();
            sandbox.stub(storage.eventEmitter, 'emit');
            storage.setUser(userId);
            storage.setUserChoice(userId, 1, 'Choice1');

            let t = storage.ttl / 2;

            clock.tick(t);
            expect(storage.eventEmitter.emit.calledWith(CONSTANTS.EVENTS.STORAGE_TTL_EXPIRED, sinon.match.string)).toBe(false);
            clock.tick(t+1);
            expect(storage.eventEmitter.emit.calledWith(CONSTANTS.EVENTS.STORAGE_TTL_EXPIRED, sinon.match.string)).toBe(true);
            expect(storage.eventEmitter.emit.callCount).toBe(1);
            clock.restore();
        });

        it('should restart ttl when set has been made in the middle of time', () => {
            let clock = sinon.useFakeTimers();
            let userId = 'userId', storage = new Storage();
            sandbox.stub(storage.eventEmitter, 'emit');
            storage.setUser(userId);
            storage.setUserChoice(userId, 1, 'Choice1');

            let t = storage.ttl / 2;

            clock.tick(t);
            expect(storage.eventEmitter.emit.calledWith(CONSTANTS.EVENTS.STORAGE_TTL_EXPIRED, sinon.match.string)).toBe(false);
            storage.setUserChoice(userId, 2, 'Choice2');

            clock.tick(t+1);
            expect(storage.eventEmitter.emit.calledWith(CONSTANTS.EVENTS.STORAGE_TTL_EXPIRED, sinon.match.string)).toBe(false);

            clock.tick(t+2);
            expect(storage.eventEmitter.emit.calledWith(CONSTANTS.EVENTS.STORAGE_TTL_EXPIRED, sinon.match.string)).toBe(true);

            clock.restore();
        });
    });

    describe('getUserData', () => {
        it('should get user information in the store', () => {
            let data = { obj: 1 }, storage = new Storage({ 'id': data }), userId = 'id';
            let userData = storage.getUserData(userId);
            expect(userData).toEqual(data);
        });

        it('should return null when no id found in store', () => {
            let storage = new Storage({ 'id': { obj: 1 } }), userId = 'hi';
            let userData = storage.getUserData(userId);

            expect(userData).toBe(null);
        })
    });

    describe('removeUser', () => {
        it('should remove user data from the storage', () => {
            let storage = new Storage(),
                userId = 'userId';

            storage.setUser(userId);
            storage.setUserChoice(userId, 1, 'Test');
            expect(storage.getUserData(userId)).toNotEqual(Storage.getEmptyUser());
            storage.removeUser(userId);
            expect(storage.getUserData(userId)).toEqual(Storage.getEmptyUser());
        });
    });

    describe('setConversationLevel', () => {
        it('should set conversation level correctly', () => {
            let storage = new Storage(), userId = 'id';
            storage.setUser(userId);
            expect(storage.getUserData(userId).conversationLevel).toBe(0);
            storage.setConversationLevel(userId, 1);
            expect(storage.getUserData(userId).conversationLevel).toBe(1);
        });
    });

    describe('setUserChoice', () => {
        it('should set users choice[conversationLevel] in the store', () => {
            let storage = new Storage(), userId = 'id', level = 1, choice = 'Choice';
            storage.setUser(userId);
            storage.setUserChoice(userId, level, choice);
            expect(storage.getUserData(userId).choice[level]).toBe(choice);
        });
    });

    describe('setUserSuggestion', () => {
        it('should set suggestions in the store for a user', () => {
            let storage = new Storage(),
                userId = 'id',
                level = 1,
                suggestions = ['hello', 'its me'];

            storage.setUser(userId);
            storage.setUserSuggestion(userId, level, suggestions);
            expect(storage.getUserData(userId).suggestion[level]).toBeA(Array);
            expect(storage.getUserData(userId).suggestion[level].length).toBe(2);
        });
    });

    describe('setUserTrack', () => {
        it('should set the users chosen track', () => {
            let storage = new Storage(),
                userId = 'id',
                track = 'DISCOVER_TRACK';

            storage.setUser(userId);
            storage.setUserTrack(userId, track);

            expect(storage.getUserData(userId).track).toBe(track);
        });
    });

});