'use strict';

const expect = require('expect');
const request = require('supertest');
const bot = require('../../bot');
const nock = require('nock');

const constants = require('../../constants');
const CONFIG = process.env.NODE_ENV === "production" ? constants['PROD'] : constants['DEV'];
const ENDPOINT_API_BASE_URL = CONFIG.CHATBOT_SERVER_HOSTNAME+':'+CONFIG.CHATBOT_SERVER_PORT;

describe('POST /api/getImageByDestinationTag', () => {

    afterEach(() => {
        nock.cleanAll();
    });

    it('should extract image url correctly when dest_type=country', (done) => {
        let imageUrl = 'https://s-ec.bstatic.com/images/city/1680x560/837/83713.jpg';

        nock(ENDPOINT_API_BASE_URL)
            .post('/scraper/getDsfPage')
            .reply(200,
                `<div><section id="about" class="dsection">
                    <div class="header_full_image page_header_universal_search dsf_limit_header_height" style="background-image: url(${imageUrl});"></div>
                </section></div>`
            );

        request(bot)
            .post('/api/getImageByDestinationTag')
            .send({ dest_type: 'country', dest_id: 168 })
            .expect('Content-Type', /json/)
            .expect(200, { imageUrl: imageUrl }, done);

    });

    it('should extract image url correctly when dest_type=country and tags=xxx', (done) => {
        let imageUrl = 'https://s-ec.bstatic.com/data/xphoto/1872x1404/331/3318769.jpg';

        nock(ENDPOINT_API_BASE_URL)
            .post('/scraper/getDsfPage')
            .reply(200,
                `<div><div id="dsf_full_header"
                      class="page_header theme page_header_universal_search"
                      style="background-image: url(${imageUrl});">
                </div></div>`
            );

        request(bot)
            .post('/api/getImageByDestinationTag')
            .send({ dest_type: 'country', dest_id: '1', tags: 1 })
            .expect(200, { imageUrl: imageUrl }, done);
    });

    it('should extract image url correctly when dest_type=city', (done) => {
        let imageUrl = 'https://s-ec.bstatic.com/data/explorer_city/2400x1600/312/3125.jpg';

        nock(ENDPOINT_API_BASE_URL)
            .post('/scraper/getDsfPage')
            .reply(200,
                `<div><section id="about" class="dsf_section js-fixed-navbar-offsetz">
                    <div class="header_full_image page_header_universal_search dsf_limit_header_height" style="background-image: url(${imageUrl});">
                </section></div>`
            );

        request(bot)
            .post('/api/getImageByDestinationTag')
            .send({ dest_type: 'city', dest_id: 1 })
            .expect('Content-Type', /json/)
            .expect(200, { imageUrl: imageUrl }, done);

    });

    it('should extract image url correctly when dest_type=city and tags=xxx', (done) => {
        let imageUrl = 'https://s-ec.bstatic.com/data/xphoto/1872x1404/331/3318769.jpg';

        nock(ENDPOINT_API_BASE_URL)
            .post('/scraper/getDsfPage')
            .reply(200,
                `<div><section id="about" class="dsf_section js-fixed-navbar-offsetz">
                    <div class="header_full_image page_header_universal_search dsf_limit_header_height" style="background-image: url(${imageUrl});">
                </section></div>`
            );

        request(bot)
            .post('/api/getImageByDestinationTag')
            .send({ dest_type: 'city', dest_id: '1', tags: 1 })
            .expect(200, { imageUrl: imageUrl }, done);
    });

    it('should extract image url correctly when dest_type=region', (done) => {
        let imageUrl = 'https://s-ec.bstatic.com/data/xphoto/1872x1404/331/3318769.jpg';

        nock(ENDPOINT_API_BASE_URL)
            .post('/scraper/getDsfPage')
            .reply(200,
                `<div id="dsf_search_header" class="dsf_landing dsf_new_home_img" data-component="DSF:page-header">
                    <div id="classic_search" class="imagebg">
                        <div class=" dsf_searchbg  js-search-bg " style="background-image: url(${imageUrl});"></div>
                    </div>
                </div>`
            );

        request(bot)
            .post('/api/getImageByDestinationTag')
            .send({ dest_type: 'region', dest_id: '1' })
            .expect(200, { imageUrl: imageUrl }, done);

    });

    it('should extract image url correctly when dest_type=region and tags=xxx', (done) => {
        let imageUrl = 'https://s-ec.bstatic.com/data/xphoto/1872x1404/331/3318769.jpg';

        nock(ENDPOINT_API_BASE_URL)
            .post('/scraper/getDsfPage')
            .reply(200,
                `<div><div id="dsf_full_header"
                      class="page_header theme page_header_universal_search"
                      style="background-image: url(${imageUrl});">
                </div></div>`
            );

        request(bot)
            .post('/api/getImageByDestinationTag')
            .send({ dest_type: 'region', dest_id: '1', tags: 1 })
            .expect(200, { imageUrl: imageUrl }, done);
    });

    it('should return 404 if image isnt found', (done) => {
        nock(ENDPOINT_API_BASE_URL)
            .post('/scraper/getDsfPage')
            .reply(200,
                `<div></div>`
            );

        request(bot)
            .post('/api/getImageByDestinationTag')
            .send({ dest_type: 'region', dest_id: 'city', tags: 1 })
            .expect(200, { imageUrl: '' }, done);
    });
});