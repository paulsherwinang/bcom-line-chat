'use strict';

const expect = require('expect');
const request = require('supertest');
const bot = require('../../bot');
const nock = require('nock');

describe('POST /api/getHotelByPriceRange', () => {

    afterEach(() => {
        nock.cleanAll();
    });

    it('should respond with json filtered hotels', (done) => {

        nock('https://distribution-xml.booking.com')
            .get('/json/getHotelAvailabilityV2')
            .query(true)
            .reply(200, {
                    "other_currency": "THB",
                    "checkout": "2017-12-03",
                    "checkin": "2017-12-01",
                    "guest_groups": [{"guests": 2, "children": []}],
                    "city_ids": ["-73635"],
                    "hotels": [
                        {"review_score": "5.9", "price": "1800", hotel_url: "http://b.com?aid=notvalid"},
                        {"review_score": "4.9", "price": "600",  hotel_url: "http://b.com?aid=notvalid"},
                        {"review_score": "3.9", "price": "2500",  hotel_url: "http://b.com?aid=notvalid"},
                        {"review_score": "9.9", "price": "700",  hotel_url: "http://b.com?aid=notvalid"},
                        {"review_score": "4.9", "price": "800",  hotel_url: "http://b.com?aid=notvalid"},
                        {"review_score": "9.8", "price": "50",  hotel_url: "http://b.com?aid=notvalid"},
                        {"review_score": "6.9", "price": "100",  hotel_url: "http://b.com?aid=notvalid"},
                        {"review_score": "9.7", "price": "1800",  hotel_url: "http://b.com?aid=notvalid"},
                        {"review_score": "7.0", "price": "1",  hotel_url: "http://b.com?aid=notvalid"},
                        {"review_score": "5.9", "price": "2500",  hotel_url: "http://b.com?aid=notvalid"}
                    ]
                }
            );

        let totalMax = 1000;
        let totalMin = 20;

        request(bot)
            .post('/api/getHotelByPriceRange')
            .send({
                checkin: "2017-12-01",
                checkout: "2017-12-03",
                cityId: '-73635', currencyCode: 'THB', totalMin: totalMin, totalMax: totalMax, limit: 5
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                expect(res.body.hotels.length).toBe(5);

                res.body.hotels.forEach((hotel, i) => {
                    expect(hotel.hotel_url).toNotContain('aid=notvalid');
                    expect(parseFloat(hotel.price) < totalMax).toBe(true, 'hotel['+i+'] price is beyond totalMax');
                    expect(totalMin < parseFloat(hotel.price)).toBe(true, 'hotel['+i+'] price is under totalMin');
                });

                done();
            });
    });

    it('should respond with json filtered when total max is not passed in', (done) => {

        let aid = 'xxxxxx';

        nock('https://distribution-xml.booking.com')
            .get('/json/getHotelAvailabilityV2')
            .query(true)
            .reply(200, {
                    "other_currency": "THB",
                    "checkout": "2017-12-03",
                    "checkin": "2017-12-01",
                    "guest_groups": [{"guests": 2, "children": []}],
                    "city_ids": ["-73635"],
                    "hotels": [
                        {"review_score": "5.9", "price": "1800", hotel_url: `http://b.com?aid=${aid}` },
                        {"review_score": "4.9", "price": "600", hotel_url: `http://b.com?aid=${aid}`},
                        {"review_score": "3.9", "price": "2500", hotel_url: `http://b.com?aid=${aid}`},
                        {"review_score": "9.9", "price": "700", hotel_url: `http://b.com?aid=${aid}`},
                        {"review_score": "4.9", "price": "800", hotel_url: `http://b.com?aid=${aid}`},
                        {"review_score": "9.8", "price": "50", hotel_url: `http://b.com?aid=${aid}`},
                        {"review_score": "6.9", "price": "100", hotel_url: `http://b.com?aid=${aid}`},
                        {"review_score": "9.7", "price": "1800", hotel_url: `http://b.com?aid=${aid}`},
                        {"review_score": "7.0", "price": "1", hotel_url: `http://b.com?aid=${aid}`},
                        {"review_score": "5.9", "price": "2500", hotel_url: `http://b.com?aid=${aid}`}
                    ]
                }
            );

        let totalMin = 100;

        request(bot)
            .post('/api/getHotelByPriceRange')
            .send({
                checkin: "2017-12-01", checkout: "2017-12-03", affiliate_id: aid, dest_type: 'city',
                dest_id: '-73635', currencyCode: 'THB', totalMin: totalMin
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                expect(res.body.hotels.length).toBe(7);

                res.body.hotels.forEach((hotel, i) => {
                    expect(hotel.hotel_url).toNotContain('aid=notvalid');
                    expect(hotel.hotel_url).toContain(`aid=${aid}`);
                    expect(totalMin < parseFloat(hotel.price)).toBe(true, 'hotel['+i+'] price is under totalMin');
                });

                done();
            });
    });

    it('should respond with json filtered when total min max not passed in', (done) => {

        nock('https://distribution-xml.booking.com')
            .get('/json/getHotelAvailabilityV2')
            .query(true)
            .reply(200, {
                    "other_currency": "THB",
                    "checkout": "2017-12-03",
                    "checkin": "2017-12-01",
                    "guest_groups": [{"guests": 2, "children": []}],
                    "city_ids": ["-73635"],
                    "hotels": [
                        {"review_score": "5.9", "price": "1800", hotel_url: `http://b.com?aid=notvalid` },
                        {"review_score": "4.9", "price": "600", hotel_url: `http://b.com?aid=notvalid`},
                        {"review_score": "3.9", "price": "2500", hotel_url: `http://b.com?aid=notvalid`},
                        {"review_score": "9.9", "price": "700", hotel_url: `http://b.com?aid=notvalid`},
                        {"review_score": "4.9", "price": "800", hotel_url: `http://b.com?aid=notvalid`},
                        {"review_score": "9.8", "price": "50", hotel_url: `http://b.com?aid=notvalid`},
                        {"review_score": "6.9", "price": "100", hotel_url: `http://b.com?aid=notvalid`},
                        {"review_score": "9.7", "price": "1800", hotel_url: `http://b.com?aid=notvalid`},
                        {"review_score": "7.0", "price": "1", hotel_url: `http://b.com?aid=notvalid`},
                        {"review_score": "5.9", "price": "2500", hotel_url: `http://b.com?aid=notvalid`}
                    ]
                }
            );

        request(bot)
            .post('/api/getHotelByPriceRange')
            .send({
                checkin: "2017-12-01", checkout: "2017-12-03",
                dest_type: 'city', dest_id: '-73635', currencyCode: 'THB',
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                expect(res.body.hotels.length).toBe(10);
                done();
            });
    });

    it('should set correct region_ids, city_ids, or district_ids in request', (done) => {

        nock('https://distribution-xml.booking.com')
            .get('/json/getHotelAvailabilityV2')
            .query(function(q){
                expect(q.district_ids).toEqual(['1234']);
                expect('dest_id' in q).toBe(false);
                expect('dest_type' in q).toBe(false);
                return true;
            }).reply(200, { "hotels": [] });

        request(bot)
            .post('/api/getHotelByPriceRange')
            .send({ dest_type: 'district', dest_id: '1234' })
            .expect('Content-Type', /json/)
            .expect(200)
            .end(done);
    });

});