'use strict';

const request = require('supertest');
const bot = require('../../bot');

describe('GET /', () => {

    it('should respond with 200', (done) => {
        request(bot)
            .get('/')
            .expect('Content-Type', /html/)
            .expect(200, done);

    });

});