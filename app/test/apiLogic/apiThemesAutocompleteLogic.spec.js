'use strict';

const expect = require('expect');
const request = require('supertest');
const bot = require('../../bot');
const nock = require('nock');

describe('POST /api/autocomplete/themes', () => {

    beforeEach(() => {
        nock('https://distribution-xml.booking.com')
            .get('/json/bookings.autocomplete')
            .query(true)
            .reply(200, [
                { dest_type: 'city' },
                { dest_type: 'region' },
                { dest_type: 'airport' },
                { dest_type: 'landmark' },
                { dest_type: 'hotel' },
                { dest_type: 'country' },
                { dest_type: 'district' },
                { dest_type: 'theme' }
            ]);
    });

    afterEach(() => {
        nock.cleanAll();
    });

    it('should respond with json and should only get dest_type=theme', (done) => {
        request(bot)
            .post('/api/autocomplete/themes')
            .send({ text: 'Nightlife' })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                expect(res.body.length).toBe(1);
                done();
            });
    });
});