'use strict';

const expect = require('expect');
const request = require('supertest');
const bot = require('../../bot');
const nock = require('nock');

describe('POST /api/autocomplete/location', () => {

    beforeEach(() => {
        nock('https://distribution-xml.booking.com')
            .get('/json/bookings.autocomplete')
            .query(true)
            .reply(200, [
                { dest_type: 'city' },
                { dest_type: 'region' },
                { dest_type: 'airport' },
                { dest_type: 'landmark' },
                { dest_type: 'hotel' },
                { dest_type: 'country' },
                { dest_type: 'district' }
            ]);
    });

    afterEach(() => {
        nock.cleanAll();
    });

    it('should respond with json and filter dest_type based on param', (done) => {
        request(bot)
            .post('/api/autocomplete/location')
            .send({ text: 'Singapore', languagecode: 'en', filter: { dest_type: ['city', 'region'] } })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                expect(res.body.length).toBe(5);
                done();
            });
    });

    it('should respond correctly with the exclude query for the api', (done) => {
        request(bot)
            .post('/api/autocomplete/location')
            .send({
                text: 'Singapore',
                languagecode: 'en'
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                expect(res.body.length).toBe(7);
                done();
            });
    });
});