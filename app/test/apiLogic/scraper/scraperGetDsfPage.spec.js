'use strict';

const request = require('supertest');
const bot = require('../../../bot');
const nock = require('nock');

describe('GET /scraper/getDsfPage', () => {

    afterEach(() => { nock.cleanAll(); });

    it('should respond with html and 200', (done) => {

        nock('https://www.booking.com')
            .get('/destinationfinder.html')
            .reply(200, '<html></html>');

        request(bot)
            .post('/scraper/getDsfPage')
            .send({ dest_type: 'country', dest_id: '168' })
            .expect('Content-Type', /html/)
            .expect(200)
            .end(done);

    });
});