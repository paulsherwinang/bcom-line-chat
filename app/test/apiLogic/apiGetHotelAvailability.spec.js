'use strict';

const expect = require('expect');
const request = require('supertest');
const bot = require('../../bot');
const nock = require('nock');

describe('POST /api/getHotelAvailability', () => {

    afterEach(() => {
        nock.cleanAll();
    });

    it('should respond with json', (done) => {

        nock('https://distribution-xml.booking.com')
            .get('/json/getHotelAvailabilityV2')
            .query(true)
            .reply(200, {"other_currency":"THB", "checkout":"2017-12-07","checkin":"2017-12-01","guest_groups":[{"guests":2,"children":[]}],
                "hotels":[
                    { hotel_url: 'http://b.com?aid=notvalid' },
                    { hotel_url: 'http://b.com?aid=notvalid' },
                    { hotel_url: 'http://b.com?aid=notvalid' }
                ],"city_ids":["-73635"]});

        request(bot)
            .post('/api/getHotelAvailability')
            .send()
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;

                res.body.hotels.forEach((hotel) => {
                    expect(hotel.hotel_url).toNotContain('aid=notvalid');
                });

                done();
            });
    });


    it('should respond with hotel urls that are not removed when AID is sent', (done) => {

        let aid = 'xxxxxx';

        nock('https://distribution-xml.booking.com')
            .get('/json/getHotelAvailabilityV2')
            .query(function(q){
                expect(q.city_ids).toEqual(['1234']);
                expect('dest_id' in q).toBe(false);
                expect('dest_type' in q).toBe(false);
                return true;
            })
            .reply(200, {"other_currency":"THB", "checkout":"2017-12-07","checkin":"2017-12-01","guest_groups":[{"guests":2,"children":[]}],
                "hotels":[
                    { hotel_url: `http://b.com?aid=${aid}` },
                    { hotel_url: `http://b.com?aid=${aid}` },
                    { hotel_url: `http://b.com?aid=${aid}` }
                ],"city_ids":["-73635"]});


        request(bot)
            .post('/api/getHotelAvailability')
            .send({
                affiliate_id: aid,
                dest_type: 'city',
                dest_id: "1234"
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;

                res.body.hotels.forEach((hotel) => {
                    expect(hotel.hotel_url).toNotContain('aid=notvalid');
                    expect(hotel.hotel_url).toContain(`aid=${aid}`);
                });

                done();
            });
    });

    it('should call api with correct urls from sent body', (done) => {
        nock('https://distribution-xml.booking.com')
            .get('/json/getHotelAvailabilityV2')
            .query(function(q){
                expect(q.region_ids).toEqual(['1234']);
                return true;
            }).reply(200, { hotels: [] });

        request(bot)
            .post('/api/getHotelAvailability')
            .send({ dest_type: 'region', dest_id: "1234" })
            .end(done);
    });
});