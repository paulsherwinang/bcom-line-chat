'use strict';

const expect = require('expect');
const request = require('supertest');
const bot = require('../../bot');
const nock = require('nock');

describe('POST /api/getHotelDistricts', () => {

    afterEach(() => {
        nock.cleanAll();
    });

    it('should respond correctly', (done) => {

        nock('https://distribution-xml.booking.com')
            .get('/json/bookings.getDistrictHotels')
            .query(true)
            .reply(200, [
                    {
                        district_id: 1,
                        hotel_id: 1
                    }
                ]
            );

        request(bot)
            .post('/api/getDistrictHotels')
            .send({ hotelIds: [1] })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                expect(res.body.length).toBe(1);
                done();
            });
    });
});