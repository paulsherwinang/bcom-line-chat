'use strict';

const expect = require('expect');
const request = require('supertest');
const bot = require('../../bot');
const nock = require('nock');

describe('POST /api/getCities', () => {

    afterEach(() => {
        nock.cleanAll();
    });

    it('should respond correctly when passed array', (done) => {

        nock('https://distribution-xml.booking.com')
            .get('/json/bookings.getCities')
            .query((q) => {
                expect(q.city_ids).toBe('hello,hello');
                return true;
            })
            .reply(200, [
                { name: 'City1', city_id: 1 },
                { name: 'City2', city_id: 2 }
            ]);

        request(bot)
            .post('/api/getCities')
            .send({ cityIds: ['hello', 'hello'] })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                expect(res.body).toBeA('array');
                expect(res.body.length).toBe(2);

                res.body.forEach((result) => {
                    expect(result).toIncludeKeys(['dest_type', 'dest_id']);
                });

                expect(res.body[0].dest_id).toBe(1);
                expect(res.body[1].dest_id).toBe(2);

                done();
            });
    });


    it('should respond correctly with passed string', (done) => {

        nock('https://distribution-xml.booking.com')
            .get('/json/bookings.getCities')
            .query((q) => {
                expect(q.city_ids).toBe('hello');
                return true;
            })
            .reply(200, [{ name: 'city', city_id: 1 }]);


        request(bot)
            .post('/api/getCities')
            .send({ cityIds: 'hello' })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                expect(res.body).toBeA('array');

                res.body.forEach((result) => {
                    expect(result).toIncludeKeys(['dest_type', 'dest_id']);
                });

                expect(res.body[0].dest_id).toBe(1);

                done();
            });
    });
});