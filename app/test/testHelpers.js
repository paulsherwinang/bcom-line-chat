'use strict';

const i18n = require('i18n');

let baseReq = {
    visitor: {
        exception: () => {},
        set: () => {},
        event: () => {
            return {
                send: () => {}
            }
        }
    },
    __: i18n.__,
    __mf: i18n.__mf,
    __l: i18n.__l,
    getLocale: i18n.getLocale
};

module.exports = {
    getRes : () => {
        return {
            send: () => {},
            status: () => {
                return {
                    send: () => {},
                    json: () => {},
                    end: () => {}
                }
            }
        }
    },
    getTextReq: function(message, userId){
        let events = [this._getTextEvent(message, userId)];
        return this._getReq(events);
    },
    getFollowReq: function(userId){
        let events = [this._getFollowEvent(userId)];
        return this._getReq(events);
    },
    getUnfollowReq: function(userId){
        let events = [this._getUnfollowEvent(userId)];
        return this._getReq(events);
    },
    getJoinReq: function(userId){
        let events = [this._getJoinEvent(userId)];
        return this._getReq(events);
    },
    getLeaveReq: function(userId){
        let events = [this._getLeaveEvent(userId)];
        return this._getReq(events);
    },
    getPostbackReq: function(data, userId){
        let events = [this._getPostbackEvent(data, userId)];
        return this._getReq(events);
    },
    getBeaconReq: function(userId){
        let events = [this._getBeaconEvent(userId)];
        return this._getReq(events);
    },
    getTextEvents: function(message, userId) {
        let event = this._getTextEvent(message, userId);
        return { events: [event] };
    },
    _getReq: function(events){
        return  Object.assign({}, baseReq, { body: { events: events } });
    },
    _getTextEvent: function(message, userId){
        return {
            replyToken: 'xxx',
            type: 'message',
            message: {
                type: 'text',
                text: message
            },
            source: {
                userId: userId
            }
        }
    },
    _getFollowEvent: function(userId) {
        return {
            "replyToken": "xxx",
            "type": "follow",
            "source": {
                "userId": userId
            }
        }

    },
    _getUnfollowEvent: function(userId) {
        return {
            "type": "unfollow",
            "source": {
                "userId": userId
            }
        }
    },
    _getJoinEvent: function(userId) {
        return {
            "replyToken": 'xxx',
            "type": "join",
            "source": {
            "type": "group",
                "groupId": "cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
            }
        }
    },
    _getLeaveEvent: function(userId) {
        return {
            "type": "leave",
            "source": {
                "type": "group",
                "groupId": "cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
            }
        }
    },
    _getPostbackEvent: function(data, userId) {
        return {
            "replyToken": "xxx",
            "type": "postback",
            "source": {
                "userId": userId
            },
            "postback": {
                "data": data
            }
        }
    },
    _getBeaconEvent: function(userId) {
        return {
            "replyToken": "xxx",
            "type": "beacon",
            "timestamp": 1462629479859,
            "source": {
                "type": "user",
                "userId": userId
            },
            "beacon": {
                "hwid": "d41d8cd98f",
                "type": "enter"
            }
        }
    }
};