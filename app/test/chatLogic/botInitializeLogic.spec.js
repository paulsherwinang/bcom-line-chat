'use strict';

const expect = require('expect');
const sinon = require('sinon');
const Storage = require('../../Storage');
const i18n = require('i18n');
const line = require('node-line-bot-api');
const logic = require('../../chatLogic/botInitializeLogic');
const tree = require('../../chatLogic/botInitialize.tree');
const chatHelpers = require('../../helpers/chatHelpers');
const testHelpers = require('../testHelpers');
const TRACKS = require('../../constants').TRACKS;
const constants = require('../../constants');
const DEFAULT_LOCALE = constants.DEV.DEFAULT_LOCALE;

let storage = new Storage();
let userId = 'userId';
let sandbox;
let nextSpy;

let res = testHelpers.getRes();

describe('CHATLOGIC -- Bot Initialize Logic', () => {

    before(() => {
        storage.setUser(userId);
        line.init({});
    });

    beforeEach(() => {
        sandbox = sinon.sandbox.create();

        tree.forEach((branch, i) => {
            sandbox.spy(tree[i], 'onValidResponseAction');
        });

        nextSpy = sandbox.spy();

        sandbox.spy(chatHelpers, 'sendReplyMessage');
    });

    afterEach(() => {
        sandbox.restore();
        storage.removeUser(userId);
    });

    it('Follow event should fire welcome text', () => {
        let req = testHelpers.getFollowReq(userId);
        logic(req, res, nextSpy, storage);
        expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
    });

    it('Unfollow event should fire res.end', () => {
        sandbox.spy(res, 'status');
        logic(testHelpers.getUnfollowReq(userId), res, nextSpy, storage);
    });

    it('Join event should propagate to next', () => {
        logic(testHelpers.getJoinReq(userId), res, nextSpy, storage);
        expect(nextSpy.callCount).toBe(1);
    });

    it('Leave event should call res.end', () => {
        sandbox.spy(res, 'status');
        logic(testHelpers.getLeaveReq(userId), res, nextSpy, storage);
    });

    it('Postback event should send next', () => {
        logic(testHelpers.getPostbackReq('', userId), res, nextSpy, storage);
        expect(nextSpy.callCount).toBe(1);
    });

    it('Beacon event should send next', () => {
        logic(testHelpers.getBeaconReq(userId), res, nextSpy, storage);
        expect(nextSpy.callCount).toBe(1);
    });

    describe('message event', () => {
         describe('text message', () => {
             it(`should react to "${i18n.__('HOTEL_BOOK_TREE_TRIGGER_TEXT')}" and set track`, () => {
                 let text = i18n.__('HOTEL_BOOK_TREE_TRIGGER_TEXT');
                 logic(testHelpers.getTextReq(text, userId), res, nextSpy, storage);

                 let userData = storage.getUserData(userId);

                 expect(userData.track).toBe(TRACKS.LOCATION_DATE);
                 expect(nextSpy.callCount).toBe(1);
             });

             it(`should react to ${i18n.__('START_OVER_TEXT')} and reset user, language retained`, () => {
                 let text = i18n.__('START_OVER_TEXT');
                 storage.setUserTrack(userId, TRACKS.LOCATION_DATE);
                 storage.setUserChoice(userId, 2, 'Test Choice');
                 storage.setUserChoice(userId, 3, 'Test Choice');
                 storage.setUserChoice(userId, 4, 'Test Choice');
                 storage.setUserLocale(userId, 'th');
                 logic(testHelpers.getTextReq(text, userId), res, nextSpy, storage);

                 let userData = storage.getUserData(userId);
                 expect(userData.track).toBe('NONE');
                 expect(userData.conversationLevel).toBe(0);
                 expect(userData.locale).toNotBe('en');
                 expect(userData.suggestion).toEqual({});
                 expect(userData.choice).toEqual({});
             });

             it(`should react to ${i18n.__('CHANGE_LANGUAGE')} and change locale correctly`, () => {
                 let text = i18n.__('CHANGE_LANGUAGE');
                 expect(storage.getUserData(userId).locale).toBe(DEFAULT_LOCALE);
                 logic(testHelpers.getTextReq(text, userId), res, nextSpy, storage);

                 expect(nextSpy.callCount).toBe(1);
                 expect(storage.getUserData(userId).locale)
                     .toBeA('string')
                     .toNotBe(DEFAULT_LOCALE);
             });

             it('should bring users without track next', () => {
                 let text = 'Hi';
                 storage.removeUser(userId);
                 logic(testHelpers.getTextReq(text, userId), res, nextSpy, storage);

                 expect(nextSpy.callCount).toBe(1);
             });

             it('should bring users with track next ', () => {
                 let text = "Should go in";
                 storage.setConversationLevel(userId, 1);
                 storage.setUserTrack(userId, TRACKS.LOCATION_DATE);
                 logic(testHelpers.getTextReq(text, userId), res, nextSpy, storage);

                 expect(nextSpy.callCount).toBe(1);
             });
         })
    });

});