'use strict';

const sinon = require('sinon');
const expect = require('expect');
const i18n = require('i18n');
const tree = require('../../chatLogic/discoverLogic.tree');
const logic = require('../../middleware/levelDistribution')(tree);
const chatHelpers = require('../../helpers/chatHelpers');
const apiHelpers = require('../../helpers/apiHelpers');
const testHelpers = require('../testHelpers');
const Storage = require('../../Storage');

let storage = new Storage();
let userId = 'userId';
let sandbox;
let nextSpy;

let res = testHelpers.getRes();

describe.skip('CHATLOGIC -- Passion Search Track', () => {

    before(() => {
        storage.setUser(userId);
    });

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        sandbox.stub(chatHelpers, 'sendReplyMessage');

        let locArr = [
            { dest_type: 'city', dest_id: '1', label: 'Singapore', hotels: 1 },
            { dest_type: 'region', dest_id: '2', label: 'Singapoor', hotels: 1  },
            { dest_type: 'city', dest_id: '3', label: 'Singapoorer', hotels: 1  },
            { dest_type: 'region', dest_id: '4', label: 'Singalong', hotels: 1  },
            { dest_type: 'city', dest_id: '5', label: 'Singalongandenjoy', hotels: 1 }
        ];

        let themesArr = [
            { dest_type: 'theme' }
        ];

        sandbox.stub(apiHelpers, 'getLocationAutocomplete').callsFake(() => {
            return {
                then: (cb) => { cb(locArr); }
            };
        });

        nextSpy = sandbox.spy();
    });

    afterEach(() => { sandbox.restore(); });

    describe('LEVEL 1 - Ask for Activity', () => {
        it('should hit the api, when results available, move to Level 9', () => {
            let suggestion1 = [{ dest_type: 'theme', label: 'hi!' }];
            sandbox.spy(chatHelpers, 'pushMessage');
            sandbox.stub(apiHelpers, 'getThemesAutocomplete').callsFake(() => {
                return { then: (cb) => { cb(suggestion1) } }
            });
            sandbox.stub(tree[9], '_getLineMessages').callsFake(() => {
                return { then: (cb) => { cb([]) } };
            });

            let text = 'Hiking', level = 1, next = 9, req = testHelpers.getTextReq(text, userId);
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(apiHelpers.getThemesAutocomplete.calledWith(text)).toBe(true);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.pushMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).suggestion[level]).toBe(suggestion1);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should hit the api, respond error message if autocomplete empty, ask question again when the response is not valid and stay in this level', () => {
            sandbox.stub(apiHelpers, 'getThemesAutocomplete').callsFake(() => {
                return { then: (cb) => { cb([]) } }
            });

            let text = "Error", level = 1;
            let req = testHelpers.getTextReq(text, userId);
            sandbox.spy(tree[level], '_getErrorMessages');
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getErrorMessages.callCount).toBe(1);
            expect(apiHelpers.getThemesAutocomplete.calledWith(text)).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });
    });

    describe('LEVEL 2 - Location ask if decided or not', () => {
        it(`should respond to any top three location suggestion and select that as the location and go to level 5`, () => {
            let text = 'Japan',
                choice9 = {
                    top_destinations: [{ name: 'Japan' }, { name: 'Korea'}, { name: 'Bangkok' }]
                }, level = 2, next = 5, req = testHelpers.getTextReq(text, userId);
            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 9, choice9);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(sinon.match.object, sinon.match.object, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[4]).toEqual(choice9.top_destinations[0]);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it(`should respond to decided location "${i18n.__('PASSION_DISCOVER_TREE.DECIDED_LOCATION_TEXT')}", and go to Level 3`, () => {
            let text = i18n.__('PASSION_DISCOVER_TREE.DECIDED_LOCATION_TEXT'),
                choice9 = {
                    top_destinations: [{ name: 'Japan' }, { name: 'Korea'}, { name: 'Bangkok' }]
                }, level = 2, next = 3, req = testHelpers.getTextReq(text, userId);
            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 9, choice9);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toEqual(i18n.__l('PASSION_DISCOVER_TREE.DECIDED_LOCATION_TEXT'));
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it(`should respond correctly to undecided "${i18n.__('PASSION_DISCOVER_TREE.UNDECIDED_LOCATION_TEXT')}" and go to Level 5`, () => {
            let text = i18n.__('PASSION_DISCOVER_TREE.UNDECIDED_LOCATION_TEXT'),
                choice9 = {
                    top_destinations: [{ name: 'Japan' }, { name: 'Korea'}, { name: 'Bangkok' }]
                }, level = 2, next = 5, req = testHelpers.getTextReq(text, userId);

            storage.setUserChoice(userId, 9, choice9)
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toEqual(i18n.__l('PASSION_DISCOVER_TREE.UNDECIDED_LOCATION_TEXT'));
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should respond to choices not in the question, stay in level, and ask again', () => {
            let choice = "Error", level = 2,
                choice9 = {
                    top_destinations: [{ name: 'Japan' }, { name: 'Korea'}, { name: 'Bangkok' }]
                };
            let req = testHelpers.getTextReq(choice, userId);

            sandbox.spy(tree[level], '_getErrorMessages');
            storage.setUserChoice(userId, 9, choice9);
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getErrorMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });
    });

    describe('LEVEL 3 - Location is decided', () => {
        it('should respond to location text input, make an autocomplete call, and let them choose which location from results and go to level 4', () => {
            let text = 'Japan',
                level = 3, next = 4, req = testHelpers.getTextReq(text, userId);
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(apiHelpers.getLocationAutocomplete.calledWith({
                text: text,
                languagecode: 'en',
                filter: { dest_type: ['airport', 'landmark', 'hotel', 'district'] }
            })).toBe(true);

            expect(storage.getUserData(userId).choice[level]).toEqual(text);
            expect(storage.getUserData(userId).suggestion[level].length).toEqual(5);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should respond to location text input, if results returned empty, should ask them the question again and stay in level')
    });

    describe('LEVEL 4 - Location api results to choose', () => {
        it('should respond to any of the choices from the carousel, and move to level 5', () => {
            let text = 'Japan',
                level = 4, next = 5,
                suggest3 = [
                    { label: 'Japan', hotels: 2 }
                ], req = testHelpers.getTextReq(text, userId);
            storage.setConversationLevel(userId, level);
            storage.setUserSuggestion(userId, 3, suggest3);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toEqual(suggest3[0]);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should respond to choices not in the carousel, stay in level, and ask again', () => {
            let choice = "Error", level = 4,
                suggest3 = [
                    { label: 'Japan', hotels: 2 }
                ], req = testHelpers.getTextReq(choice, userId);
            sandbox.spy(tree[level], '_getErrorMessages');
            storage.setUserSuggestion(userId, 3, suggest3);
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getErrorMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });

        it(`should respond to "${i18n.__('PASSION_DISCOVER_TREE.LOCATION_NOT_IN_CHOICE_ACTION_TEXT')}" and go back to ask question again`);
    });

    describe('LEVEL 5 - Ask for dates if decided or undecided', () => {
        it(`should respond correctly to "${i18n.__('PASSION_DISCOVER_TREE.UNDECIDED_DATES_TEXT')}" and go to Level 8`, () => {
            let text = i18n.__('PASSION_DISCOVER_TREE.UNDECIDED_DATES_TEXT'),
                level = 5, next = 8, req = testHelpers.getTextReq(text, userId);

            sandbox.stub(tree[8], '_getLineMessages').callsFake(() => {
                return { then: (cb) => { cb([]) } };
            });
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toEqual(i18n.__l('PASSION_DISCOVER_TREE.UNDECIDED_DATES_TEXT'));
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it(`should respond to "${i18n.__('PASSION_DISCOVER_TREE.DECIDED_DATES_TEXT')}", and go to Level 6`, () => {
            let text = i18n.__('PASSION_DISCOVER_TREE.DECIDED_DATES_TEXT'),
                level = 5, next = 6, req = testHelpers.getTextReq(text, userId);
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toEqual(i18n.__l('PASSION_DISCOVER_TREE.DECIDED_DATES_TEXT'));
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should respond to choices not in the question, stay in level, and ask again', () => {
            let choice = "Error", level = 5, req = testHelpers.getTextReq(choice, userId);
            sandbox.spy(tree[level], '_getErrorMessages');
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getErrorMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });
    });

    describe('LEVEL 6 - Dates are decided', () => {
        it(`should ask for dates, and move to level 7`, () => {
            let clock = sinon.useFakeTimers(1491264000000);

            let text = '05/05 - 20/05',
                level = 6, next = 7, req = testHelpers.getTextReq(text, userId);
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).suggestion[level]).toEqual(['2017-05-05', '2017-05-20']);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);

            clock.restore();
        });

        it('should respond to text if invalid, stay in level and ask again', () => {
            let choice = "Error", level = 6, req = testHelpers.getTextReq(choice, userId);
            sandbox.spy(tree[level], '_getErrorMessages');
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getErrorMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });
    });

    describe('LEVEL 7 - Dates confirmation', () => {
        it('should respond to yes and store dates and move to level 8', () => {
            let text = i18n.__('PASSION_DISCOVER_TREE.DATE_CONFIRM_YES_TEXT_ACTION'),
                level = 7, next = 8, req = testHelpers.getTextReq(text, userId),
                suggest6 =  ['2017-05-05', '2017-05-20'];

            sandbox.stub(tree[8], '_getLineMessages').callsFake(() => {
                return { then: (cb) => { cb([]) } };
            });
            storage.setUserSuggestion(userId, 6, suggest6);
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toEqual(suggest6);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should respond to no, stay in level and ask again', () => {
            let choice = i18n.__('PASSION_DISCOVER_TREE.DATE_CONFIRM_NO_TEXT_ACTION'),
                level = 7, suggest6 =  ['2017-05-05', '2017-05-20'],
                req = testHelpers.getTextReq(choice, userId);
            sandbox.spy(tree[level], '_getErrorMessages');
            storage.setUserSuggestion(userId, 6, suggest6);
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getErrorMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });

        it('should respond to not yes or no, stay in level and ask again', () => {
            let choice = "Error", level = 7, req = testHelpers.getTextReq(choice, userId);
            sandbox.spy(tree[level], '_getErrorMessages');
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getErrorMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });
    });

    describe('Level 8 - Link out to Booking.com', () => {
        it(`should respond to "${i18n.__('PASSION_DISCOVER_TREE.SEARCH_AGAIN_TEXT')}" and go to level 1`, () => {
            let text = i18n.__('PASSION_DISCOVER_TREE.SEARCH_AGAIN_TEXT'),
                level = 8, next = 1, req = testHelpers.getTextReq(text, userId);

            sandbox.stub(tree[8], '_getLineMessages').callsFake(() => {
                return { then: (cb) => { cb([]) } };
            });
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should respond to anything, send the same message since "search again" is in there', () => {
            let text = "Error", level = 8, req = testHelpers.getTextReq(text, userId);
            sandbox.stub(tree[level], '_getLineMessages').callsFake(() => {
                return { then: (cb) => { cb([]) } };
            });
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getLineMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });
    });

    describe('LEVEL 9 - Carousel for themes autocomplete', () => {
        it('should check response if in suggestion, and move to level 2', () => {
            let suggest1 = [{ label: 'Partying', dest_type: 'theme', dest_id: '123' }];
            let text = 'Partying',
                level = 9, next = 2, req = testHelpers.getTextReq(text, userId);

            sandbox.stub(apiHelpers, 'getCities').callsFake(() => {
                return { then: (cb) => { cb([]); } };
            });

            storage.setUserSuggestion(userId, 1, suggest1);
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toEqual(suggest1[0]);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should check response if not in suggestion, stay in level and ask again', () => {
            let choice = "Error", level = 9;
            let req = testHelpers.getTextReq(choice, userId);
            sandbox.stub(tree[level], '_getLineMessages').callsFake(() => {
                return { then: (cb) => { cb([]) } };
            });

            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getErrorMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });

        it(`should check for ${i18n.__('PASSION_DISCOVER_TREE.THEMES_CAROUSEL_SEARCH_AGAIN_SELECT_TEXT')} go to previous level and ask location again`, () => {
            let text = i18n.__('PASSION_DISCOVER_TREE.THEMES_CAROUSEL_SEARCH_AGAIN_SELECT_TEXT'),
                level = 9, next = 1, req = testHelpers.getTextReq(text, userId);
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });
    });
});