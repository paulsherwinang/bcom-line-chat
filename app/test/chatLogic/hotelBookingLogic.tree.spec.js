'use strict';

const testHelpers = require('../testHelpers');
const locationHelpers = require('../../helpers/locationHelpers');
const tree = require('../../chatLogic/hotelBookingLogic.tree');
const expect = require('expect');
const i18n = require('i18n');
const sinon = require('sinon');

let sandbox;
let clock;
let userId = 'userId';

let res = testHelpers.getRes();

describe('TREE -- hotel booking logic tree', () => {
    describe('level 1', () => {
        it('should filter correctly', () => {
            let orig = [
                { dest_type: 'region', city_name: 'London' },
                { dest_type: 'city', city_name: 'London' },
                { dest_type: 'city', city_name: 'Tokyo' },
                { dest_type: 'city', city_name: 'Manila' },
                { dest_type: 'city', city_name: 'Jakarta' },
            ];

            let filtered = locationHelpers.locationInput.filterSuggestionsWithSameCity(orig);
            expect(filtered.length).toBe(4);
            expect(filtered[0].dest_type).toBe('city');

            let orig2 = [
                { dest_type: 'region', city_name: 'Tokyo' },
                { dest_type: 'city', city_name: 'Manila' }
            ];

            let filtered2 = locationHelpers.locationInput.filterSuggestionsWithSameCity(orig2);
            expect(filtered2.length).toBe(2);
        })
    });

    describe('level 2', () => {
        it('_getChoiceFromSuggestions should get correct object from inputted label', () => {
            let suggestions = [
                { dest_id: '1', label: 'label1' },
                { dest_id: '2', label: 'label2' },
                { dest_id: '3', label: 'label3' },
                { dest_id: '4', label: 'label4' }
            ];

            expect(tree[2]._getChoiceFromSuggestions('label1', suggestions)).toEqual(suggestions[0]);
            expect(tree[2]._getChoiceFromSuggestions('12345', suggestions)).toBeFalsy();
        });

        it('_getNextLevel should get correct level depending on the choice. If country, level 3. If anything, 4', () => {
            let choice1 = { dest_id: '1', dest_type: 'city', label: 'label1' };
            let level3 = tree[2]._getNextLevel(choice1);
            expect(level3).toBe(5);

            let choice2 = { dest_id: '1', dest_type: 'country', label: 'label1' };
            let level2 = tree[2]._getNextLevel(choice2);
            expect(level2).toBe(3);
        });
    });

    describe('level 4', () => {

        beforeEach(() => {
            sandbox = sinon.sandbox.create();
            let mockDate = new Date('Thu May 18 2017 14:58:26 GMT+0800 (SGT)');
            clock = sinon.useFakeTimers(mockDate.getTime());
        });

        afterEach(() => {
            sandbox.restore();
            clock.restore();
        });

        it('_getErrorMessages should call error message correctly', () => {
            let getE = (text) => {
                return { message: { text: text } };
            }, storage = {}, userId = 'xx', req = testHelpers.getTextReq('', '');

            expect(tree[5]._getErrorMessages(getE('02/01'), storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.DATE_INVALID_MESSAGE'));
            expect(tree[5]._getErrorMessages(getE('02/03-02/01'), storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.CHECKOUT_BEFORE_CHECKIN_INVALID_MESSAGE'));
            expect(tree[5]._getErrorMessages(getE('02/03-03/05'), storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.STAY_DURATION_INVALID_MESSAGE'));
        });

        describe('_isValidDates', () => {
            it('should loosen validation and return true even if text has whitespaces " mm/dd - mm/dd "', () => {
                let valid1 = tree[5]._isValidDates('02/07', '12/07');
                expect(valid1).toBe(true);
                let valid2 = tree[5]._isValidDates(' 02/07', '12/07 ');
                expect(valid2).toBe(true);
                let valid3 = tree[5]._isValidDates('02/07', '12/07');
                expect(valid3).toBe(true);
            });

            it('should return false when checkin is after checkout', () => {
                expect(tree[5]._isValidDates('01/08', '01/06')).toBe(false);
            });

            it('should return false when stay duration is more than 30 days', () => {
                expect(tree[5]._isValidDates('01/08','10/09')).toBe(false);
            });
        });

        describe('_storeDatesInChoice', () => {
            it('should create dates correctly', () => {
                expect(tree[5]._getDatesArrayForStore('07/06', '08/06')).toEqual(['2017-06-07', '2017-06-08']);
                expect(tree[5]._getDatesArrayForStore('07/01','08/01')).toEqual(['2018-01-07', '2018-01-08']);
            });
        });

    });

    describe('level 6', () => {
         it('isValidResponse should only validate on the choices sent in', () => {
             let text = i18n.__('HOTEL_BOOK_TREE.SHOW_BY_BUDGET_MESSAGE'),
                 validReq = testHelpers.getTextReq(text, userId);
             expect(tree[6].isValidResponse(validReq, res, {})).toBe(true);

             let invalidReq = testHelpers.getTextReq('invalid', userId);
             expect(tree[6].isValidResponse(invalidReq, res, {})).toBe(false);
         });

        it('_getNextLevel should get appropriate level when appropriate message has been sent', () => {
            let toLevel7 = i18n.__('HOTEL_BOOK_TREE.SHOW_DEALS_MESSAGE');
            let req1 = testHelpers.getTextReq(toLevel7, userId);
            expect(tree[6]._getNextLevel(toLevel7, req1)).toBe(7);

            let toLevel8 = i18n.__('HOTEL_BOOK_TREE.SHOW_BY_BUDGET_MESSAGE');
            let req2 = testHelpers.getTextReq(toLevel8, userId);
            expect(tree[6]._getNextLevel(toLevel8, req2)).toBe(8);
        });
    });

    describe('level 8', () => {
        it('_getLineMessages should get the correct object', () => {
            let req = testHelpers.getTextReq('', userId);
            let msgs = tree[8]._getLineMessages({}, {}, '', req, {});

            expect(msgs.length).toBe(2);
            expect(msgs[1].template.columns.length).toBe(5);
            expect(msgs[1].template.columns[4].title).toContain('+');
            expect(msgs[1].template.columns[3].title).toNotContain('+');
        });

        it('isValidResponse should get correct response from possible responses', () => {
            let tier1 = i18n.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_TITLE_40CH', {min: 0, max: 1800}),
                req1 = testHelpers.getTextReq(tier1, userId),
                tier2 = i18n.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_TITLE_40CH', {min: 1800, max: 3700}),
                req2 = testHelpers.getTextReq(tier2, userId),
                tier3 = i18n.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_TITLE_40CH', {min: 3700, max: 5600}),
                req3 = testHelpers.getTextReq(tier3, userId),
                tier4 = i18n.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_TITLE_40CH', {min: 5600, max: 7500}),
                req4 = testHelpers.getTextReq(tier4, userId),
                tier5 = i18n.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIERMAX_TITLE_40CH', {min: 7500 }),
                req5 = testHelpers.getTextReq(tier5, userId),
                error = 'error',
                reqE = testHelpers.getTextReq(error, userId);

            expect(tree[8].isValidResponse(req1, res, {})).toBe(true);
            expect(tree[8].isValidResponse(req2, res, {})).toBe(true);
            expect(tree[8].isValidResponse(req3, res, {})).toBe(true);
            expect(tree[8].isValidResponse(req4, res, {})).toBe(true);
            expect(tree[8].isValidResponse(req5, res, {})).toBe(true);
            expect(tree[8].isValidResponse(reqE, res, {})).toBe(false);
        });
    });
});