// 'use strict';
//
// const expect = require('expect');
// const request = require('supertest');
// const proxyquire = require('proxyquire');
// const i18n = require('i18n');
// const nock = require('nock');
// const testHelpers = require('../testHelpers');
// const async = require('async');
//
// let agent;
// let bot = proxyquire('../../bot', {
//     './middleware/validateSignature': (req, res, next) => {
//         next();
//     }
// });
//
// nock('https://api.line.me')
//     .post('/v2/oauth/accessToken')
//     .reply(200, {
//         access_token: 'xxx'
//     });
//
// nock('https://api.line.me')
//     .persist()
//     .post('/v2/bot/message/reply', (body) => {
//         console.log('=========================');
//         console.log(JSON.stringify(body));
//         console.log('=========================');
//         return true;
//     })
//     .reply(200);
//
// // nock('http://localhost:3000')
// //     .persist()
// //     .get('/api/autocomplete/themes')
// //     .query(true)
// //     .reply(200, [
// //         {
// //             dest_id: "239",
// //             nr_dest: 603,
// //             label: "Shopping for shoes",
// //             top_destinations: [-121726, -1679595, -3715584],
// //             dest_type: "theme"
// //         },
// //         {
// //             top_destinations: [-246227, -1353149, -73635],
// //             dest_type: "theme",
// //             nr_dest: 444,
// //             dest_id: "242",
// //             label: "Shopping for electronics"
// //         }
// //     ]);
//
// describe.only('e2e bot discoverLogic', () => {
//
//     before(() => {
//         agent = bot.listen(3000, () => {
//             console.log('~~~Bot listening at port 3000~~~~');
//         });
//     });
//
//     it('should do stuff', (done) => {
//
//         async.series([
//             function(cb) { request(bot).post('/webhook').send(testHelpers.getTextEvents('Hi!', 'xxx')).expect(200, cb); },
//             function(cb) { request(bot).post('/webhook').send(testHelpers.getTextEvents('Help me discover new places to go!', 'xxx')).expect(200, cb); },
//             function(cb) { request(bot).post('/webhook').send(testHelpers.getTextEvents('Shop', 'xxx')).expect(200, cb); },
//             // function(cb) { request(bot).post('/webhook').send(testHelpers.getTextEvents('Hi!', 'xxx')).expect(200, cb); },
//             // function(cb) { request(bot).post('/webhook').send(testHelpers.getTextEvents('Hi!', 'xxx')).expect(200, cb); },
//             // function(cb) { request(bot).post('/webhook').send(testHelpers.getTextEvents('Hi!', 'xxx')).expect(200, cb); },
//             // function(cb) { request(bot).post('/webhook').send(testHelpers.getTextEvents('Hi!', 'xxx')).expect(200, cb); },
//         ]);
//
//     }).timeout(10000);
//
// });