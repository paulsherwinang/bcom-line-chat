'use strict';

require('mocha');

const expect = require('expect');
const sinon = require('sinon');
const line = require('node-line-bot-api');
const tree = require('../../chatLogic/hotelBookingLogic.tree');
const logic = require('../../middleware/levelDistribution')(tree);
const apiHelpers = require('../../helpers/apiHelpers');
const apiaiHelpers = require('../../helpers/apiaiHelpers');
const chatHelpers = require('../../helpers/chatHelpers');
const testHelpers = require('../testHelpers');
const Storage = require('../../Storage');
const CONST = require('../../constants');
const i18n = require('i18n');

let storage = new Storage();
let userId = 'userId';
let sandbox;
let nextSpy;

describe('CHATLOGIC -- Location, Date, and Filters Hotel Booking', () => {

    before(() => {
        storage.setUser(userId);
        line.init({});
    });

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        sandbox.stub(chatHelpers, 'sendReplyMessage').callsFake(() => {
            return { then: (cb) => { cb(); return { catch: () => {} } } };
        });

        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].forEach((level) => {
            sandbox.spy(tree[level], '_getLineMessages');
            sandbox.spy(tree[level], '_getErrorMessages');
        });

        let availabilityResponse = {
            checkout: '2017-12-14',
            checkin: '2017-12-13',
            city_ids: ['-1'],
            other_currency: 'THB',
            hotels: [
                {
                    review_score: '8.9',
                    review_score_word: 'Very Good',
                    review_nr: 123,
                    photo: "http://aff.bstatic.com/images/hotel/max500/340/34007512.jpg",
                    price: '200.50',
                    hotel_name: 'Hotel 1',
                    hotel_url: "https://www.booking.com/hotel/sg/mandarin.en.html?aid=353251&checkin=2017-12-01&checkout=2017-12-07&room1=A%2CA"

                }
            ]
        };

        sandbox.stub(apiHelpers, 'getLocationAutocomplete').callsFake(() => {
            return { then: (cb) => { cb([
                { dest_type: 'city', hotels: 2, label: 'ho'},
                { dest_type: 'country', hotels: 2, label: 'ho'},
                { dest_type: 'airport', hotels: 2, label: 'ho'},
                { dest_type: 'airport', hotels: 2, label: 'ho'},
                { dest_type: 'airport', hotels: 2, label: 'ho'}
            ]); return { catch: () => {} } } };
        });

        sandbox.stub(apiHelpers, 'getHotelAvailabilityForDeals').callsFake(() => {
            return {
                then: (cb) => {
                    cb(availabilityResponse);
                    return { catch: () => {} }
                }
            }
        });

        sandbox.stub(apiHelpers, 'getHotelAvailabilityForBudget').callsFake(() => {
            return { then: (cb) => { cb(availabilityResponse); return { catch: () => {} } } }
        });

        nextSpy = sandbox.spy();
    });

    afterEach(() => {
        storage.removeUser(userId);
        sandbox.restore();
    });

    describe('LEVEL 0 - Trigger Question Response', () => {
        it('respond to the trigger question if response is valid and move to level 1', () => {
            let text = i18n.__('HOTEL_BOOK_TREE_TRIGGER_TEXT'), level = 0, next = 1, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(), event = req.body.events[0];
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[next]._getLineMessages.callCount).toBe(1);
            expect(tree[storage.getUserData(userId).conversationLevel]._getLineMessages(event, storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.INTRO_QUESTION'));
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });
    });

    describe('LEVEL 1 - Input location name and make an autocomplete search', () => {
        it('should hit the api, set first four suggestions in storage, and show location carousel', () => {
            let text = 'Japan', level = 1, next = 2, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(), event = req.body.events[0];
            storage.setConversationLevel(userId, level);
            sandbox.spy(tree[level], '_getPendingMessages');
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(apiHelpers.getLocationAutocomplete.calledOnce).toBe(true);
            expect(tree[level]._getPendingMessages.callCount).toBe(1);
            expect(tree[next]._getLineMessages.callCount).toBe(1);
            expect(tree[userLevel]._getLineMessages(event, storage, userId, req, res, [])[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.LOCATION_AUTOCOMPLETE_RESULT_SEARCH_MESSAGE', {text: text}));
            expect(chatHelpers.sendReplyMessage.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).suggestion[next].length).toBe(4);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should hit the api. If autocomplete returned empty, show error and ask again', () => {
            apiHelpers.getLocationAutocomplete.restore();

            sandbox.stub(apiHelpers, 'getLocationAutocomplete').callsFake(() => {
                return { then: (cb) => { cb([]); return { catch: () => {} } } };
            });

            let text = 'Empty', level = 1, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(), event = req.body.events[0];
            sandbox.spy(tree[level], '_getPendingMessages');
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(tree[level]._getErrorMessages.calledOnce).toBe(true);
            expect(tree[level]._getPendingMessages.callCount).toBe(1);
            expect(tree[userLevel]._getLineMessages(event, storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.INTRO_QUESTION'));
            expect(chatHelpers.sendReplyMessage.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(apiHelpers.getLocationAutocomplete.calledOnce).toBe(true);
            expect(userLevel).toBe(level);
        });
    });

    describe('LEVEL 2 - Chosen location check, if country ', () => {
        it('should check for selected choice. If country bring you to level 3', () => {
            let text = 'Japan', level = 2, next = 3, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(),
                suggestions = [
                    { dest_id: '1', label: 'Japan', dest_type: 'country', name: 'Japan' },
                    { dest_id: '2', label: 'Tokyo', dest_type: 'city', name: 'Tokyo' }
                ];
            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 2, suggestions[1]);
            storage.setUserSuggestion(userId, level, suggestions);
            logic(req, res, nextSpy, storage);

            expect(tree[next]._getLineMessages.calledOnce).toBe(true);
            expect(tree[storage.getUserData(userId).conversationLevel]._getLineMessages(req.body.events[0], storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.COUNTRY_CHOSEN_ASK_CITY_QUESTION', { name: text}));
            expect(chatHelpers.sendReplyMessage.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toBe(suggestions[0]);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should check for reply. If not country bring you to level 4', () => {
            let text = 'Japan', level = 2, next = 5,
                req = testHelpers.getTextReq(text, userId),
                res = testHelpers.getRes(),
                event = req.body.events[0],
                suggestions = [
                    { dest_id: '1234', label: 'Japan', dest_type: 'city' },
                    { dest_id: '2', label: 'Tokyo', dest_type: 'city' }
                ];
            storage.setConversationLevel(userId, level);
            storage.setUserSuggestion(userId, level, suggestions);
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(tree[next]._getLineMessages.calledOnce).toBe(true);
            expect(tree[userLevel]._getLineMessages(event, storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.CHECKIN_CHECKOUT_DATE_QUESTION'));
            expect(chatHelpers.sendReplyMessage.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).suggestion[level].length).toBe(2);
            expect(userLevel).toBe(next);
        });

        it('should check for reply and make sure it is any of the suggestions', () => {
            let text = 'error', level = 2, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(),
                suggestions = [ { dest_id: '1234', label: 'Japan', dest_type: 'city', hotels: 2 } ];
            storage.setConversationLevel(userId, level);
            storage.setUserSuggestion(userId, level, suggestions);
            logic(req, res, nextSpy, storage);

            expect(tree[level]._getErrorMessages.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });

        it('should react to "none of the above", ask location again', () => {
            let text = i18n.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_NONE_ACTION_TEXT_300CH'),
                level = 2, previousLevel = 1, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes();
            storage.setConversationLevel(userId, level);

            logic(req, res, nextSpy, storage);
            expect(tree[previousLevel]._getLineMessages.callCount).toBe(1);
            expect(storage.getUserData(userId).conversationLevel).toBe(previousLevel);
        });
    });

    describe('LEVEL 3 - Location chosen is a country, ask for a city', () => {
        it('should hit the api, return the results without countries and display and set first four suggestions in storage', () => {
            let text = 'Tokyo', level = 3, next = 4, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(), event = req.body.events[0];
            sandbox.spy(tree[level], '_getPendingMessages');
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            expect(tree[next]._getLineMessages.callCount).toBe(1);
            expect(tree[level]._getPendingMessages.callCount).toBe(1);
            expect(tree[storage.getUserData(userId).conversationLevel]._getLineMessages(event, storage, userId, req, res, [])[1].altText).toBe(i18n.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_ALTEXT_140CH'));
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(apiHelpers.getLocationAutocomplete.callCount).toBe(1);
            expect(storage.getUserData(userId).suggestion[next].length).toBe(4);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should hit the api. If autocomplete returned empty, send error, stay in level and ask again', () => {
            apiHelpers.getLocationAutocomplete.restore();
            sandbox.stub(apiHelpers, 'getLocationAutocomplete').callsFake(() => { return { then: (cb) => { cb([]); return { catch: () => {} } } }; });
            let text = 'Empty', level = 3, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(), event = req.body.events[0],
                choice = { dest_id: '1234', label: 'Japan', dest_type: 'country', name: 'Japan' },
                suggestions = [ { dest_id: '1234', label: 'Japan', dest_type: 'city', name: 'Japan' } ];

            sandbox.spy(tree[level], '_getPendingMessages');
            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 2, choice);
            storage.setUserSuggestion(userId, level, suggestions);
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(tree[level]._getLineMessages.callCount).toBe(1);
            expect(tree[level]._getPendingMessages.callCount).toBe(1);
            expect(tree[userLevel]._getLineMessages(event, storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.COUNTRY_CHOSEN_ASK_CITY_QUESTION', { name: choice.name }));
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(apiHelpers.getLocationAutocomplete.callCount).toBe(1);
            expect(userLevel).toBe(level);
        });
    });

    describe('LEVEL 4 - carousel, choose a location (no country)', () => {
        it('if valid choice should bring you to level 5', () => {
            let text = 'Tokyo', level = 4, next = 5, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(), event = req.body.events[0],
                suggestions = [
                    { dest_id: '1234', label: 'Japan', dest_type: 'country'},
                    { dest_id: '1234', label: 'Tokyo', dest_type: 'city' }
                ];
            storage.setConversationLevel(userId, level);
            storage.setUserSuggestion(userId, level, suggestions);
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(tree[next]._getLineMessages.callCount).toBe(1);
            expect(tree[userLevel]._getLineMessages(event, storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.CHECKIN_CHECKOUT_DATE_QUESTION'));
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toEqual(suggestions[1]);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('if invalid choice should stay on level and ask again', () => {
            let text = 'error', level = 4, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(), event = req.body.events[0],
                suggestions = [ { dest_id: '1234', label: 'Tokyo', dest_type: 'city', hotels: 0 } ];
            storage.setConversationLevel(userId, level);
            storage.setUserSuggestion(userId, level, suggestions);
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(tree[level]._getLineMessages.callCount).toBe(1);
            expect(tree[userLevel]._getLineMessages(event, storage, userId, req, res)[1].altText).toBe(i18n.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_ALTEXT_140CH'));
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(userLevel).toBe(level);
        });

        it('should react to "none of the above", ask location again', () => {
            let text = i18n.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_NONE_ACTION_TEXT_300CH'),
                level = 4, previousLevel = 3, choice2 = { name: 'Hi' }, suggestions4 = [{}];

            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 2, choice2);
            storage.setUserSuggestion(userId, level, suggestions4);
            logic(testHelpers.getTextReq(text, userId), testHelpers.getRes(), nextSpy, storage);

            expect(tree[previousLevel]._getLineMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(sinon.match.object, sinon.match.object, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(previousLevel);
        });
    });

    describe('LEVEL 5 - ask for check in and check out date', () => {
        it('should ask for the check in and check out date in the format "dd/mm - dd/mm"', () => {
            let apiaiRes = {
                "lang": "en",
                "result": {
                    "parameters": {
                        "any": "till",
                        "checkin_day": "05",
                        "checkin_month": "05",
                        "checkout_day": "05",
                        "checkout_month": "20"
                    },
                },
                "sessionId": "usersessiojn"
            };

            sandbox.stub(apiaiHelpers, 'getTextRequest').callsFake(() => {
                return { then: (cb) => {
                    cb(apiaiRes);
                    return { catch: () => {} }
                } }
            });

            let clock = sinon.useFakeTimers(1491264000000);
            let text = '05/05 - 20/05', level = 5, next = 6, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(), event = req.body.events[0];
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(tree[next]._getLineMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(tree[userLevel]._getLineMessages(event, storage, userId, req, res)[0].template.type).toBe('confirm');
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toEqual(['2017-05-05','2017-05-20']);
            expect(userLevel).toBe(next);
            clock.restore();
        });

        it('should send error message if not a valid "mm/dd - mm/dd" format', () => {
            let text = 'error', level = 5, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(), event = req.body.events[0];
            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(tree[level]._getErrorMessages.callCount).toBe(1);
            expect(tree[userLevel]._getLineMessages(event, storage, userId, req, res)[0].text).toBe(i18n.__('HOTEL_BOOK_TREE.CHECKIN_CHECKOUT_DATE_QUESTION'));
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(userLevel).toBe(level);
        });
    });

    describe('LEVEL 6 - Asked filters, respond to "Deals" and set suggestions', () => {
        it('hit the api once deals get correct response for deals and move to level 7', () => {
            let text = i18n.__('HOTEL_BOOK_TREE.SHOW_DEALS_MESSAGE'),
                choice5 = ['2017-01-06', '2017-01-10'],
                choice2 = { dest_type: 'country', dest_id: '-1' },
                choice4 = { dest_type: 'city', dest_id: '-100' }, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes(),
                level = 6, next = 7;

            sandbox.spy(tree[level], '_getPendingMessages');
            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 5, choice5);
            storage.setUserChoice(userId, 2, choice2);
            storage.setUserChoice(userId, 4, choice4);
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(tree[level]._getPendingMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(apiHelpers.getHotelAvailabilityForDeals.callCount).toBe(1);
            expect(apiHelpers.getHotelAvailabilityForDeals.calledWithMatch({
                checkin: choice5[0],
                checkout: choice5[1],
                dest_type: choice4.dest_type,
                dest_id: choice4.dest_id,
                lang: 'en'
            })).toBe(true);
            expect(storage.getUserData(userId).suggestion[level]).toBeA(Object);
            expect(tree[next]._getLineMessages.calledOnce).toBe(true);
            expect(userLevel).toBe(next);
        });

        it('should ask me filters, get correct response for budgets and move to level 8', () => {
            let text = i18n.__('HOTEL_BOOK_TREE.SHOW_BY_BUDGET_MESSAGE'),
                req = testHelpers.getTextReq('',''), res = testHelpers.getRes(),
                level = 6, next = 8;

            storage.setConversationLevel(userId, level);
            logic(testHelpers.getTextReq(text, userId), testHelpers.getRes(), nextSpy, storage);

            expect(tree[next]._getLineMessages.calledOnce).toBe(true);
            expect(apiHelpers.getHotelAvailabilityForDeals.callCount).toBe(0);

            expect(chatHelpers.sendReplyMessage.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should send error message if choice isnt any of them', () => {
            let text = 'error', level = 6, req = testHelpers.getTextReq('',''), res = testHelpers.getRes();
            storage.setConversationLevel(userId, level);
            logic(testHelpers.getTextReq(text, userId), testHelpers.getRes(), nextSpy, storage);

            expect(tree[level]._getErrorMessages.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });

        it('should send no deals error message if suggestions come back as empty', () => {
            apiHelpers.getHotelAvailabilityForDeals.restore();
            let req = testHelpers.getTextReq('', '');
            let res = testHelpers.getRes();
            let text = i18n.__('HOTEL_BOOK_TREE.SHOW_DEALS_MESSAGE'),
                level = 6,
                choice5 = ['string', 'string'],
                choice2 = { dest_id: 'id' };

            sandbox.spy(tree[level], '_getNoDealsErrorMessages');

            sandbox.stub(apiHelpers, 'getHotelAvailabilityForDeals').callsFake(() => {
                return {
                    then: (cb) => { cb({ checkout: '2017-12-15', checkin: '2017-12-13', city_ids: ['-1'], other_currency: 'THB', hotels: [] });
                    return { catch: () => {} } }
                }
            });

            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 2, choice2);
            storage.setUserChoice(userId, 5, choice5);
            logic(testHelpers.getTextReq(text, userId), testHelpers.getRes(), nextSpy, storage);

            expect(tree[level]._getNoDealsErrorMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });
    });

    describe('LEVEL 7 - Hotels Deals Carousel', () => {
        it('should react to "Search again" and bring you back to level 1', () => {
            let text = i18n.__('HOTEL_BOOK_TREE.SEARCH_AGAIN_ACTION_TEXT_300CH'),
                level = 7, next = 1, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes();

            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 2, 'choice');
            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(tree[next]._getLineMessages.callCount).toBe(1);
            expect(storage.getUserData(userId).choice).toEqual({});
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should send thank you message when not search again', () => {
            let text = 'Error', level = 7, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes();

            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
        });
    });

    describe('LEVEL 8 - Ask Budget Range', () => {
        it('should respond to the messages correctly when they are in the choices', () => {
            let text = i18n.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_TITLE_40CH', {
                    min: CONST.BUDGET_TIERS.TIER1.MIN, max: CONST.BUDGET_TIERS.TIER1.MAX }),
                choice5 = ['2017-06-07', '2017-06-08'],
                choice2 = { dest_type: 'city' },
                level = 8, next = 9, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes();

            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 5, choice5);
            storage.setUserChoice(userId, 2, choice2);
            sandbox.spy(tree[level], '_getPendingMessages');
            logic(req, res, nextSpy, storage);

            let userLevel = storage.getUserData(userId).conversationLevel;

            expect(tree[next]._getLineMessages.callCount).toBe(1);
            expect(apiHelpers.getHotelAvailabilityForBudget.callCount).toBe(1);
            expect(tree[level]._getPendingMessages.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(storage.getUserData(userId).choice[level]).toBe('TIER1');
            expect(userLevel).toBe(next);
        });

        it('should send an error message when the choice is not valid, stay in level and ask again', () => {
            let text = 'error', level = 8, req = testHelpers.getTextReq('',''), res = testHelpers.getRes();
            storage.setConversationLevel(userId, level);
            logic(testHelpers.getTextReq(text, userId), testHelpers.getRes(), nextSpy, storage);

            expect(tree[8]._getErrorMessages.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledOnce).toBe(true);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(tree[level]._getLineMessages.calledOnce).toBe(true);
            expect(storage.getUserData(userId).conversationLevel).toBe(level);
        });
    });

    describe('LEVEL 9 - Hotel Carousel for Budget Range', () => {
        it('should react to "Search again" and bring you back to level 1', () => {
            let text = i18n.__('HOTEL_BOOK_TREE.SEARCH_AGAIN_ACTION_TEXT_300CH'),
                level = 9, next = 1, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes();

            storage.setConversationLevel(userId, level);
            storage.setUserChoice(userId, 2, 'choice');
            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
            expect(tree[next]._getLineMessages.callCount).toBe(1);
            expect(storage.getUserData(userId).choice).toEqual({});
            expect(storage.getUserData(userId).conversationLevel).toBe(next);
        });

        it('should send thank you message when not search again', () => {
            let text = 'Error', level = 9, req = testHelpers.getTextReq(text, userId), res = testHelpers.getRes();

            storage.setConversationLevel(userId, level);
            logic(req, res, nextSpy, storage);
            expect(chatHelpers.sendReplyMessage.calledWith(req, res, sinon.match.array)).toBe(true);
            expect(chatHelpers.sendReplyMessage.callCount).toBe(1);
        });
    });
});