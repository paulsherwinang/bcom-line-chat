'use strict';

const lineHelpers = require('../../helpers/lineMessageHelpers');
const expect = require('expect');

describe('lineHelpers', () => {
    it('getActionMessage should return correct object and truncate things correctly', () => {
        let label = new Array(40+1).join('#');
        let text = new Array(500+1).join('#');

        let message = lineHelpers.getActionMessage(label, text);
        expect(message)
            .toIncludeKeys(['label', 'text']);

        expect(message.label.length).toBe(20);
        expect(message.text.length).toBe(300);
    });

    it('getTemplateButtons should return correct object', () => {
        let text = 'text', actions = ['hi'];

        expect(lineHelpers.getTemplateButtons(actions, text, 'title'))
            .toExcludeKeys(['thumbnailImageUrl']);

        expect(lineHelpers.getTemplateButtons(actions, text))
            .toIncludeKeys(['actions', 'text'])
            .toExcludeKeys(['title', 'thumbnailImageUrl']);

        expect(() => { lineHelpers.getTemplateButtons([], '') })
            .toThrow();

        expect(() => { lineHelpers.getTemplateButtons(undefined, text) })
            .toThrow();
    });

    it('getCarouselColumn should return correct object', () => {
        let text = 'text', actions = ['hi'];

        expect(lineHelpers.getCarouselColumn(actions, text, 'title'))
            .toExcludeKeys(['thumbnailImageUrl']);

        expect(lineHelpers.getCarouselColumn(actions, text))
            .toIncludeKeys(['actions', 'text'])
            .toExcludeKeys(['title', 'thumbnailImageUrl']);

        let titleLong = new Array(50+1).join('#');
        let textLong = new Array(70+1).join('#');

        let col = lineHelpers.getCarouselColumn(['actions'], textLong, titleLong);
        expect(col.title.length).toBeLessThanOrEqualTo(40);
        expect(col.text.length).toBeLessThanOrEqualTo(60);
    })
});