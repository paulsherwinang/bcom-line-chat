'use strict';

const chatHelpers = require('../../helpers/chatHelpers');
const apiHelpers = require('../../helpers/apiHelpers');
const testHelpers = require('../testHelpers');
const sinon = require('sinon');
const expect = require('expect');
const i18n = require('i18n');
const CONSTANTS = require('../../constants');

let sandbox;

let req = testHelpers.getTextReq('','');

describe('chatHelpers', () => {

    beforeEach(() => { sandbox = sinon.sandbox.create() });
    afterEach(() => { sandbox.restore(); });

    it('_getDurationFromResponse should get number of nights correctly', () => {
        let response = { checkout: '2017-12-15', checkin: '2017-12-13' };
        expect(chatHelpers.deals._getDurationFromResponse(response)).toBe(2);
    });

    it('deals.getMessage should create carousel object from response object', () => {

        let location = { city_name: 'Hello' };
        let blockData = [
            {
                block: [{
                    rack_rate: {
                        other_currency: {
                            price: '100.00'
                        }
                    },
                    min_price: {
                        other_currency: {
                            price: '80.00'
                        }
                    }
                }]
            }
        ];
        let response = {
            city_ids: ['-1'],
            checkout: '2017-12-15',
            checkin: '2017-12-13',
            other_currency: 'THB',
            hotels: [
                {
                    review_score: '8.9', review_score_word: 'Very Good', review_nr: 123,
                    photo: "http://aff.bstatic.com/images/hotel/max500/340/34007512.jpg",
                    price: '200.50', hotel_name: 'Hotel 1',
                    hotel_url: "https://www.booking.com/hotel/sg/mandarin.en.html?checkin=2017-12-01&checkout=2017-12-07&room1=A%2CA",
                    room_min_price: {
                        block_id: 'hhh'
                    }
                }
            ]
        };

        sandbox.stub(apiHelpers, 'getBlockAvailability').callsFake(() => {
            return {
                then: (cb) => { cb(blockData); }
            }
        });

        let msgProm = chatHelpers.deals.getMessage(response, req, location);

        return msgProm.then((message) => {
            let seeHotelAction = message[0].template.columns[0].actions[0];
            let reviewAction = message[0].template.columns[0].actions[1];
            let seeDealsAction = message[0].template.columns[1].actions[0];

            expect(message[0].template.columns[0].title).toContain(response.hotels[0].hotel_name);
            expect(message[0].template.columns[0].text).toContain('-20%');

            expect(seeHotelAction.uri)
                .toContain(`${CONSTANTS['DEV'].LINE_BASEURI}`)
                .toContain('aid='+CONSTANTS.AID_DEEP_LINK)
                .toContain('room1='+encodeURIComponent(CONSTANTS.DEFAULT_GUEST_QUANTITY))
                .toContain(`label=${CONSTANTS.BCOM_LABELS.DEALS_CAROUSEL_SEE_HOTEL}`)
                .toContain('selected_currency='+CONSTANTS.DEFAULT_CURRENCY);

            expect(reviewAction.uri)
                .toContain(`${CONSTANTS['DEV'].LINE_BASEURI}`)
                .toContain('aid='+CONSTANTS.AID_DEEP_LINK)
                .toContain('room1='+encodeURIComponent(CONSTANTS.DEFAULT_GUEST_QUANTITY))
                .toContain(`label=${CONSTANTS.BCOM_LABELS.DEALS_CAROUSEL_SEE_REVIEWS}`)
                .toContain('activeTab=htReviews');

            expect(seeDealsAction.uri)
                .toContain(`${CONSTANTS['DEV'].LINE_BASEURI}`)
                .toContain('lang='+CONSTANTS['DEV'].DEFAULT_LOCALE)
                .toContain('aid='+CONSTANTS.AID_CHATBOT)
                .toNotContain('nflt')
                .toContain(`label=${CONSTANTS.BCOM_LABELS.DEALS_CAROUSEL_SEE_DEALS}`)
                .toContain('room1='+encodeURIComponent(CONSTANTS.DEFAULT_GUEST_QUANTITY))
                .toContain('checkin='+response.checkin)
                .toContain('checkout='+response.checkout)
        });
    });

    it('budget.getMessage should create carousel object from response object', () => {
        let response = {
            city_ids: ['-1'],
            checkout: '2017-12-15',
            checkin: '2017-12-13',
            other_currency: 'THB',
            hotels: [
                {
                    review_score: '8.9', review_score_word: 'Very Good', review_nr: 123,
                    photo: "http://aff.bstatic.com/images/hotel/max500/340/34007512.jpg", price: '200.50', hotel_name: 'Hotel 1',
                    hotel_url: "https://www.booking.com/hotel/sg/mandarin.en.html?checkin=2017-12-01&checkout=2017-12-07&room1=A%2CA",
                }
            ]
        };

        let location = { city_name: 'hello' };
        let tierKey = 'TIER1';
        let msgProm = chatHelpers.budget.getMessage(response, tierKey, req, location);

        return msgProm.then((message) => {
            let seeHotelAction = message[0].template.columns[0].actions[0];
            let reviewAction = message[0].template.columns[0].actions[1];
            let seeHotelsInBudgetAction = message[0].template.columns[1].actions[0];

            expect(message[0].template.columns[0].title).toContain(response.hotels[0].hotel_name);
            expect(message[0].template.columns[0].text).toContain('100.25');

            expect(seeHotelAction.uri)
                .toContain(`${CONSTANTS['DEV'].LINE_BASEURI}`)
                .toContain('aid='+CONSTANTS.AID_DEEP_LINK)
                .toContain('room1='+encodeURIComponent(CONSTANTS.DEFAULT_GUEST_QUANTITY))
                .toContain(`label=${CONSTANTS.BCOM_LABELS.BUDGET_CAROUSEL_SEE_HOTEL}`)
                .toContain('selected_currency='+CONSTANTS.DEFAULT_CURRENCY);

            expect(reviewAction.uri)
                .toContain(`${CONSTANTS['DEV'].LINE_BASEURI}`)
                .toContain('aid='+CONSTANTS.AID_DEEP_LINK)
                .toContain('room1='+encodeURIComponent(CONSTANTS.DEFAULT_GUEST_QUANTITY))
                .toContain(`label=${CONSTANTS.BCOM_LABELS.BUDGET_CAROUSEL_SEE_REVIEWS}`)
                .toContain('activeTab=htReviews');

            expect(seeHotelsInBudgetAction.uri)
                .toContain(`${CONSTANTS['DEV'].LINE_BASEURI}`)
                .toContain('lang='+CONSTANTS['DEV'].DEFAULT_LOCALE)
                .toContain('aid='+CONSTANTS.AID_CHATBOT)
                .toContain('room1='+encodeURIComponent(CONSTANTS.DEFAULT_GUEST_QUANTITY))
                .toContain('checkin='+response.checkin)
                .toContain(`label=${CONSTANTS.BCOM_LABELS.BUDGET_CAROUSEL_SEE_BUDGET}`)
                .toContain('nflt='+encodeURIComponent('pri=1'));
        });
    });

    it('getSrUrlWithDates should return me correct srUrl', () => {
        let userData = {
            choice: {
                '2': { url: 'http://booking.com?aid=xxx' },
                '5': ['2017-05-05', '2017-06-06']
            }
        };

        expect(chatHelpers.getSrUrlWithDates(userData))
            .toContain('checkin=2017-05-05')
            .toContain('checkout=2017-06-06');
    });

    it('deals._getSeeDealsAction should transform queries correctly', () => {
         let res1 = { region_ids: ['1234'] };

        expect(chatHelpers.deals._getSeeDealsAction(res1, {}, testHelpers.getTextReq('', '')).uri)
            .toContain('dest_type=region')
            .toContain('dest_id=1234');

        let res2 = { city_ids: ['1234'] };

        expect(chatHelpers.deals._getSeeDealsAction(res2, {}, testHelpers.getTextReq('', '')).uri)
            .toContain('dest_type=city')
            .toContain('dest_id=1234');

        let res3 = { district_ids: ['1234'] };

        expect(chatHelpers.deals._getSeeDealsAction(res3, {}, testHelpers.getTextReq('', '')).uri)
            .toContain('dest_type=district')
            .toContain('dest_id=1234');

    });

    it('destFinder._getLink should get you correct link', () => {
        let q = {
            tag: '123',
            dest_type: 'country',
            dest_id: '168',
            checkin:  '2017-05-05',
            checkout: '2017-05-06'
        }, req = testHelpers.getTextReq('hi');

        let link = chatHelpers.destFinder._getLink(req, q);
        expect(link)
            .toContain(`${CONSTANTS['DEV'].LINE_BASEURI}`)
            .toContain(`aid=${CONSTANTS.AID_CHATBOT}`)
            .toContain(`tag=${q.tag}`)
            .toContain(`dest_type=${q.dest_type}`)
            .toContain(`dest_id=${q.dest_id}`)
            .toContain(`checkin=${q.checkin}`)
            .toContain(`checkout=${q.checkout}`);

        let q2 = {
            tag: '123',
            dest_type: 'country',
            dest_id: '168',
            checkin:  '',
            checkout: ''
        }, req2 = testHelpers.getTextReq('hi');

        let link2 = chatHelpers.destFinder._getLink(req2, q2);

        expect(link2)
            .toContain(`${CONSTANTS['DEV'].LINE_BASEURI}`)
            .toContain(`aid=${CONSTANTS.AID_CHATBOT}`)
            .toContain(`tag=${q.tag}`)
            .toContain(`dest_type=${q2.dest_type}`)
            .toContain(`dest_id=${q2.dest_id}`)
            .toNotContain(`checkin=${q2.checkin}`)
            .toNotContain(`checkout=${q2.checkout}`);
    });

    it.skip('themes.getMessage should get correct messages', () => {
        let activities = [{dest_type: '1', label: 'Hi!'}, {dest_type: '2', label: 'hi!'}],
            req = testHelpers.getTextReq('', '');

        let messages = chatHelpers.themes.getMessages(activities, req);

        expect(messages).toBeA('array');
        expect(messages[0].template.columns.length).toBe(3);

        messages[0].template.columns.forEach((column) => {
            expect(column).toIncludeKeys(['text', 'actions']);
            column.actions.forEach((action) => {
                expect(action).toIncludeKeys(['type', 'label', 'text']);
            });
        });
    });

});