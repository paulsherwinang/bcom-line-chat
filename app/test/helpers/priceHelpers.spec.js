'use strict';

const priceHelpers = require('../../helpers/priceHelpers');
const expect = require('expect');

describe('priceHelpers', () => {
    it('should fetch correct discountRate from original and final price', () => {
        expect(priceHelpers.getDiscountRate('100.00', '80.00')).toBe('-20%');
    });
});