'use strict';

const sinon = require('sinon');
const expect = require('expect');
const middleware = require('../../middleware/validateAccessToken');
const apiHelpers = require('../../helpers/apiHelpers');
const testHelpers = require('../testHelpers');
const line = require('node-line-bot-api');

let sandbox;
let nextSpy;
let response;

let res = testHelpers.getRes();

describe('validateAccessToken middleware', () => {
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        sandbox.useFakeTimers();

        nextSpy = sandbox.spy();
        response = { access_token: "xxx", "expires_in": 200, "token_type": "Bearer" };

        sandbox.stub(apiHelpers, 'getNewAccessToken').callsFake(() => {
            return { then: (cb) => { cb(response); return { catch: () => {} } } };
        });
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('should check if accesstoken is valid. if not, request a new one', () => {
        let req = testHelpers.getTextReq('','');

        middleware(req, res, nextSpy);

        expect(apiHelpers.getNewAccessToken.callCount).toBe(1);
        expect(nextSpy.callCount).toBe(1);
    });

    it('successful accesstoken request should be invalid after response.expires_in', () => {
        sinon.spy(line, 'init');

        let req = testHelpers.getTextReq('', '');

        middleware(req, res, nextSpy);
        expect(apiHelpers.getNewAccessToken.callCount).toBe(0);
        expect(line.init.callCount).toBe(0);
        expect(nextSpy.callCount).toBe(1);
        sandbox.clock.tick(100);
        expect(apiHelpers.getNewAccessToken.callCount).toBe(0);

        sandbox.clock.tick(101);
        middleware(req, res, nextSpy);
        expect(apiHelpers.getNewAccessToken.callCount).toBe(1);
        expect(line.init.callCount).toBe(1);
        expect(nextSpy.callCount).toBe(2);
    });
});