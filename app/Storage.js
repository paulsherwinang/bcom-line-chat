'use strict';

const _ = require('lodash');
const constants = require('./constants');
const debug = require('debug')('linebot:storage');
const EventEmitter = require('events').EventEmitter;
const CONFIG = process.env.NODE_ENV === 'production' ? constants['PROD'] : constants['DEV'];

module.exports = class Storage {
    constructor(store) {
        this.store = !store ? {} : store;
        this.ttl = 10*60*1000;
        this.eventEmitter = new EventEmitter();
    }

    static getEmptyUser () {
        return {
            timeout: null,
            track: 'NONE',
            locale: CONFIG.DEFAULT_LOCALE,
            conversationLevel: 0,
            suggestion: {},
            choice: {}
        };
    }

    isUserInStore (userId) {
        return _.has(this.store, userId);
    }

    setUser (userId) {
        if(_.has(this.store, userId)) return;
        this.store[userId] = Storage.getEmptyUser();
    }

    removeUser (userId) {
        this.store[userId] = Storage.getEmptyUser();
    }

    resetUserState (userId) {
        debug('reset user state');
        this.removeUserTrack(userId);
        this.removeUserChoices(userId);
        this._removeUserSuggestions(userId);
        this.setConversationLevel(userId, 0);
    }

    removeUserTrack (userId) {
        this.store[userId].track = 'NONE';
    }

    removeUserChoices (userId) {
        this.store[userId].choice = {};
    }

    _removeUserSuggestions(userId){
        this.store[userId].suggestion = {};
    }

    getUserData (userId){
        if(!_.has(this.store, userId)) return null;
        return this.store[userId];
    }

    _setUserValue (userId, path, value) {
        debug('updated user: %s, key: %s, value: %j', userId, path, value);
        let userData = this.getUserData(userId);
        let startTimer = () => {
            userData.timeout = setTimeout(() => {
                this.eventEmitter.emit(constants.EVENTS.STORAGE_TTL_EXPIRED, userId);
            }, this.ttl);
        };

        clearTimeout(userData.timeout);
        startTimer();
        _.set(this.store[userId], path, value);
    }

    setUserLocale (userId, locale) {
        this._setUserValue(userId, ['locale'], locale);
    }

    setUserTrack (userId, track) {
        this._setUserValue(userId, ['track'], track);
    }

    setConversationLevel (userId, level) {
        if(!this.isUserInStore(userId)) return;
        this._setUserValue(userId, ['conversationLevel'], level);
    }

    setUserChoice (userId, level, choice) {
        if(!this.isUserInStore(userId)) return;
        this._setUserValue(userId, ['choice', level], choice);
    }

    setUserSuggestion (userId, level, suggestions) {
        this._setUserValue(userId, ['suggestion', level], suggestions);
    }
};