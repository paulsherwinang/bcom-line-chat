'use strict';

const line = require('node-line-bot-api');
const _ = require('lodash');
const i18n = require('i18n');
const moment = require('moment');
const truncate = require('truncate');
const numeral = require('numeral');
const debug = require('debug')('linebot:chatHelpers');
const querystring = require('querystring');
const apiHelpers = require('../helpers/apiHelpers');
const lineHelpers = require('./lineMessageHelpers');
const urlHelpers = require('../helpers/urlHelpers');
const priceHelpers = require('../helpers/priceHelpers');
const CONSTANTS = require('../constants');
const punycode = require('punycode');
const CONFIG = process.env.NODE_ENV === 'production' ? CONSTANTS['PROD'] : CONSTANTS['DEV'];

module.exports = {

    sendReplyMessage: (req, res, lineMessages) => {
        debug('[PENDING] replyMessage request: %j', lineMessages);
        line.client.replyMessage({
            replyToken: req.body.events[0].replyToken,
            messages: lineMessages
        }).then((response) => {
            debug('[SUCCESS] replyMessage response success: %j', response);
        }).catch((err) => {
            if(err){
                let error = err.response.text || err.response;
                req.visitor.exception(`sendReplyMessage has come back with an error while sending: ${JSON.stringify(lineMessages)}. reason: ${JSON.stringify(error)}`, true).send();
                debug('[ERROR] replyMessage error: %s, messages: %j', error, lineMessages);
            }
        });
    },

    pushMessage: (req, res, lineMessages) => {
        debug('[PENDING] pushMessage request: %j', lineMessages);

        line.client.pushMessage({
            to: req.body.events[0].source.userId,
            messages: lineMessages
        }).then((response) => {
            debug('[SUCCESS] pushMessage response success: %O', response);
        }).catch((err) => {
            if(err){
                let error = err.response.text || err.response;
                req.visitor.exception(`pushMessage has come back with an error while sending: ${JSON.stringify(lineMessages)}. reason: ${JSON.stringify(error)}`, true).send();
                debug('[FAILED] pushMessage error: %s, messages: %O', error, lineMessages);
            }
        });
    },

    initialMessage: {
        getMessage: function(req){
            return [{
                "type": "template",
                "altText": req.__('HOW_CAN_I_HELP_YOU'),
                "template": this._getTemplate(req)
            }];
        },
        _getTemplate: function(req) {
            let hotelBookTrigger = this._getHotelBookingTriggerAction(req);
            let discoverTrigger = this._getDiscoverTriggerAction(req); //TODO disabled till discover works

            return lineHelpers.getTemplateButtons(
                [hotelBookTrigger],
                req.__('CHOOSE_BELOW_TEXT'),
                req.__('HOW_CAN_I_HELP_YOU')
            );
        },
        _getDiscoverTriggerAction: function(req) {
            return lineHelpers.getActionMessage(
                req.__('PASSION_DISCOVER_TREE_LABEL_20CH'),
                req.__('PASSION_DISCOVER_TREE_TRIGGER_TEXT')
            )
        },
        _getHotelBookingTriggerAction: function (req) {
            return lineHelpers.getActionMessage(
                req.__('HOTEL_BOOK_TREE_LABEL_20CH'),
                req.__('HOTEL_BOOK_TREE_TRIGGER_TEXT')
            );
        }
    },

    getErrorMessage: () => {
        return [
            lineHelpers.getMessageText('Error message here')
        ]
    },

    getFarewellMessages: (req) => {
        let message = lineHelpers.getMessageText(req.__('INACTIVE_THANK_YOU_MESSAGE'));
        return [message];
    },

    getSrUrlWithDates: function(userData){
        let sUrl = _.isEmpty(userData.choice[4]) ? _.get(userData.choice[2], 'url') : _.get(userData.choice[4], 'url');
        let q = { checkin: userData.choice[5][0], checkout: userData.choice[5][1] };

        return sUrl+'&'+querystring.stringify(q);
    },

    getLocationSelectedMessage: (req, res, choice) => {
        return lineHelpers.getMessageText(
            req.__('SELECTED_LOCATION_MESSAGE', {
                label: choice.label
            })
        );
    },

    budget: {
        getMessage: function(response, tierKey, req, locationChoice){
            return new Promise ((resolve, reject) => {
                let duration = this._getDurationFromResponse(response);
                let urlQueries = { selected_currency: CONSTANTS.DEFAULT_CURRENCY };
                let colPromises = [];

                response.hotels.forEach((hotel) => {
                    let colProm = this._getHotelColumn(hotel, urlQueries, req, duration, response, locationChoice);
                    colPromises.push(colProm);
                });

                Promise.all(colPromises).then((columns) => {
                    let lastColumn = this._getBcomLastColumn(response, urlQueries, tierKey, req);
                    columns.push(lastColumn);

                    let messages = [
                        {
                            type: "template",
                            altText: req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_ALTTEXT_140CH'),
                            template: {
                                type: 'carousel',
                                columns: columns
                            }
                        }
                    ];

                    resolve(messages);
                });
            });
        },
        _getHotelColumn: function(hotel, urlQueries, req, duration, response, locationChoice) {
            return new Promise ((resolve, reject) => {
                apiHelpers.getDistrictHotels([hotel.hotel_id])
                    .then((districtHotels) => {
                        let districtId = districtHotels[0].district_id;
                        return apiHelpers.getDistricts([districtId])
                    })
                    .catch(() => {
                        return null;
                    })
                    .then((districts) => {
                        if(districts){
                            return districts[0].name;
                        } else {
                            return locationChoice.city_name || locationChoice.region;
                        }
                    })
                    .then((locationName) => {
                        hotel['location_name'] = locationName;

                        let ppn = parseFloat(hotel.price) / duration;
                        let seeHotelAction = this._getSeeThisHotelAction(hotel, urlQueries, req);
                        let seeReviewsActon = this._getSeeReviewsAction(hotel, urlQueries, req);

                        let text = req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_TEXT_60CH', {
                            review_score: hotel.review_score,
                            review_score_word: hotel.review_score_word,
                            hotel_location: hotel.location_name,
                            supplementary_info: req.__('PRICE_PER_NIGHT', {
                                currency: response.other_currency,
                                price_per_night: numeral(ppn).format('0,0.00')
                            })
                        });

                        let title = req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_TITLE_40CH', {
                            star_rating: _.padEnd('', hotel.stars,  "⭐️"),
                            hotel_name: hotel.hotel_name
                        });

                        let column = lineHelpers.getCarouselColumn(
                            [seeHotelAction, seeReviewsActon],
                            text, title,
                            hotel.photo
                        );

                        resolve(column);
                    });
            });

        },
        _getBcomLastColumn: function(response, urlQueries, tierKey, req) {
            let seeHotelsInBudgetAction = this._getSeeHotelsInBudgetAction(response, urlQueries, tierKey, req);
            let searchAgainAction = this._getSearchAgainAction(req);

            return lineHelpers.getCarouselColumn(
                [seeHotelsInBudgetAction, searchAgainAction],
                req.__('HOTEL_BOOK_TREE.BCOM_SEE_HOTELS_BUDGET_CAROUSEL_ENTRY_TEXT'),
                req.__('HOTEL_BOOK_TREE.BCOM_SEE_HOTELS_BUDGET_TITLE_40CH'),
                `${CONFIG.STATIC_ASSETS_HOSTNAME_HTTPS}/images/booking-carousel-banner.jpg`
            );
        },
        _getSeeHotelsInBudgetAction: function(response, urlQueries, tierKey, req){
            let q = {
                lang: req.getLocale(),
                aid: CONSTANTS.AID_CHATBOT,
                checkin: response.checkin,
                checkout: response.checkout,
                room1: CONSTANTS.DEFAULT_GUEST_QUANTITY,
                label: CONSTANTS.BCOM_LABELS.BUDGET_CAROUSEL_SEE_BUDGET
            };

            let tier = tierKey.substr(tierKey.length -1);

            Object.assign(q, urlQueries, { nflt: 'pri='+tier }, this._getDestObj(response));
            let srUrl = CONFIG.BCOM_SR_BASEURL_HTTPS+'?'+querystring.stringify(q);
            srUrl = urlHelpers.replaceBaseUrlWithLineUri(srUrl);

            return lineHelpers.getActionUri(
                req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_LABEL_20CH'),
                srUrl
            );
        },
        _getSeeThisHotelAction: (hotel, urlQueries, req) => {
            let q = {
                aid: CONSTANTS.AID_DEEP_LINK,
                label: CONSTANTS.BCOM_LABELS.BUDGET_CAROUSEL_SEE_HOTEL
            };
            Object.assign(q, urlQueries);
            let hotelPageUrl = hotel.hotel_url + '&' + querystring.stringify(q);
            hotelPageUrl = urlHelpers.replaceBaseUrlWithLineUri(hotelPageUrl);

            return lineHelpers.getActionUri(
                req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_DETAILS_ACTION_LABEL_20CH'),
                hotelPageUrl
            );
        },
        _getSeeReviewsAction: (hotel, urlQueries, req) => {
            let q = {
                aid: CONSTANTS.AID_DEEP_LINK,
                activeTab: 'htReviews',
                label: CONSTANTS.BCOM_LABELS.BUDGET_CAROUSEL_SEE_REVIEWS
            };
            Object.assign(q, urlQueries);
            let tabReviewsUrl = hotel.hotel_url+'&'+querystring.stringify(q);
            tabReviewsUrl = urlHelpers.replaceBaseUrlWithLineUri(tabReviewsUrl);

            return lineHelpers.getActionUri(
                req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_REVIEWS_ACTION_LABEL_20CH'),
                tabReviewsUrl
            );
        },
        _getSearchAgainAction: (req) => {
            return lineHelpers.getActionMessage(
                req.__('HOTEL_BOOK_TREE.SEARCH_AGAIN_LABEL_20CH'),
                req.__('HOTEL_BOOK_TREE.SEARCH_AGAIN_ACTION_TEXT_300CH')
            );
        },
        _getDurationFromResponse: (response) => {
            let checkin = moment(response.checkin, 'YYYY-MM-DD');
            let checkout = moment(response.checkout, 'YYYY-MM-DD');

            return checkout.diff(checkin, 'days');
        },
        _getDestObj: (response) => {
            let obj = {};

            if ('region_ids' in response) {
                obj.dest_id = response.region_ids[0];
                obj.dest_type = 'region';
            } else if ('city_ids' in response) {
                obj.dest_id = response.city_ids[0];
                obj.dest_type = 'city';
            } else if ('district_ids' in response) {
                obj.dest_id = response.district_ids[0];
                obj.dest_type = 'district';
            }

            return obj;
        }
    },
    locationAutocomplete: {
        getMessage: function(locations, req){
            if(locations.length > 4) locations = _.take(locations, 4);
            let columns = [];

            locations.forEach((location) => {
                let locationColumn = this._getLocationColumn(location, req);
                columns.push(locationColumn);
            });

            let lastColumn = this._getNoneLastColumn(req);
            columns.push(lastColumn);

            return [
                {
                    "type": "template",
                    "altText": req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_ALTEXT_140CH'),
                    "template": {
                        "type": "carousel",
                        "columns": columns
                    }
                }
            ];
        },
        _getLocationColumn: function(suggestion, req){
            let selectAction = this._getLocationSelectAction(suggestion, req);

            let columnText = req.__mf('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_SUGGESTION_CARD_TEXT_120CH', {
                dest_type: suggestion.dest_type,
                hotel_number: suggestion.hotels
            });

            let columnTitle = req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_SUGGESTION_CARD_TITLE_40CH', {
                dest_name: suggestion.label
            });

            return lineHelpers.getCarouselColumn(
                [selectAction],
                columnText,
                columnTitle
            );
        },
        _getLocationSelectAction: function(suggestion, req){
            return lineHelpers.getActionMessage(
                req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_SELECT_ACTION_LABEL_20CH'),
                req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_SELECT_ACTION_TEXT_300CH', { label: suggestion.label })
            );
        },
        _getNoneLastColumn: function(req){
            let noneSelectAction = this._getNoneSelectAction(req);
            let text = req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_NONE_CARD_TEXT_120CH');
            let title = req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_NONE_CARD_TITLE_40CH');

            return lineHelpers.getCarouselColumn(
                [noneSelectAction], text, title

            );
        },
        _getNoneSelectAction: function(req) {
            return lineHelpers.getActionMessage(
                req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_NONE_ACTION_LABEL_20CH'),
                req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_NONE_ACTION_TEXT_300CH')
            );
        }
    },
    deals: {
        getMessage: function(response, req, locationChoice){
            return new Promise((resolve, reject) => {
                let duration = this._getDurationFromResponse(response);
                let urlQueries = { selected_currency: CONSTANTS.DEFAULT_CURRENCY };
                let colPromises = [];

                response.hotels.forEach((hotel) => {
                    let colPromise = this._getHotelColumn(hotel, urlQueries, req, duration, response, locationChoice);
                    colPromises.push(colPromise);
                });

                Promise.all(colPromises).then((columns) => {
                    let lastColumn = this._getBcomLastColumn(response, urlQueries, req);
                    columns.push(lastColumn);

                    let messages = [
                        {
                            type: "template",
                            altText: req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_ALTTEXT_140CH'),
                            template: {
                                type: 'carousel',
                                columns: columns
                            }
                        }
                    ];

                    resolve(messages);
                });
            });

        },
        _getHotelColumn: function(hotel, urlQueries, req, duration, response, locationChoice) {
            return new Promise((resolve, reject) => {
                apiHelpers.getDistrictHotels([hotel.hotel_id])
                    .then((districtHotels) => {
                        let districtId = districtHotels[0].district_id;
                        return apiHelpers.getDistricts([districtId])
                    })
                    .catch(() => {
                        return null;
                    })
                    .then((districts) => {
                        if(districts){
                            return districts[0].name;
                        } else {
                            return locationChoice.city_name || locationChoice.region;
                        }
                    })
                    .then((locationName) => {
                        hotel['location_name'] = locationName;

                        return apiHelpers.getBlockAvailability({
                            arrival_date: response.checkin,
                            departure_date: response.checkout,
                            hotel_ids: hotel.hotel_id,
                            block_id: hotel.room_min_price.block_id,
                            include_rack_rates: 1,
                            limit_incremental_price: 1,
                            currency_code: CONSTANTS.DEFAULT_CURRENCY
                        });
                    })
                    .then((blockData) => {
                        let original = blockData[0].block[0].rack_rate.other_currency.price;
                        let final = blockData[0].block[0].min_price.other_currency.price;
                        let discount = this._getDiscountRate(original, final);
                        let seeThisHotelAction = this._getSeeThisHotelAction(hotel, urlQueries, req);
                        let seeReviewsAction = this._getSeeReviewsAction(hotel, urlQueries, req);

                        let text = req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_TEXT_60CH', {
                            review_score: hotel.review_score,
                            review_score_word: hotel.review_score_word,
                            hotel_location: hotel.location_name,
                            supplementary_info: discount ? req.__("PERCENT_OFF_TODAY", { discount_rate: discount }) : ''
                        });

                        let title = req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_TITLE_40CH', {
                            star_rating: _.padEnd('', hotel.stars,  "⭐️"),
                            hotel_name: hotel.hotel_name
                        });

                        let column = lineHelpers.getCarouselColumn(
                            [seeThisHotelAction, seeReviewsAction],
                            text, title,
                            hotel.photo
                        );

                        resolve(column);
                    });
            });
        },
        _getBcomLastColumn: function(response, urlQueries, req) {
            let seeOtherDealsAction = this._getSeeDealsAction(response, urlQueries, req);
            let searchAgainAction = this._getSearchAgainAction(req);

            return lineHelpers.getCarouselColumn(
                [seeOtherDealsAction, searchAgainAction],
                req.__('HOTEL_BOOK_TREE.BCOM_SEE_HOTELS_BUDGET_CAROUSEL_ENTRY_TEXT'),
                req.__('HOTEL_BOOK_TREE.BCOM_SEE_HOTELS_BUDGET_TITLE_40CH'),
                `${CONFIG.STATIC_ASSETS_HOSTNAME_HTTPS}/images/booking-carousel-banner.jpg`
            );
        },
        _getSeeDealsAction: function(response, urlQueries, req){
            let q = {
                lang: req.getLocale(),
                aid: CONSTANTS.AID_CHATBOT,
                checkin: response.checkin,
                checkout: response.checkout,
                room1: CONSTANTS.DEFAULT_GUEST_QUANTITY,
                label: CONSTANTS.BCOM_LABELS.DEALS_CAROUSEL_SEE_DEALS
            };

            Object.assign(q, urlQueries, this._getDestObj(response));
            let srUrl = CONFIG.BCOM_SR_BASEURL_HTTPS+'?'+querystring.stringify(q);

            return lineHelpers.getActionUri(
                req.__('HOTEL_BOOK_TREE.SHOW_DEALS_LABEL_20CH'),
                srUrl
            );
        },
        _getSeeThisHotelAction: (hotel, urlQueries, req) => {
            let q = {
                aid: CONSTANTS.AID_DEEP_LINK,
                label: CONSTANTS.BCOM_LABELS.DEALS_CAROUSEL_SEE_HOTEL
            };
            Object.assign(q, urlQueries);
            let hotelPageUrl = hotel.hotel_url + '&' + querystring.stringify(q);
            hotelPageUrl = urlHelpers.replaceBaseUrlWithLineUri(hotelPageUrl);

            return lineHelpers.getActionUri(
                req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_DETAILS_ACTION_LABEL_20CH'),
                hotelPageUrl
            );
        },
        _getSeeReviewsAction: (hotel, urlQueries, req) => {
            let q = {
                aid: CONSTANTS.AID_DEEP_LINK,
                activeTab: 'htReviews',
                label: CONSTANTS.BCOM_LABELS.DEALS_CAROUSEL_SEE_REVIEWS
            };
            Object.assign(q, urlQueries);
            let tabReviewsUrl = hotel.hotel_url+'&'+querystring.stringify(q);
            tabReviewsUrl = urlHelpers.replaceBaseUrlWithLineUri(tabReviewsUrl);

            return lineHelpers.getActionUri(
                req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_REVIEWS_ACTION_LABEL_20CH'),
                tabReviewsUrl
            );
        },
        _getSearchAgainAction: (req) => {
            return lineHelpers.getActionMessage(
                req.__('HOTEL_BOOK_TREE.SEARCH_AGAIN_LABEL_20CH'),
                req.__('HOTEL_BOOK_TREE.SEARCH_AGAIN_ACTION_TEXT_300CH')
            );
        },
        _getDurationFromResponse: (response) => {
            let checkin = moment(response.checkin, 'YYYY-MM-DD');
            let checkout = moment(response.checkout, 'YYYY-MM-DD');

            return checkout.diff(checkin, 'days');
        },
        _getDestObj: (response) => {
            let obj = {};

            if ('region_ids' in response) {
                obj.dest_id = response.region_ids[0];
                obj.dest_type = 'region';
            } else if ('city_ids' in response) {
                obj.dest_id = response.city_ids[0];
                obj.dest_type = 'city';
            } else if ('district_ids' in response) {
                obj.dest_id = response.district_ids[0];
                obj.dest_type = 'district';
            }

            return obj;
        },
        _getDiscountRate: (original, final) => {
            if(parseInt(original) === 0) return null;
            return priceHelpers.getDiscountRate(original, final);
        }
    },
    themes: {
        getMessages: function(suggestions, req) {
            if(suggestions.length > 4) suggestions = _.take(suggestions, 4);

            return [
                {
                    type: 'template',
                    altText: req.__('PASSION_DISCOVER_TREE.THEMES_CAROUSEL_ALTTEXT'),
                    template: {
                        type: 'carousel',
                        columns: this._getThemeColumns(suggestions, req)
                    }
                }
            ]
        },
        _getThemeColumns: function(suggestions, req) {
            let promises = [];

            suggestions.forEach((activity) => {
                let column = this._getThemeColumn(activity, req);
                promises.push(column);
            });

            return promises;
        },
        _getThemeColumn: function(activity, req) {
            let selectAction = this._getSelectThemeAction(activity, req);
            let imageUrl;

            return new Promise((resolve, reject) => {
                scraperHelpers.getImageUrlFromDsfPage({ tags: activity.dest_id })
                    .then((image) => {
                        imageUrl = image.imageUrl;

                        return apiHelpers.getCities(activity.top_destinations);
                    })
                    .then((destinations) => {
                        let arrStr = [];
                        destinations.forEach((dest) => {
                            arrStr.push(dest.name);
                        });
                        return arrStr.join('  |  ');
                    })
                    .then((destNameString) => {
                        let text = `Our top destinations:\n${destNameString}`;
                        let title = activity.label;

                        let column = lineHelpers.getCarouselColumn(
                            [selectAction], text, title, imageUrl
                        );

                        resolve(column);
                    })
                    .catch((err) => {
                        let selectAction = this._getSelectThemeAction(activity, req);
                        let column = lineHelpers.getCarouselColumn(
                            [selectAction],
                            activity.label
                        );

                        resolve(column);
                    });
            });
        },
        _getChooseAgainColumn: function(req) {
            let chooseAgain = this._getSelectChooseAgainAction(req);
            return lineHelpers.getCarouselColumn(
                [chooseAgain],
                req.__('PASSION_DISCOVER_TREE.THEMES_CAROUSEL_SEARCH_AGAIN_TEXT'),
                'Search again',
                `${CONFIG.STATIC_ASSETS_HOSTNAME_HTTPS}/images/booking-carousel-banner.jpg`
            )
        },
        _getSelectThemeAction: function(activity, req){
            return lineHelpers.getActionMessage(
                req.__('PASSION_DISCOVER_TREE.THEMES_CAROUSEL_SELECT_LABEL_20CH'),
                req.__('PASSION_DISCOVER_TREE.THEMES_CAROUSEL_SELECT_ACTION_TEXT', { activity_name: activity.label })
            )
        },
        _getSelectChooseAgainAction: function(req) {
            return lineHelpers.getActionMessage(
                req.__('PASSION_DISCOVER_TREE.THEMES_CAROUSEL_SEARCH_AGAIN_SELECT_LABEL_20CH'),
                req.__('PASSION_DISCOVER_TREE.THEMES_CAROUSEL_SEARCH_AGAIN_SELECT_TEXT')
            )
        }
    },
    destFinder: {
        getMessages: function(req, res, userData) {
            return new Promise((resolve, reject) => {
                this._getDsfTemplate(req, userData)
                    .then((dsfTemplate) => {
                        resolve([{
                            type: 'template',
                            altText: req.__('PASSION_DISCOVER_TREE.TRAVEL_GUIDE_MESSAGE_TEXT'),
                            template: dsfTemplate
                        }]);
                    });
            });
        },
        _getDsfTemplate: function(req, userData){
            let activity = userData.choice[9] ? userData.choice[9]: {};
            let dest = userData.choice [4] ? userData.choice[4] : {} ;
            let dates = userData.choice[7] ? userData.choice[7] : [];

            let q = {
                tags: activity.dest_id,
                dest_type: dest.dest_type, dest_id: dest.dest_id,
                checkin: dates[0], checkout: dates[1]
            };

            return new Promise((resolve, reject) => {
                let linkAction = this._getDsfLinkAction(req, q);
                let searchAgainAction = this._getSearchAgainAction(req);

                let options = {
                    tags: activity.dest_id,
                    dest_id: dest.dest_id,
                    dest_type: dest.dest_type
                };

                scraperHelpers.getImageUrlFromDsfPage(options)
                    .then((image) => {
                        let title = dest.name ? req.__('PASSION_DISCOVER_TREE.TRAVEL_GUIDE_MESSAGE_TITLE_40CH', {
                            activity_name: activity.label,
                            destination_name: dest.name
                        }) : req.__('PASSION_DISCOVER_TRACK.TRAVEL_GUIDE_MESSAGE_TITLE_NODEST_40CH', {
                            activity_name: activity.label
                        });

                        let template = lineHelpers.getTemplateButtons(
                            [linkAction, searchAgainAction],
                            req.__('PASSION_DISCOVER_TREE.TRAVEL_GUIDE_MESSAGE_TEXT_60CH'),
                            title, image.imageUrl
                        );

                        resolve(template);
                    });
            });
        },
        _getSearchAgainAction: function(req) {
            return lineHelpers.getActionMessage(
                req.__('PASSION_DISCOVER_TREE.SEARCH_AGAIN_LABEL_20CH'),
                req.__('PASSION_DISCOVER_TREE.SEARCH_AGAIN_TEXT')
            )
        },
        _getDsfLinkAction: function(req, qs){
            return lineHelpers.getActionUri(
                req.__('PASSION_DISCOVER_TREE.TRAVEL_GUIDE_LINKOUT_LABEL_20CH'),
                this._getLink(req, qs)
            );
        },
        _getLink: function(req, qs){
            let baseQ = { aid: CONSTANTS.AID_CHATBOT, lang: req.getLocale() };
            let q = {};

            qs = _.pickBy(qs, _.identity);

            Object.assign(q, baseQ, qs);
            return `${CONFIG.BCOM_DSF_BASEURL_HTTPS}?${querystring.stringify(q)}`;
        }
    },
    topDestDsf: {
        getMessages: function(req, destinations){
            let columns = this._getCarouselColumns(req, destinations);

            return [{
                type: 'template',
                altText: 'Here are your results for activity search',
                template: {
                    type: 'carousel',
                    columns: columns
                }
            }];
        },
        _getCarouselColumns: function(req, destinations){
            let columns = [];

            destinations.forEach((dest) => {
                let column = this._getColumn(req, dest);
                columns.push(column);
            });

            columns.push(this._getHaveDestCol(req));
            columns.push(this._getNoDestCol(req));

            return columns;
        },
        _getColumn: function(req, dest){
            let selectAction = lineHelpers.getActionMessage(
                req.__('PASSION_DISCOVER_TREE.RECOMMENDED_LOCATIONS_SELECT_LABEL_20CH'),
                dest.name
            );
            return lineHelpers.getCarouselColumn(
                [selectAction],
                req.__('PASSION_DISCOVER_TREE.RECOMMENDED_LOCATIONS_DESCRIPTION_TEXT'),
                dest.name
            )
        },
        _getHaveDestCol: function(req) {
            let selectAction = lineHelpers.getActionMessage(
                req.__('PASSION_DISCOVER_TREE.DECIDED_LOCATION_LABEL_20CH'),
                req.__('PASSION_DISCOVER_TREE.DECIDED_LOCATION_TEXT')
            );
            return lineHelpers.getCarouselColumn(
                [selectAction],
                req.__('PASSION_DISCOVER_TREE.DECIDED_LOCATION_TEXT'),
                req.__('PASSION_DISCOVER_TREE.DECIDED_LOCATION_TITLE_40CH')
            )
        },
        _getNoDestCol: function(req) {
            let selectAction = lineHelpers.getActionMessage(
                req.__('PASSION_DISCOVER_TREE.UNDECIDED_LOCATION_LABEL_20CH'),
                req.__('PASSION_DISCOVER_TREE.UNDECIDED_LOCATION_TEXT')
            );
            return lineHelpers.getCarouselColumn(
                [selectAction],
                req.__('PASSION_DISCOVER_TREE.UNDECIDED_LOCATION_TEXT'),
                req.__('PASSION_DISCOVER_TREE.UNDECIDED_LOCATION_TITLE_40CH')
            )
        }
    }
};