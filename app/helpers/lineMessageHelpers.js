'use strict';

const truncate = require('truncate');
const _ = require('lodash');

const MAX_CHARCOUNT_LABEL = 20;
const MAX_CHARCOUNT_TEXT = 300;

module.exports = {
    getActionMessage: (label = '' , text = '') => {
        if(label === '') throw new Error('Label should not be empty');
        if(text === '') throw new Error('Text should not be empty');

        if(label.length > MAX_CHARCOUNT_LABEL) label = truncate(label, MAX_CHARCOUNT_LABEL-1);
        if(text.length > 300) text = truncate(text, MAX_CHARCOUNT_TEXT-1);

        return {
            type: 'message',
            label: label,
            text: text
        }
    },
    getActionUri: (label = '', uri = '') => {
        if(label.length > MAX_CHARCOUNT_LABEL) label = truncate(label, MAX_CHARCOUNT_LABEL-3);

        return {
            type: 'uri',
            label: label,
            uri: uri
        }
    },
    getMessageText: (text = '') => {
        if(text === '') throw new Error('Text should not be empty');

        return {
            type: 'text',
            text: text
        }
    },
    getTemplateConfirm: (actions = [], text = '') => {
        if(actions.length !== 2) throw new Error('Confirm Actions should only have 2 actions');
        if(text === '') throw new Error('Text should be present');

        return {
            type: 'confirm',
            text: text,
            actions: actions
        }
    },
    getTemplateButtons: (actions, text = '', title = '', imageUrl = '') => {
        if(!Array.isArray(actions)) throw new Error('Actions should be an array');
        if(actions.length > 4) throw new Error('Actions should only have 4 actions or less');
        if(text === '') throw new Error('Text should be present');

        let o = { type: 'buttons', text: text, actions: actions };
        let f = {};

        Object.assign(f, o);

        if(title !== ''){
            if(title.length > 40) title = truncate(title, 39);
            Object.assign(f, o, { title: title });
        }

        if(imageUrl !== '') Object.assign(f, { thumbnailImageUrl: imageUrl });

        return f;
    },
    getTemplateCarousel: (columns) => {
        if(columns.length > 5) throw new Error('Columns should not be more than 5');
        return { type: 'carousel', columns: columns }
    },
    getCarouselColumn: (actions, text, title = '', imageUrl = '') => {
        let o = {
            text: truncate(text, 60-1),
            title: truncate(title, 40-1),
            actions: actions,
            thumbnailImageUrl: imageUrl
        };

        return _.pickBy(o, _.identity);
    }
};