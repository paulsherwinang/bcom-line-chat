'use strict';

const moment = require('moment');
const CONSTANTS = require('../constants');

module.exports = {
    _getMoment: (dateString) => {
        return moment(dateString, 'DD/MM');
    },
    getCheckInDate: function (arr){
        return this._getMoment(arr[0]);
    },
    getCheckoutDate: function(arr){
        return this._getMoment(arr[1]);
    },
    isValidDates: (checkin, checkout) => {
        return checkin.isValid() && checkout.isValid();
    },
    isCheckinBeforeCheckout: (checkin, checkout) => {
        return checkin.isBefore(checkout);
    },
    isInPast: (checkin, checkout) => {
        return checkin.isBefore() || checkout.isBefore();
    },
    isDurationValid: (checkin, checkout) => {
        return checkout.diff(checkin, 'days') <= CONSTANTS.MAX_STAY_DURATION_DAYS
    }

};