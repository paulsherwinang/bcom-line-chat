'use strict';

const request = require('request');

const constants = require('../constants');
const CONFIG = process.env.NODE_ENV === "production" ? constants['PROD'] : constants['DEV'];
const ENDPOINT_API_BASE_URL = CONFIG.CHATBOT_SERVER_HOSTNAME+':'+CONFIG.CHATBOT_SERVER_PORT;

let scraperHelpers = {
    getImageUrlFromDsfPage: (options) => {
        return new Promise((resolve, reject) => {
            return request({
                method: 'POST',
                url: `${ENDPOINT_API_BASE_URL}/api/getImageByDestinationTag`,
                json: true,
                body: {
                    dest_type: options.dest_type,
                    dest_id: options.dest_id,
                    tags: options.tags
                }
            }, (err, response, body) => {
                if(err) return reject(err);
                return resolve(body);
            });
        });
    }
};


module.exports = scraperHelpers;