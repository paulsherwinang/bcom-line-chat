'use strict';

const request = require('request');
const constants = require('../constants');

const CONFIG = process.env.NODE_ENV === "production" ? constants['PROD'] : constants['DEV'];
const ENDPOINT_API_BASE_URL = CONFIG.CHATBOT_SERVER_HOSTNAME+':'+CONFIG.CHATBOT_SERVER_PORT;

const apiHelpers = {
    getNewAccessToken: (channelId, channelSecret) => {
        return new Promise((resolve, reject) => {
            request.post({
                url: 'https://api.line.me/v2/oauth/accessToken',
                form: {
                    grant_type: 'client_credentials',
                    client_id: channelId,
                    client_secret: channelSecret
                },
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                resolve(body);
            })
        });
    },
    getLocationAutocomplete: (options) => {
        return new Promise((resolve, reject) => {
            request.post({
                url: ENDPOINT_API_BASE_URL+'/api/autocomplete/location',
                body: options,
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                return resolve(body);
            });
        });
    },
    getHotelAvailabilityForDeals: (options) => {
        return new Promise((resolve, reject) => {
            request.post({
                url: ENDPOINT_API_BASE_URL+'/api/getHotelAvailability',
                body: Object.assign({
                    show_only_deals: 'smart,lastm'
                }, options),
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                return resolve(body);
            });
        });
    },
    getHotelAvailabilityForBudget: (options) => {
        return new Promise((resolve, reject) => {
            request.post({
                url: ENDPOINT_API_BASE_URL+'/api/getHotelByPriceRange',
                body: options,
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                return resolve(body);
            })
        })
    },
    getThemesAutocomplete: (text) => {
        return new Promise((resolve, reject) => {
            request({
                method: 'POST',
                url: ENDPOINT_API_BASE_URL+'/api/autocomplete/themes',
                body: { text: text },
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                return resolve(body);
            });
        });
    },
    getDistrictHotels: (hotelIds) => {
        return new Promise((resolve, reject) => {
            request({
                method: 'POST',
                url: ENDPOINT_API_BASE_URL+'/api/getDistrictHotels',
                body: { hotelIds: hotelIds },
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                return resolve(body);
            });
        });
    },
    getDistricts: (districtIds) => {
        return new Promise((resolve, reject) => {
            request({
                method: 'POST',
                url: ENDPOINT_API_BASE_URL+'/api/getDistricts',
                body: { districtIds: districtIds },
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                return resolve(body);
            });
        });
    },
    getBlockAvailability: (options) => {
        return new Promise((resolve, reject) => {
            request({
                method: 'POST',
                url: ENDPOINT_API_BASE_URL+'/api/getBlockAvailability',
                body: options,
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                return resolve(body);
            });
        });
    },

};

module.exports = apiHelpers;
