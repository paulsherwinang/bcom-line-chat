'use strict';

const _ = require('lodash');
const CONSTANTS = require('../constants');
const CONFIG = process.env.NODE_ENV === 'production' ? CONSTANTS['PROD'] : CONSTANTS['DEV'];

module.exports = {
    replaceBaseUrlWithLineUri: (url) => {
        if(!_.includes(url, CONSTANTS.TO_REPLACE_URL)) return url;

        return url.replace(CONSTANTS.TO_REPLACE_URL, CONFIG.LINE_BASEURI);
    }
};