'use strict';

const numeral = require('numeral');

module.exports = {
    getDiscountRate: (originalPrice, finalPrice) => {
        let opN = numeral(originalPrice);
        let fpN = numeral(finalPrice);

        let rate = fpN.value()/opN.value();
        let pc = rate-1;

        return numeral(pc).format('0%');
    }
};