'use strict';

const _ = require('lodash');
const chatHelpers = require('./chatHelpers');
const debug = require('debug')('linebot:locationHelpers');

module.exports = {
    locationInput: {
        filterSuggestionsWithSameCity: (suggestions) => {
            let groups = _.groupBy(suggestions, 'city_name');
            return _.flatMap(groups, (group) => {
                if(group.length === 1) return group;

                return _.filter(group, (elem) => {
                    return elem.dest_type === 'city';
                });
            });
        },
        onSuggestionsAvailableAction: (req, res, storage, nextLevel, suggestions) => {
            let _getLineMessages = (req, res, userData, suggestions) => {
                let choice = userData.choice[1];
                let messages = chatHelpers.locationAutocomplete.getMessage(suggestions, req);

                messages.unshift({
                    type: 'text',
                    text: req.__('HOTEL_BOOK_TREE.LOCATION_AUTOCOMPLETE_RESULT_SEARCH_MESSAGE', {text: choice})
                });

                return messages;
            };

            let event = req.body.events[0];
            let userId = event.source.userId;

            debug('[VALID] LEVEL %s VALID RESPONSE ACTION. AutocompleteApi returned with items', this.currentLevel);

            let firstFour = _.take(suggestions, 4);
            storage.setUserSuggestion(userId, nextLevel, firstFour);
            let userData = storage.getUserData(userId);

            let lineMessages = _getLineMessages(req, res, userData, suggestions);
            chatHelpers.sendReplyMessage(req, res, lineMessages);
            storage.setConversationLevel(userId, nextLevel);
        }
    },
    locationCarousel: {
        isNoneOfTheAbove: function(text, req){
            return text === req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_NONE_ACTION_TEXT_300CH');
        },
        onNoneOfSuggestionAction: (req, res, storage, previousLevel, replyMessages) => {
            let event = req.body.events[0];
            let userId = event.source.userId;
            chatHelpers.sendReplyMessage(req, res, replyMessages);
            storage.setConversationLevel(userId, previousLevel);
        }
    },

};