'use strict';

const request = require('request');
const constants = require('../constants');
const CONFIG = process.env.NODE_ENV === 'production' ? constants['DEV'] : constants['PROD'];

module.exports = {
    getTextRequest: (userId, queryText) => {
        return new Promise((resolve, reject) => {
            request({
                json: true,
                method: 'GET',
                url: 'http://api.api.ai/v1/query',
                headers: {
                    'Authorization': `Bearer ${CONFIG.APIAI_CLIENT_ACCESS_TOKEN}`
                },
                qs: {
                    query: queryText,
                    sessionId: userId,
                    lang: 'en'
                }
            }, (err, response, body) => {
                if(err) return reject(err);
                resolve(body);
            })
        });
    },

};