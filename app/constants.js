module.exports = {
    AID_DEEP_LINK: '1289751',
    AID_CHATBOT: '1173491',
    DEFAULT_GUEST_QUANTITY: 'A,A',
    DEFAULT_CURRENCY: 'THB',
    AVAILABLE_LOCALES: ['en', 'th'],
    TO_REPLACE_URL: 'https://www.booking.com',
    BCOM_SR_BASEURL_HTTPS: 'https://www.booking.com/searchresults.html',
    BCOM_DSF_BASEURL_HTTPS: 'https://www.booking.com/destinationfinder.html',
    CHECKOUT_DATE_THRESHOLD_MONTHS: 16,
    MAX_STAY_DURATION_DAYS: 30,
    EVENTS: {
        STORAGE_TTL_EXPIRED: 'STORAGE_TTL_EXPIRED'
    },
    BCOM_LABELS: {
        DEALS_CAROUSEL_SEE_HOTEL: 'LineBot_HotelBook-DealCarousel_SeeHotel',
        DEALS_CAROUSEL_SEE_REVIEWS: 'LineBot_HotelBook-DealCarousel_SeeReviews',
        DEALS_CAROUSEL_SEE_DEALS: 'LineBot_HotelBook-DealCarousel_SeeDeals',
        BUDGET_CAROUSEL_SEE_HOTEL: 'LineBot_HotelBook-BudgetCarousel_SeeHotel',
        BUDGET_CAROUSEL_SEE_REVIEWS: 'LineBot_HotelBook-BudgetCarousel_SeeReviews',
        BUDGET_CAROUSEL_SEE_BUDGET: 'LineBot_HotelBook-BudgetCarousel_SeeBudget'
    },
    TRACKS : {
        PASSION: 'TRACK_PASSION',
        LOCATION_DATE: 'TRACK_LOCATION_DATE_TRACK'
    },
    BUDGET_TIERS: {
        TIER1: { MIN: 0, MAX: 1800 },
        TIER2: { MIN: 1800, MAX: 3700 },
        TIER3: { MIN: 3700, MAX: 5600 },
        TIER4: { MIN: 5600, MAX: 7500 },
        TIER5: { MIN: 7500 }
    },
    "DEV": {
        APIAI_CLIENT_ACCESS_TOKEN: "8248c9d73210461abeea65507e40db42",
        APIAI_DEVELOPER_ACCESS_TOKEN: "3e7363a5b6a54e238c82c83fc6630dc7",
        BCOM_SR_BASEURL_HTTPS: 'line://ch/1469667592/searchresults.html',
        BCOM_DSF_BASEURL_HTTPS: 'line://ch/1469667592/destinationfinder.html',
        BOOKING_API_CREDENTIALS: {
            USERNAME: 'jwlee',
            PASSWORD: '1234'
        },
        CHANNEL_ID: "1469667592",
        CHANNEL_SECRET: "7e9076e8af62c804e7363a07b547e868",
        CHATBOT_SERVER_HOSTNAME: 'http://localhost',
        CHATBOT_SERVER_PORT: 3000,
        DEFAULT_LOCALE: 'en',
        GA_UAID: 'UA-100010400-2',
        LINE_BASEURI: 'line://ch/1469667592',
        STATIC_ASSETS_HOSTNAME_HTTPS: 'https://linechatbot-dev.booking.com',
    },
    "PROD": {
        APIAI_CLIENT_ACCESS_TOKEN: "8248c9d73210461abeea65507e40db42",
        APIAI_DEVELOPER_ACCESS_TOKEN: "3e7363a5b6a54e238c82c83fc6630dc7",
        BCOM_SR_BASEURL_HTTPS: 'line://ch/1493165888/searchresults.html',
        BCOM_DSF_BASEURL_HTTPS: 'line://ch/1493165888/destinationfinder.html',
        BOOKING_API_CREDENTIALS: {
            USERNAME: 'jwlee',
            PASSWORD: '1234'
        },
        CHANNEL_ID: "1493165888",
        CHANNEL_SECRET: "3c25861ee7d803ade7236c3b4cea0b8c",
        CHATBOT_SERVER_HOSTNAME: 'http://localhost',
        CHATBOT_SERVER_PORT: 3000,
        DEFAULT_LOCALE: 'th',
        GA_UAID: 'UA-100010400-1',
        LINE_BASEURI: 'line://ch/1493165888',
        STATIC_ASSETS_HOSTNAME_HTTPS: 'https://linechatbot.booking.com'
    }
};