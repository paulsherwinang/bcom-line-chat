'use strict';

const request = require('request');
const _ = require('lodash');
const constants = require('../constants');
const debug = require('debug')('linebot:apiAutocompleteLogic');
const CONFIG = process.env.NODE_ENV === "production" ? constants["PROD"] : constants["DEV"];

const BOOKING_COM_API_HOST = 'https://distribution-xml.booking.com/json/bookings.autocomplete';

module.exports = (req, res) => {

    let options = {
        uri: BOOKING_COM_API_HOST,
        method: 'GET',
        qs: Object.assign({
            affiliate_id: constants.AID_CHATBOT
        }, req.body),
        auth: {
            user: CONFIG.BOOKING_API_CREDENTIALS.USERNAME,
            pass: CONFIG.BOOKING_API_CREDENTIALS.PASSWORD
        },
        json: true
    };

    if(process.env.HTTP_PROXY) options['proxy'] = process.env.HTTP_PROXY;

    request(options, function (error, response, body) {
        if(!error && response.statusCode === 200){
            if(req.body.filter && _.isObject(req.body.filter)){
                body = _.filter(body, (item) => {
                    return _.some(req.body.filter, (value, key) => {
                        return !_.includes(value, item[key]);
                    });
                });
            }

            debug('[SUCCESS] Autocomplete API success. Response: %j', body);

            res.status(200).json(body);
        } else {
            debug('[FAILED] Autocomplete API failed. Error: %j', error);

            req.visitor.exception(`Bcom Autocomplete API error: ${JSON.stringify(error)}`, true).send();
            res.status(500).json({error: error});
        }
    });

    debug('[PENDING] Calling Autocomplete API with query: %j', options.qs);
};