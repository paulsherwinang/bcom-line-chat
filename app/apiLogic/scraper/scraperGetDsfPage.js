'use strict';

const request = require('request');
const _ = require('lodash');
const debug = require('debug')('linebot:scraperGetDsfPage');

const BCOM_DESTINATION_FINDER_BASEURL = 'https://www.booking.com/destinationfinder.html';

module.exports = (req, res) => {

    let options = {
        uri: BCOM_DESTINATION_FINDER_BASEURL,
        method: 'GET',
        form: req.body
    };

    if(process.env.HTTP_PROXY) options['proxy'] = process.env.HTTP_PROXY;

    options.form = _.pickBy(options.form, _.identity);

    request(options, function (error, response, body) {
        if(!error){
            debug('[SUCCESS] Destfinder url scraped. URL: %s', response.url);
            res.status(200).send(body);
        } else {
            debug('[FAILED] Autocomplete API failed. Error: %j', error);
            req.visitor.exception(`scraping error: ${JSON.stringify(error)}`, true).send();
            res.status(500).send({error: error});
        }
    });

    debug('[PENDING] Getting Destfinder Page with query: %j', options.form);
};