'use strict';

const _ = require('lodash');
const request = require('request');
const debug = require('debug')('linebot:apiGetImageByDestinationTag');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const constants = require('../constants');
const CONFIG = process.env.NODE_ENV === "production" ? constants['PROD'] : constants['DEV'];
const ENDPOINT_API_BASE_URL = CONFIG.CHATBOT_SERVER_HOSTNAME+':'+CONFIG.CHATBOT_SERVER_PORT;

module.exports = (req, res) => {

    let options = {
        uri: `${ENDPOINT_API_BASE_URL}/scraper/getDsfPage`,
        method: 'POST',
        json: true,
        body: req.body
    };

    if(process.env.HTTP_PROXY) options['proxy'] = process.env.HTTP_PROXY;

    options.body = _.pickBy(options.body, _.identity);

    request(options, function (error, response, body) {
        if(!error){
            debug('[SUCCESS] Destfinder url scraped. Response: %j', response.url);

            let dom = new JSDOM(body);
            let $ = require('jquery')(dom.window);
            let cssUrl;

            let _getUrlFromCssurl = (cssUrl) => {
                if(!cssUrl) return '';
                return cssUrl.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
            };

            let elems = [
                $(body).find('#about > div'),
                $(body).find('.dsf_searchbg'),
                $(body).find('#dsf_full_header')
            ];

            $.each(elems, function(){
                if(!$(this).css('background-image')) return;
                cssUrl = $(this).css('background-image');
            });

            let url = _getUrlFromCssurl(cssUrl);

            res.status(200).json({ imageUrl : url });
        } else {
            debug('[FAILED] Autocomplete API failed. Error: %j', error);

            req.visitor.exception(`scraping error: ${JSON.stringify(error)}`, true).send();
            res.status(500).send({error: error});
        }
    });

    debug('[PENDING] Calling Destfinder API with query: %j', options.body);
};