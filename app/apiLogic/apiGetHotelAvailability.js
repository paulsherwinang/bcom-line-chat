'use strict';

const _ = require('lodash');
const request = require('request');
const querystring = require('querystring');
const debug = require('debug')('linebot:apiGetHotelAvailability');
const constants = require('../constants');
const CONFIG = process.env.NODE_ENV === "production" ? constants['PROD'] : constants['DEV'];

const BOOKING_COM_API_HOST = 'https://distribution-xml.booking.com/json/getHotelAvailabilityV2';

module.exports = (req, res) => {
    let options = {
        uri: BOOKING_COM_API_HOST,
        method: 'GET',
        qs: Object.assign({
            rows: 4,
            order_by: 'popularity',
            order_dir: 'descending',
            output: 'hotel_details,room_details',
            room1: constants.DEFAULT_GUEST_QUANTITY,
            https_photos: 1
        }, req.body),
        auth: {
            user: CONFIG.BOOKING_API_CREDENTIALS.USERNAME,
            pass: CONFIG.BOOKING_API_CREDENTIALS.PASSWORD
        },
        json: true
    };

    if(process.env.HTTP_PROXY) options['proxy'] = process.env.HTTP_PROXY;

    switch(req.body.dest_type) {
        case 'region':
            options.qs['region_ids'] = req.body.dest_id;
            break;
        case 'city':
            options.qs['city_ids'] = req.body.dest_id;
            break;
        case 'district':
            options.qs['district_ids'] = req.body.dest_id;
            break;
    }

    delete options.qs.dest_type;
    delete options.qs.dest_id;

    request(options, function (error, response, body) {
        if(!error && response.statusCode === 200){
            if(_.has(body, 'code')){
                debug('[ERROR] BOOKING API ERROR, Message: %s', body.message);
                debug('[ERROR] Request body: %O', req.body);
                res.status(400).send({error: body.message});
                return;
            }

            let _getCleanedUrl = (baseUrl, q) => {
                let params = querystring.stringify(q);
                return baseUrl+'?'+params;
            };

            body.hotels.forEach((hotel) => {
                if(req.body.affiliate_id) return;

                let baseUrl = hotel.hotel_url.split('?')[0];
                let params = hotel.hotel_url.split('?')[1];

                let q = querystring.parse(params);
                delete q.aid;

                hotel.hotel_url = _getCleanedUrl(baseUrl, q);
            });

            debug('[SUCCESS] Successful getHotelAvailabilityV2, response: %j', body);

            res.status(200).json(body);
        } else {
            req.visitor.exception(`Bcom getHotelAvailabilityV2 API error: ${JSON.stringify(error)}`, true).send();
            debug('[FAILED] Failed getHotelAvailabilityV2, Error: %j', error);
            res.status(500).json({error: error});
        }
    });

    debug('[PENDING] Request made with queries: %j', options.qs);
};