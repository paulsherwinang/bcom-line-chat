'use strict';

const request = require('request');
const _ = require('lodash');
const constants = require('../constants');
const debug = require('debug')('linebot:apiThemesAutocomplete');
const CONFIG = process.env.NODE_ENV === "production" ? constants["PROD"] : constants["DEV"];

const BOOKING_COM_API_HOST = 'https://distribution-xml.booking.com/json/bookings.autocomplete';

module.exports = (req, res) => {

    let options = {
        uri: BOOKING_COM_API_HOST,
        method: 'GET',
        qs: {
            text: req.body.text,
            languagecode: req.getLocale(),
            add_themes: 1,
            affiliate_id: constants.AID_CHATBOT
        },
        auth: {
            user: CONFIG.BOOKING_API_CREDENTIALS.USERNAME,
            pass: CONFIG.BOOKING_API_CREDENTIALS.PASSWORD
        },
        json: true
    };

    if(process.env.HTTP_PROXY) options['proxy'] = process.env.HTTP_PROXY;

    request(options, function (error, response, body) {
        if(!error && response.statusCode === 200){
            body = _.filter(body, (entry) => {
                return entry.dest_type === 'theme';
            });

            res.status(200).json(body);
        } else {
            debug('[FAILED] Themes Autocomplete API failed. Error: %j', error);
            req.visitor.exception(`Bcom Themes Autocomplete API error: ${JSON.stringify(error)}`, true).send();
            res.status(500).json({error: error});
        }
    });

    debug('[PENDING] Calling Themes Autocomplete API with query: %j', options.qs);
};