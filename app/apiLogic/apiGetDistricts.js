'use strict';

const _ = require('lodash');
const request = require('request');
const querystring = require('querystring');
const debug = require('debug')('linebot:apiGetDistricts');
const constants = require('../constants');
const CONFIG = process.env.NODE_ENV === "production" ? constants['PROD'] : constants['DEV'];

const BOOKING_COM_API_HOST = 'https://distribution-xml.booking.com/json/bookings.getDistricts';

module.exports = (req, res) => {
    let options = {
        uri: BOOKING_COM_API_HOST,
        method: 'GET',
        qs: {
            district_ids: req.body.districtIds.join(','),
            languagecodes: req.getLocale()
        },
        auth: {
            user: CONFIG.BOOKING_API_CREDENTIALS.USERNAME,
            pass: CONFIG.BOOKING_API_CREDENTIALS.PASSWORD
        },
        json: true
    };

    if(process.env.HTTP_PROXY) options['proxy'] = process.env.HTTP_PROXY;

    request(options, function (error, response, body) {
        if(!error && response.statusCode === 200){
            if(_.has(body, 'code')){
                debug('[ERROR] BOOKING API ERROR, Message: %s', body.message);
                debug('[ERROR] Request body: %O', req.body);
                res.status(400).send({error: body.message});
                return;
            }

            debug('[SUCCESS] Successful getDistricts, response: %j', body);

            res.status(200).json(body);
        } else {
            req.visitor.exception(`Bcom getDistricts API error: ${JSON.stringify(error)}`, true).send();
            debug('[FAILED] Failed getDistricts, Error: %j', error);
            res.status(500).json({error: error});
        }
    });

    debug('[PENDING] Request made with queries: %j', options.qs);
};