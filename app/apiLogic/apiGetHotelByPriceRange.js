'use strict';

const request = require('request');
const _ = require('lodash');
const constants = require('../constants');
const CONFIG = process.env.NODE_ENV === "production" ? constants['PROD'] : constants['DEV'];
const debug = require('debug')('linebot:apiGetHotelByPriceRange');
const querystring = require('querystring');

const BOOKING_COM_API_HOST = 'https://distribution-xml.booking.com/json/getHotelAvailabilityV2';

module.exports = (req, res) => {

    let options = {
        uri: BOOKING_COM_API_HOST,
        method: 'GET',
        qs: {
            checkin: req.body.checkin,
            checkout: req.body.checkout,
            order_by: 'review_score',
            order_dir: 'ascending',
            output: 'hotel_details',
            room1: constants.DEFAULT_GUEST_QUANTITY,
            https_photos: 1,
            currency_code: req.body.currencyCode
        },
        auth: {
            user: CONFIG.BOOKING_API_CREDENTIALS.USERNAME,
            pass: CONFIG.BOOKING_API_CREDENTIALS.PASSWORD
        },
        json: true
    };

    switch(req.body.dest_type) {
        case 'region':
            options.qs['region_ids'] = req.body.dest_id;
            break;
        case 'city':
            options.qs['city_ids'] = req.body.dest_id;
            break;
        case 'district':
            options.qs['district_ids'] = req.body.dest_id;
            break;
    }

    delete options.qs.dest_type;
    delete options.qs.dest_id;

    if(process.env.HTTP_PROXY) options['proxy'] = process.env.HTTP_PROXY;

    request(options, function (error, response, body) {
        if(!error && response.statusCode == 200){
            if(_.has(body, 'code')){
                debug('[ERROR] BOOKING API ERROR, Message: %s', body.message);
                debug('[ERROR] Request body: %O', req.body);
                res.status(400).send({error: body.message});
                return;
            }

            debug('[SUCCESS] Request successful response: %j', body);

            let filteredHotels = _.filter(body.hotels, (hotel) => {
                let isValidTotalMax = _.isNumber(req.body.totalMax);
                let isValidTotalMin = _.isNumber(req.body.totalMin);
                let price = parseFloat(hotel.price);

                let isBiggerThanMin = isValidTotalMin ? req.body.totalMin < price : true;
                let isSmallerEqualThanMax = isValidTotalMax ? price <= req.body.totalMax : true;
                return isBiggerThanMin && isSmallerEqualThanMax;
            });

            if(!_.isUndefined(req.body.limit)) filteredHotels = filteredHotels.slice(0, req.body.limit);

            let _getCleanedUrl = (baseUrl, q) => {
                let params = querystring.stringify(q);
                return baseUrl+'?'+params;
            };

            filteredHotels.forEach((hotel) => {
                if(req.body.affiliate_id) return;

                let baseUrl = hotel.hotel_url.split('?')[0];
                let params = hotel.hotel_url.split('?')[1];

                let q = querystring.parse(params);
                delete q.aid;

                hotel.hotel_url = _getCleanedUrl(baseUrl, q);
            });


            body.hotels = filteredHotels;

            res.status(200).json(body);
        } else {
            req.visitor.exception(`Bcom getHotelAvailabilityV2 (by price range) API error: ${JSON.stringify(error)}`, true).send();
            debug('[FAILED] Failed getHotelAvailabilityV2 (by price range), Error: %j', error);
            res.status(500).json({error: error});
        }
    });

    debug('[PENDING] Request has been made. query: %j', options.qs);

};