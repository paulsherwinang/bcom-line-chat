'use strict';

const _ = require('lodash');
const i18n = require('i18n');
const moment = require('moment');
const apiHelpers = require('../helpers/apiHelpers');
const chatHelpers = require('../helpers/chatHelpers');
const dateHelpers = require('../helpers/dateHelpers');
const debug = require('debug')('linebot:discoverLogic');
const lineHelpers = require('../helpers/lineMessageHelpers');
const gaMapping = require('../gaMapping');

const discoverTree = [];

// Level 0 will always have the Trigger text as text.
discoverTree[0] = {
    currentLevel: 0,
    isValidResponse: function(req, res, userData){
        return true;
    },
    onValidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_0_VALID).send();

        let lineMessages = discoverTree[1]._getLineMessages(req, res, storage);

        storage.setUserChoice(userId, this.currentLevel, event.message.text);
        storage.setConversationLevel(userId, 1);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    onInvalidResponseAction: function(event, storage, userId, req, res){}
};


// Ask for activity, make api call. save suggestions, move to level 9
discoverTree[1] = {
    currentLevel: 1,
    isValidResponse: function(req, res, userData){ return true; },
    onValidResponseAction: function(event, storage, userId, req, res) {
        let promise = apiHelpers.getThemesAutocomplete(event.message.text);
        let pendingMessages = this._getPendingMessages(req);
        chatHelpers.pushMessage(req, res, pendingMessages);

        promise.then((results) => {
            if(results.length !== 0){
               storage.setUserSuggestion(userId, this.currentLevel, results);
               this._onAutocompleteResults(req, res, storage);
           } else {
               this._onAutocompleteResultsUnavailable(req, res, storage)
           }
        });
    },
    onInvalidResponseAction: function(event, storage, userId, req, res) {
        debug('[INVALID] LEVEL %s Invalid Response. Activity is not in list. Stay in level and ask again.', this.currentLevel);

        let userData = storage.getUserData(userId);
        let errorMessages = this._getErrorMessages(req, res, userData);

        chatHelpers.sendReplyMessage(req, res, errorMessages);
    },
    _onAutocompleteResults: function(req, res, storage){
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_1_VALID).send();

        debug('[VALID] LEVEL %s Valid Response. Themes autocomplete api with results.', this.currentLevel);
        let event = req.body.events[0];
        let userId = event.source.userId;
        let userData = storage.getUserData(userId);
        let lineMessages = discoverTree[9]._getLineMessages(req, res, userData);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
        storage.setConversationLevel(userId, 9);
    },
    _onAutocompleteResultsUnavailable: function(req, res, storage){
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_1_VALID_1).send();

        debug('[VALID] LEVEL %s Themes autocomplete api without results.', this.currentLevel);
        let event = req.body.events[0];
        let userId = event.source.userId;
        this.onInvalidResponseAction(event, storage, userId, req, res);
    },
    _getLineMessages: function(req, res, userData){
        let message = lineHelpers.getMessageText(
            req.__('PASSION_DISCOVER_TREE.THEMES_QUESTION_TEXT')
        );

        return [message]
    },
    _getErrorMessages: function(req, res, userData){
        let lineMessages = this._getLineMessages(req, res, userData);
        let errorMessage = {
            type: 'text',
            text: req.__('PASSION_DISCOVER_TREE.THEMES_AUTOCOMPLETE_INVALID_MESSAGE')
        };

        lineMessages.unshift(errorMessage);
        return lineMessages;
    },
    _getPendingMessages: function(req){
        let message = lineHelpers.getMessageText(
            req.__('PASSION_DISCOVER_TREE.THEMES_AUTOCOMPLETE_PENDING_MESSAGE')
        );

        return [message]
    }
};


// Top three recommendation for activity, or yes and no
discoverTree[2] = {
    currentLevel: 2,
    isValidResponse: function(req, res, userData){
        return this._isAnyOfThree(req, res, userData) || this._isDecision(req, res, userData);
    },
    _isAnyOfThree: function(req, res, userData) {
        let choice = userData.choice[9], text = req.body.events[0].message.text;
        return this._getChoiceFromDestList(choice.top_destinations, text);
    },
    _isDecision: function(req, res, userData) {
        let options = [
            req.__('PASSION_DISCOVER_TREE.DECIDED_LOCATION_TEXT'),
            req.__('PASSION_DISCOVER_TREE.UNDECIDED_LOCATION_TEXT')
        ];

        return _.includes(options, req.body.events[0].message.text);
    },
    onValidResponseAction: function(event, storage, userId, req, res){
        let userData = storage.getUserData(userId);

        if(this._isDecision(req, res, userData)){
            if(event.message.text === req.__('PASSION_DISCOVER_TREE.DECIDED_LOCATION_TEXT')){
                this._onDecidedLocationAction(req, res, storage);
            } else {
                this._onUndecidedLocationAction(req, res, storage);
            }
        } else {
            let choice = this._getChoiceFromDestList(userData.choice[9].top_destinations, event.message.text);
            let lineMessages = discoverTree[5]._getLineMessages(req, res, storage);

            storage.setUserChoice(userId, 4, choice);
            storage.setConversationLevel(userId, 5);

            chatHelpers.sendReplyMessage(req, res, lineMessages);
        }
    },
    onInvalidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_2_INVALID).send();

        let userData = storage.getUserData(userId);
        let errorMessages = this._getErrorMessages(req, res, userData);

        chatHelpers.sendReplyMessage(req, res, errorMessages);
    },
    _onUndecidedLocationAction: function(req, res, storage){
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_2_VALID_2).send();

        debug('[VALID] Undecided location path. Going to level 3');
        let event = req.body.events[0];
        let userId = event.source.userId;

        let lineMessages = discoverTree[5]._getLineMessages(req, res, storage);
        chatHelpers.sendReplyMessage(req, res, lineMessages);

        storage.setUserChoice(userId, this.currentLevel, req.__l('PASSION_DISCOVER_TREE.UNDECIDED_LOCATION_TEXT'));
        storage.setConversationLevel(userId, 5);
    },
    _onDecidedLocationAction: function(req, res, storage){
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_2_VALID_1).send();

        debug('[VALID] Decided location path. Going to level 4');
        let event = req.body.events[0];
        let userId = event.source.userId;
        let userData = storage.getUserData(userId);

        let lineMessages = discoverTree[3]._getLineMessages(req, res, userData);
        chatHelpers.sendReplyMessage(req, res, lineMessages);

        storage.setUserChoice(userId, this.currentLevel, req.__l('PASSION_DISCOVER_TREE.DECIDED_LOCATION_TEXT'));
        storage.setConversationLevel(userId, 3)
    },
    _getChoiceFromDestList: function(suggestions, text){
        return _.find(suggestions, { name: text });
    },
    _getLineMessages: function(req, res, userData){
        let activity = userData.choice[9];
        let destinations = activity.top_destinations;
        let lineMessages = chatHelpers.topDestDsf.getMessages(req, destinations);
        let introMessages = lineHelpers.getMessageText(req.__('PASSION_DISCOVER_TREE.RECOMMENDED_LOCATIONS_INTRO_MESSAGE', {
            activity_name: activity.label,
            dest_joined: _.map(destinations, 'name').join(', ')
        }));

        lineMessages.unshift(introMessages);
        return lineMessages;
    },
    _getErrorMessages: function(req, res, userData){
        let messages = this._getLineMessages(req, res, userData);
        let errorMessage = lineHelpers.getMessageText(req.__('INVALID_CHOICE_ENTERED'));
        messages.unshift(errorMessage);
        return messages;
    }
};


// Location is decided. Autocomplete API
discoverTree[3] = {
    currentLevel: 3,
    isValidResponse: function(req, res, userData){
        return true;
    },
    onValidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_3_VALID).send();

        debug('[PENDING] LEVEL %s Valid Response. Called Autocomplete API', this.currentLevel);

        let promise = apiHelpers.getLocationAutocomplete({
            text: event.message.text,
            languagecode: i18n.getLocale(req),
            filter: { dest_type: ['airport', 'landmark', 'hotel', 'district'] }
        });

        promise.then((results) => {
            if(results.length !== 0){
                storage.setUserSuggestion(req.body.events[0].source.userId, this.currentLevel, results);
                this._onAutocompleteResults(req, res, storage);
            } else {
                this._onAutocompleteResultsUnavailable(req, res, storage);
            }
        });
    },
    onInvalidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_3_INVALID).send();

        let userData = storage.getUserData(userId);
        let errorMessages = this._getErrorMessages(req, res, userData);

        chatHelpers.sendReplyMessage(req, res, errorMessages);
    },
    _onAutocompleteResults: function(req, res, storage){
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_3_VALID_1).send();

        let event = req.body.events[0];
        let userId = event.source.userId;
        let userData = storage.getUserData(userId);
        let lineMessages = discoverTree[4]._getLineMessages(req, res, userData);

        chatHelpers.sendReplyMessage(req, res, lineMessages);
        storage.setUserChoice(userId, this.currentLevel, event.message.text);
        storage.setConversationLevel(userId, 4);
    },
    _onAutocompleteResultsUnavailable: function(req, res, storage){
        req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_3_VALID_2).send();

        let event = req.body.events[0];
        let userId = event.source.userId;
        this.onInvalidResponseAction(event, storage, userId, req, res);
    },
    _getLineMessages: function(req, res, userData){
        let textMessage = lineHelpers.getMessageText(
            req.__('PASSION_DISCOVER_TREE.LOCATION_QUESTION_TEXT')
        );
        return [textMessage];
    },
    _getErrorMessages: function(req, res, userData){}
};


// Location select via carousel
discoverTree[4] = {
    currentLevel: 4,
    isValidResponse : function(req, res, userData) {
        return this._getChoiceFromSuggestions(req, res, userData);
    },
    onValidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_4_VALID).send();

        debug('[VALID] Level %s Location carousel message, selected valid location. Going to level 5', this.currentLevel);
        let userData = storage.getUserData(userId);

        let lineMessages = discoverTree[5]._getLineMessages(req, res, storage);
        chatHelpers.sendReplyMessage(req, res, lineMessages);

        storage.setUserChoice(userId, this.currentLevel, this._getChoiceFromSuggestions(req, res, userData));
        storage.setConversationLevel(userId, 5);
    },
    onInvalidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_4_INVALID).send();

        debug('[INVALID] Level %s Choice isnt in carousel. Asking again', this.currentLevel);

        let userData = storage.getUserData(userId);
        let errorMessages = this._getErrorMessages(req, res, userData);

        chatHelpers.sendReplyMessage(req, res, errorMessages);
    },
    _getChoiceFromSuggestions: function(req, res, userData){
        let suggestion = userData.suggestion[3];
        return _.find(suggestion, { label: req.body.events[0].message.text })
    },
    _getLineMessages: (req, res, userData) => {
        let suggestions = userData.suggestion[3];
        return chatHelpers.locationAutocomplete.getMessage(suggestions, req);
    },
    _getErrorMessages: function(req, res, userData){
        let messages = this._getLineMessages(req, res, userData);
        let errorMessage = lineHelpers.getMessageText(req.__('INVALID_CHOICE_ENTERED'));
        messages.unshift(errorMessage);
        return messages;
    }
};


// Ask if dates are available
discoverTree[5] = {
    currentLevel: 5,
    isValidResponse: function(req, res, userData){
        let options = [
            req.__('PASSION_DISCOVER_TREE.DECIDED_DATES_TEXT'),
            req.__('PASSION_DISCOVER_TREE.UNDECIDED_DATES_TEXT')
        ];

        return _.includes(options, req.body.events[0].message.text);
    },
    onValidResponseAction: function(event, storage, userId, req, res) {
        let isDecidedReply = event.message.text === req.__('PASSION_DISCOVER_TREE.DECIDED_DATES_TEXT');

        if(isDecidedReply){
            this._onDecidedDateAction(req, res, storage);
        } else {
            this._onUndecidedDateAction(req, res, storage);
        }
    },
    onInvalidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_5_INVALID).send();

        let userData = storage.getUserData(userId);
        let errorMessages = this._getErrorMessages(req, res, userData);

        chatHelpers.sendReplyMessage(req, res, errorMessages);
    },
    _onUndecidedDateAction: function(req, res, storage) {
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_5_VALID_2).send();

        debug('[VALID] LEVEL %s Uncedided Date action.', this.currentLevel);

        let event = req.body.events[0];
        let userId = event.source.userId;
        let userData = storage.getUserData(userId);

        chatHelpers.sendReplyMessage(req, res, lineMessages);
        storage.setUserChoice(userId, this.currentLevel, req.__l('PASSION_DISCOVER_TREE.UNDECIDED_DATES_TEXT'));
        storage.setConversationLevel(userId, 8);
    },
    _onDecidedDateAction: function(req, res, storage) {
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_5_VALID_1).send();

        debug('[VALID] LEVEL %s Decided Date action.', this.currentLevel);

        let event = req.body.events[0];
        let userId = event.source.userId;
        let userData = storage.getUserData(userId);
        let lineMessages = discoverTree[6]._getLineMessages(req, res, userData);

        chatHelpers.sendReplyMessage(req, res, lineMessages);
        storage.setUserChoice(userId, this.currentLevel, req.__l('PASSION_DISCOVER_TREE.DECIDED_DATES_TEXT'));
        storage.setConversationLevel(userId, 6);
    },
    _getLineMessages: function(req, res, userData){
        let decidedAction = lineHelpers.getActionMessage(
                req.__('PASSION_DISCOVER_TREE.DECIDED_DATES_LABEL_20CH'),
                req.__('PASSION_DISCOVER_TREE.DECIDED_DATES_TEXT')
            );

        let undecidedAction = lineHelpers.getActionMessage(
                req.__('PASSION_DISCOVER_TREE.UNDECIDED_DATES_LABEL_20CH'),
                req.__('PASSION_DISCOVER_TREE.UNDECIDED_DATES_TEXT')
            );

        return [
            {
                type: 'template',
                altText: req.__('PASSION_DISCOVER_TREE.DATE_AVAILABLE_QUESTION_TEXT'),
                template: lineHelpers.getTemplateConfirm(
                    [decidedAction, undecidedAction],
                    req.__('PASSION_DISCOVER_TREE.DATE_AVAILABLE_QUESTION_TEXT')
                )
            }
        ];
    },
    _getErrorMessages: function (req, res, userData) {
        let messages = this._getLineMessages(req, res, userData);
        let errorMessage = lineHelpers.getMessageText(req.__('INVALID_CHOICE_ENTERED'));
        messages.unshift(errorMessage);
        return messages;
    }
};


// Date is decided
discoverTree[6] = {
    currentLevel: 6,
    isValidResponse: function(req, res, storage) {
        let event = req.body.events[0];
        if(!_.includes(event.message.text, '-')) return false;
        let dateArr = event.message.text.split('-');

        let checkin = dateHelpers.getCheckInDate(dateArr);
        let checkout = dateHelpers.getCheckoutDate(dateArr);

        let isValidDates = dateHelpers.isValidDates(checkin, checkout);
        let isCheckinBeforeCheckout = dateHelpers.isCheckinBeforeCheckout(checkin, checkout);
        let isDurationValid = dateHelpers.isDurationValid(checkin, checkout);
        let isInPast = dateHelpers.isInPast(checkin, checkout);

        return isValidDates && isCheckinBeforeCheckout && isDurationValid && !isInPast;
    },
    onValidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_6_VALID).send();

        let suggestion = this._getDatesArrayForStore(event.message.text);

        debug('[VALID] LEVEL %s VALID RESPONSE ACTION. Sending question for dates', this.currentLevel);

        storage.setUserSuggestion(userId, this.currentLevel, suggestion);
        storage.setConversationLevel(userId, 7);

        let userData = storage.getUserData(userId);
        let lineMessages = discoverTree[7]._getLineMessages(req, res, userData);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    onInvalidResponseAction: function(event, storage, userId, req, res) {
        debug('[INVALID] LEVEL %s Date confirm reject. Asking again', this.currentLevel);
        let lineMessages = this._getErrorMessages(req, res, storage);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    _getLineMessages: function(req, res, userData){
        let message = lineHelpers.getMessageText(
            req.__('PASSION_DISCOVER_TREE.CHECKIN_CHECKOUT_DATE_QUESTION')
        );

        return [message];
    },
    _getErrorMessages: function(req, res, storage){
        let event = req.body.events[0];
        let userId = event.source.userId;
        let userData = storage.getUserData(userId);

        let lineMessages = this._getLineMessages(req, res, userData);
        let errorMsg;

        let arr = event.message.text.split('-');
        let checkin = dateHelpers.getCheckInDate(arr);
        let checkout = dateHelpers.getCheckoutDate(arr);

        if(!dateHelpers.isValidDates(checkin, checkout)){
            req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_6_INVALID_1).send();

            errorMsg = {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.DATE_INVALID_MESSAGE')
            };
        } else

        if(!dateHelpers.isCheckinBeforeCheckout(checkin, checkout)){
            req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_6_INVALID_2).send();

            errorMsg = {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.CHECKOUT_BEFORE_CHECKIN_INVALID_MESSAGE')
            };
        } else

        if(!dateHelpers.isDurationValid(checkin, checkout)){
            req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_6_INVALID_3).send();

            errorMsg = {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.STAY_DURATION_INVALID_MESSAGE')
            };
        } else

        if(dateHelpers.isInPast(checkin, checkout)){
            req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_6_INVALID_4).send();

            errorMsg = {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.DATE_INVALID_MESSAGE')
            }
        }

        lineMessages.unshift(errorMsg);
        return lineMessages;
    },
    _getDatesArrayForStore: function(text) {
        let dates = text.split('-');
        let checkin = dateHelpers._getMoment(dates[0]);
        let checkout = dateHelpers._getMoment(dates[1]);

        if (checkin.isBefore(new Date(), 'month')) {
            checkin.add(1, 'year'); checkout.add(1, 'year');
        }

        return [checkin.format('YYYY-MM-DD'), checkout.format('YYYY-MM-DD')];
    }
};


// Confirm date entry
discoverTree[7] = {
    currentLevel: 7,
    isValidResponse: function(req, res, userData) {
        let options = [
            req.__('PASSION_DISCOVER_TREE.DATE_CONFIRM_YES_TEXT_ACTION'),
            req.__('PASSION_DISCOVER_TREE.DATE_CONFIRM_NO_TEXT_ACTION')
        ];

        return _.includes(options, req.body.events[0].message.text);
    },
    onValidResponseAction: function (event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_7_VALID).send();

        let isDecidedAction = event.message.text === req.__('PASSION_DISCOVER_TREE.DATE_CONFIRM_YES_TEXT_ACTION');

        if(isDecidedAction){
            this._onConfirmedDateAction(req, res, storage);
        } else {
            this._onRejectedDateAction(req, res, storage);
        }
    },
    onInvalidResponseAction: function (event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_7_INVALID).send();

        let userData = storage.getUserData(userId);
        let errorMessages = this._getErrorMessages(req, res, userData);

        chatHelpers.sendReplyMessage(req, res, errorMessages);
    },
    _onConfirmedDateAction: function (req, res, storage) {
        debug('[VALID] LEVEL %s Date confirmed action.', this.currentLevel);

        let event = req.body.events[0];
        let userId = event.source.userId;
        let userData = storage.getUserData(userId);

        chatHelpers.sendReplyMessage(req, res, lineMessages);
        storage.setUserChoice(userId, this.currentLevel, userData.suggestion[6]);
        storage.setConversationLevel(userId, 8);
    },
    _onRejectedDateAction: function (req, res, storage) {
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_7_VALID_1).send();

        debug('[VALID] LEVEL %s Date rejected action. Stay in level and ask again', this.currentLevel);

        let event = req.body.events[0];
        let userId = event.source.userId;
        this.onInvalidResponseAction(event, storage, userId, req, res);
    },
    _getLineMessages: function(req, res, userData){
        let i = moment(userData.suggestion[6][0]).locale(userData.locale);
        let checkinStr = i.format('LL');
        let o = moment(userData.suggestion[6][1]).locale(userData.locale);
        let checkoutStr = o.format('LL');
        let duration = o.diff(i, 'days');

        let confirmAction = lineHelpers.getActionMessage(
            req.__('PASSION_DISCOVER_TREE.DATE_CONFIRM_YES_LABEL_20CH'),
            req.__('PASSION_DISCOVER_TREE.DATE_CONFIRM_YES_TEXT_ACTION')
        );

        let denyAction = lineHelpers.getActionMessage(
            req.__('PASSION_DISCOVER_TREE.DATE_CONFIRM_NO_LABEL_20CH'),
            req.__('PASSION_DISCOVER_TREE.DATE_CONFIRM_NO_TEXT_ACTION')
        );

        let confirmTemplate = lineHelpers.getTemplateConfirm(
            [confirmAction, denyAction],
            req.__mf('DATE_CONFIRMATION_MESSAGE', {
                checkin: checkinStr, checkout: checkoutStr, duration: duration
            })
        );

        return [{
            type: 'template',
            altText: req.__mf('DATE_CONFIRMATION_MESSAGE', {
                checkin: checkinStr, checkout: checkoutStr, duration: duration
            }),
            template: confirmTemplate
        }];
    },
    _getErrorMessages: function (req, res, userData) {
        let messages = this._getLineMessages(req, res, userData);
        let errorMessage = lineHelpers.getMessageText(req.__('INVALID_CHOICE_ENTERED'));
        messages.unshift(errorMessage);
        return messages;
    }
};

// Show message to redirect to the correct Dest Finder Page
// maybe add an api to get image from the page
discoverTree[8] = {
    isValidResponse: (req, res, userData) => {
        return req.body.events[0].message.text === req.__('PASSION_DISCOVER_TREE.SEARCH_AGAIN_TEXT');
    },
    onValidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_8_VALID).send();
        req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_SEARCH_AGAIN).send();

        storage.setConversationLevel(userId, 1);
        storage.removeUserChoices(userId);

        let lineMessages = discoverTree[1]._getLineMessages(req);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    onInvalidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_8_INVALID).send();

        let userData = storage.getUserData(userId);
        let lineMessages = this._getLineMessages(req, res, userData);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    _getLineMessages: function (req, res, userData) {
        return chatHelpers.destFinder.getMessages(req, res, userData);
    }
};


// Carousel for themes autocomplete
discoverTree[9] = {
    currentLevel: 9,
    isValidResponse: function(req, res, userData) {
        return this._getChoiceFromSuggestions(req, res, userData) || this._isNoneOfTheAbove(req);
    },
    onValidResponseAction: function(event, storage, userId, req, res) {
        if(!this._isNoneOfTheAbove(req)){
            req.visitor.event('chat trigger', 'valid', gaMapping.PASSION_DISCOVER_9_VALID).send();

            debug('[INVALID] LEVEL %s Valid Response. Activity is in list of suggestions. Next level', this.currentLevel);

            let userData = storage.getUserData(userId);
            let choice = this._getChoiceFromSuggestions(req, res, userData);

            apiHelpers.getCities(choice.top_destinations)
                .then((results) => {
                    choice.top_destinations = results;
                    storage.setUserChoice(userId, this.currentLevel, choice);
                    storage.setConversationLevel(userId, 2);

                    storage.setUserChoice(userId, this.currentLevel, choice);
                    storage.setConversationLevel(userId, 2);
                    chatHelpers.sendReplyMessage(req, res, lineMessages);
                });
        } else {
            this._onNoneOfSuggestionAction(req, res, storage);
        }
    },
    onInvalidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_9_INVALID).send();

        debug('[INVALID] LEVEL %s Invalid Response. Activity is not in suggestion. Stay in level and ask again.', this.currentLevel);
        let userData = storage.getUserData(userId);
        let errorMessages = this._getErrorMessages(req, res, userData);
        chatHelpers.sendReplyMessage(req, res, errorMessages);
    },
    _isNoneOfTheAbove: function(req){
        return req.body.events[0].message.text === req.__('PASSION_DISCOVER_TREE.THEMES_CAROUSEL_SEARCH_AGAIN_SELECT_TEXT');
    },
    _onNoneOfSuggestionAction: function(req, res, storage) {
        req.visitor.event('chat trigger', 'invalid', gaMapping.PASSION_DISCOVER_9_VALID_1).send();

        let event = req.body.events[0];
        let userId = event.source.userId;
        let userData = storage.getUserData(userId);
        let lineMessages = discoverTree[1]._getLineMessages(req, res, userData);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
        storage.setConversationLevel(userId, 1);
    },
    _getChoiceFromSuggestions: function(req, res, userData){
        return _.find(userData.suggestion[1], { label: req.body.events[0].message.text });
    },
    _getLineMessages: function(req, res, userData){
        let activities = userData.suggestion[1];
        return chatHelpers.themes.getMessages(activities, req);
    }
};

module.exports = discoverTree;
