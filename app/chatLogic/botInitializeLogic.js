'use strict';

const _ = require('lodash');
const CONSTANTS = require('../constants');
const chatHelpers = require('../helpers/chatHelpers');
const botInitializeTree = require('./botInitialize.tree');
const i18n = require('i18n');
const debug = require('debug')('linebot:botInit');

module.exports = function(req, res, next, storage){
    let userId = req.body.events[0].source.userId;

    if(!storage.isUserInStore(userId)) storage.setUser(userId);

    if(storage.eventEmitter.listenerCount(CONSTANTS.EVENTS.STORAGE_TTL_EXPIRED) === 0) {
        storage.eventEmitter.on(CONSTANTS.EVENTS.STORAGE_TTL_EXPIRED, (userId) => {
            chatHelpers.pushMessage(req, res, chatHelpers.getFarewellMessages(req));
            storage.removeUser(userId);
        });
    }

    let userData = storage.getUserData(userId);
    i18n.setLocale(req, userData.locale);

    debug('NEW EVENT: %s', req.body.events[0].type);

    let i = _.findIndex(botInitializeTree, (tree) => {
        return tree.isValidResponse(req, userData);
    });

    if(i !== -1){
        botInitializeTree[i].onValidResponseAction(req, res, next, storage);
    } else {
        debug('no matching action found, propagating to next middleware');
        next();
    }
};