'use strict';

const _ = require('lodash');
const i18n = require('i18n');
const chatHelpers = require('../helpers/chatHelpers');
const debug = require('debug')('linebot:botInitializeTree');
const TRACKS = require('../constants').TRACKS;
const gaMapping = require('../gaMapping');

const botInitializeTree = [];

// Middleware should check for event types and
// propagate them or take action in this middleware

// if text message
botInitializeTree[0] = {
    isValidResponse: function(req, res, userData){
        let event = req.body.events[0];
        return event.type === 'message' && event.message.type === 'text';
    },
    onValidResponseAction: function(req, res, next, storage){
        let event = req.body.events[0];
        let userId = event.source.userId;
        let userData = storage.getUserData(userId);

        debug('NEW MESSAGE: %s, USER: %o', event.message.text, userData);
        req.visitor.set('cd1', userData.track);
        req.visitor.set('cd2', userData.conversationLevel);
        req.visitor.set('cd3', event.message.text);
        req.visitor.set('cd4', userId);
        req.visitor.set('userLanguage', userData.locale);

        switch(event.message.text) {
            case req.__('START_OVER_TEXT'):
                debug('[VALID] Keyword "start over" recognized and handling');
                req.visitor.event('chat trigger', 'special', gaMapping.START_OVER).send();

                storage.resetUserState(userId);
                next();
                break;

            case req.__('CHANGE_LANGUAGE'):
                req.visitor.event('chat trigger', 'special', gaMapping.CHANGE_LANGUAGE).send();

                let oldLocale = userData.locale;
                let newLocale = userData.locale == 'en' && 'th' || 'en';

                debug('[VALID] Keyword "change language" recognized and handling. Changed Locale %s -> %s', oldLocale, newLocale);
                storage.setUserLocale(userId, newLocale);
                i18n.setLocale(req, userData.locale);

                next();
                break;

            case req.__('HOTEL_BOOK_TREE_TRIGGER_TEXT') :
                debug('[VALID] Keyword for hotel book tree, recognized and handling.');

                storage.setUserTrack(userId, TRACKS.LOCATION_DATE);
                next();
                break;

            case req.__('PASSION_DISCOVER_TREE_TRIGGER_TEXT') :
                debug('[VALID] Keyword for passion discover tree, recognized and handling.');

                storage.setUserTrack(userId, TRACKS.PASSION);
                next();
                break;

            default:
                next();
                break;
        }
    }
};

// if image message
botInitializeTree[1] = {
    isValidResponse: function(req, res, userData){
        let event = req.body.events[0];
        return event.type === 'message' && event.message.type === 'image';
    },
    onValidResponseAction: function(req, res, next, storage){
        let event = req.body.events[0];
        let msgId = `messageId_${event.message.id}`;
        req.visitor.event('image message', 'special', msgId).send();
        next();
    }
};

// if sticker message
botInitializeTree[2] = {
    isValidResponse: function(req, res, userData){
        let event = req.body.events[0];
        return event.type === 'message' && event.message.type === 'sticker';
    },
    onValidResponseAction: function (req, res, next, storage) {
        let event = req.body.events[0];
        let data = `stickerId_${event.message.stickerId}_packageId_${event.message.packageId}`;
        req.visitor.event('sticker message', 'special', data).send();
        next();
    }
};

// if location message
botInitializeTree[3] = {
    isValidResponse: function(req, res, userData){
        let event = req.body.events[0];
        return event.type === 'message' && event.message.type === 'location';
    },
    onValidResponseAction: function (req, res, next, storage) {
        let event = req.body.events[0];
        let data = `lat_${event.message.latitude}_lon_${event.message.longitude}`;
        req.visitor.event('location message', 'special', data).send();
        next();
    }
};

// if video message TODO
// if audio message TODO
// if file message TODO

// Check if follow event
botInitializeTree[4] = {
    isValidResponse: (req, res, userData) => {
        return req.body.events[0].type === 'follow';
    },
    onValidResponseAction: (req, res, next, storage) => {
        req.visitor.event('chat event', 'add friend', gaMapping.ADD_FOLLOW_EVENT).send();

        debug('[VALID] User added bot as friend. recognized and handling');

        let lineMessages = chatHelpers.initialMessage.getMessage(req);
        lineMessages.unshift({
            type: 'text',
            text: req.__('ADDED_FRIEND_MESSAGE_TEXT')
        });

        chatHelpers.sendReplyMessage(req, res, lineMessages);
    }
};

// Check if unfollow event
botInitializeTree[5] = {
    isValidResponse: (req, res, userData) => {
        return req.body.events[0].type === 'unfollow';
    },
    onValidResponseAction: (req, res, next, storage) => {
        req.visitor.event('chat event', 'block remove friend', gaMapping.BLOCK_UNFOLLOW_EVENT).send();

        debug('[VALID] unfollow action, fire tracking stuff here and end');
        storage.removeUser(req.body.events[0].source.userId);
    }
};

// Check if postback event
// Check if join event
// Check if leave event
botInitializeTree[6] = {
    isValidResponse: (req, res, userData) => {
        return req.body.events[0].type === 'leave';
    },
    onValidResponseAction: (req, res, next, storage) => {
        debug('[VALID] leave group action, fire tracking stuff here and end');
    }
};


module.exports = botInitializeTree;
