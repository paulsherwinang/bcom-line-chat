'use strict';

const _ = require('lodash');
const moment = require('moment');
const debug = require('debug')('linebot:hotelBookingLogic');
const CONSTANTS = require('../constants');
const chatHelpers = require('../helpers/chatHelpers');
const apiHelpers = require('../helpers/apiHelpers');
const dateHelpers = require('../helpers/dateHelpers');
const apiaiHelpers = require('../helpers/apiaiHelpers');
const lineHelpers = require('../helpers/lineMessageHelpers');
const locationHelpers = require('../helpers/locationHelpers');
const gaMapping = require('../gaMapping');
// const apiai = require('../apiai');

let _getCorrectLocationChoice = (userData) => {
    return _.isEmpty(userData.choice[4]) ? userData.choice[2] : userData.choice[4];
};

const hotelBookingTree = [];

// ------------------------------------------
// Message: "How can I help you?"
// Input: "Book me a hotel" (trigger question)
// Validation: Check if trigger question matches
// Extra Step: none
// ------------------------------------------
hotelBookingTree[0] = {
    currentLevel: 0,
    onValidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_0_VALID).send();

        let lineMessages = hotelBookingTree[1]._getLineMessages(event, storage, userId, req, res);

        debug('[VALID] LEVEL %s - Asked "%s"', this.currentLevel, lineMessages[0].text);

        chatHelpers.sendReplyMessage(req, res, lineMessages);
        storage.setConversationLevel(userId, 1);
    },
    isValidResponse: (req, res, userData) => { return true; },
    onInvalidResponseAction: function(event, storage, userId, req, res){},
    _getLineMessages: (event, storage, userId, req, res) => {},
    _getErrorMessages: function(event, storage, userId, req, res){}
};


// ------------------------------------------
// Message: "Where do you want to go?"
// Input: Any location name (free text)
// Validation: none
// Extra Step: Call Autocomplete API, set UserData
// ------------------------------------------
hotelBookingTree[1] = {
    currentLevel: 1,
    onValidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_1_VALID).send();

        storage.setUserChoice(userId, this.currentLevel, event.message.text);
        debug('[VALID] LEVEL %s Valid Response. Calling AutocompleteApi', this.currentLevel);

        let promise = apiHelpers.getLocationAutocomplete({
            text: event.message.text,
            languagecode: req.getLocale(),
            filter: { dest_type: ['airport', 'hotel', 'landmark'] }
        });

        promise.then((suggestions) => {
            if(suggestions.length !== 0) {
                suggestions = locationHelpers.locationInput.filterSuggestionsWithSameCity(suggestions);

                if(suggestions.length === 1){
                    this._onObviousAssumptionAction(req, res, storage, suggestions[0]);
                } else {
                    this._onSuggestionsAvailableAction(req, res, storage, suggestions);
                }

            } else {
                req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_1_VALID_2).send();

                debug('[INVALID] LEVEL %s - Autocomplete API returned empty', this.currentLevel);
                this.onInvalidResponseAction(event, storage, userId, req, res);
            }
        }).catch((err) => {
            console.log(err);
            debug('[ERROR] LEVEL %s - ERROR Calling AUTOCOMPLETE API', this.currentLevel);
        });

    },
    onInvalidResponseAction: function(event, storage, userId, req, res) {
        let lineMessages = this._getErrorMessages(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    isValidResponse: (req, res, userData) => {
        return true;
    },
    _onSuggestionsAvailableAction: (req, res, storage, suggestions) => {
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_1_VALID_1).send();

        locationHelpers.locationInput.onSuggestionsAvailableAction(req, res, storage, 2, suggestions);
    },
    _onObviousAssumptionAction: function(req, res, storage, choice){
        let event = req.body.events[0]
        let userId = event.source.userId;

        storage.setUserChoice(userId, 2, choice);

        let lineMessages = hotelBookingTree[5]._getLineMessages(event, storage, userId, req, res);
        let selectedMessages = chatHelpers.getLocationSelectedMessage(req, res, choice);
        lineMessages.unshift(selectedMessages);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
        storage.setConversationLevel(userId, 5);
    },
    _getLineMessages: function(event, storage, userId, req, res){
        return [
            {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.INTRO_QUESTION')
            }
        ];
    },
    _getPendingMessages: function(req) {
        return [
            {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.API_AUTOCOMPLETE_PENDING_MESSAGE')
            }
        ];
    },
    _getErrorMessages: function(event, storage, userId, req, res){
        let lineMessages = this._getLineMessages(event, storage, userId, req, res);
        let errorMessage = {
            type: 'text',
            text: req.__('HOTEL_BOOK_TREE.LOCATION_AUTOCOMPLETE_INVALID_MESSAGE')
        };

        lineMessages.unshift(errorMessage);
        return lineMessages;
    }
};

// ------------------------------------------
// Message: Carousel with suggestions
// Input: Text from carousel LV2
// Validation: Check if entry selected is a country or not
// Extra Step: set UserData
// ------------------------------------------
hotelBookingTree[2] = {
    currentLevel: 2,
    onValidResponseAction: function(event, storage, userId, req, res) {
        if (!locationHelpers.locationCarousel.isNoneOfTheAbove(event.message.text, req)) {
            req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_2_VALID).send();

            let userData = storage.getUserData(userId);
            let suggestion = userData.suggestion[this.currentLevel];
            let choice = this._getChoiceFromSuggestions(event.message.text, suggestion);
            let nextLevel = this._getNextLevel(choice);

            debug('[VALID] LEVEL %s VALID RESPONSE ACTION. Dest type is correct.', this.currentLevel);

            storage.setUserChoice(userId, this.currentLevel, choice);
            storage.setConversationLevel(userId, nextLevel);

            let lineMessages = hotelBookingTree[nextLevel]._getLineMessages(event, storage, userId, req, res);
            chatHelpers.sendReplyMessage(req, res, lineMessages);
        } else {
            this._onNoneOfSuggestionAction(req, res, storage);
        }
    },
    onInvalidResponseAction: function (event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_2_INVALID).send();

        let lineMessages = this._getErrorMessages(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    isValidResponse: function(req, res, userData){
        let event = req.body.events[0];
        let suggestions = userData.suggestion[this.currentLevel];
        return _.some(suggestions, { label: event.message.text }) || locationHelpers.locationCarousel.isNoneOfTheAbove(event.message.text, req);
    },
    _onNoneOfSuggestionAction: function(req, res, storage) {
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_2_VALID_1).send();
        let event = req.body.events[0];
        let userId = event.source.userId;
        let previousLevel = 1;
        let lineMessages = hotelBookingTree[previousLevel]._getLineMessages(event, storage, userId, req, res);

        locationHelpers.locationCarousel.onNoneOfSuggestionAction(req, res, storage, previousLevel, lineMessages);
    },
    _isNoneOfTheAbove: function(text, req){
        return text === req.__('HOTEL_BOOK_TREE.LOCATION_CAROUSEL_NONE_ACTION_TEXT_300CH');
    },
    _getLineMessages: function(event, storage, userId, req, res){
        let userData = storage.getUserData(userId);
        let cityName = this._getLocationChoice(userData);
        let suggestions = userData.suggestion[this.currentLevel];
        let messages = chatHelpers.locationAutocomplete.getMessage(suggestions, req);

        messages.unshift({
            type: 'text',
            text: req.__('HOTEL_BOOK_TREE.LOCATION_AUTOCOMPLETE_RESULT_SEARCH_MESSAGE', {text: cityName})
        });

        return messages;
    },
    _getLocationChoice: (userData) => {
        return userData.choice[1];
    },
    _getChoiceFromSuggestions: (text, suggestion) => {
        return _.find(suggestion, { label: text });
    },
    _getErrorMessages: function(event, storage, userId, req, res){
        let lineMessages = this._getLineMessages(event, storage, userId, req, res);
        let errorMessage = {
            type: 'text', text: req.__('INVALID_CHOICE_ENTERED')
        };

        lineMessages.unshift(errorMessage);
        return lineMessages;
    },
    _getNextLevel: (choice) => {
        if(choice.dest_type === 'country'){
            return 3;
        } else {
            return 5;
        }
    }
};


// ------------------------------------------
// Message: "You've chosen a location which is a country"
// Input: city name from country
// Validation: Call Autocomplete API, return no country. Check if entry selected is a country or not
// Extra Step: set UserData
// ------------------------------------------
hotelBookingTree[3] = {
    currentLevel: 3,
    onValidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_3_VALID).send();

        debug('[VALID] LEVEL %s VALID RESPONSE ACTION. Calling Autocomplete API', this.currentLevel);

        let promise = apiHelpers.getLocationAutocomplete({
            text: event.message.text,
            languagecode: req.getLocale(),
            filter: { dest_type: ['airport', 'landmark', 'country', 'hotel'] }
        });

        promise.then((suggestions) => {
            if(suggestions.length !== 0) {
                suggestions = locationHelpers.locationInput.filterSuggestionsWithSameCity(suggestions);

                if(suggestions.length === 1){
                    this._onObviousAssumptionAction(req, res, storage, suggestions[0]);
                } else {
                    this._onSuggestionsAvailableAction(req, res, storage, suggestions);
                }

            } else {
                req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_3_VALID_2).send();

                debug('[VALID] LEVEL %s INVALID ACTION. API returned empty', this.currentLevel);
                this.onInvalidResponseAction(event, storage, userId, req, res);
            }
        }).catch((err) => {
            debug('[ERROR] LEVEL %s, AutocompleteApi returned error. ERROR: %j', this.currentLevel, err);
        });
    },
    onInvalidResponseAction: function(event, storage, userId, req, res){
        let lineMessages = this._getErrorMessages(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    isValidResponse: function(req, res, userData){
        return true;
    },
    _onSuggestionsAvailableAction: (req, res, storage, suggestions) => {
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_3_VALID_1).send();

        locationHelpers.locationInput.onSuggestionsAvailableAction(req, res, storage, 4, suggestions);
    },
    _onObviousAssumptionAction: function(req, res, storage, choice){
        let event = req.body.events[0];
        let userId = event.source.userId;

        storage.setUserChoice(userId, 4, choice);

        let lineMessages = hotelBookingTree[5]._getLineMessages(event, storage, userId, req, res);
        let selectedMessages = chatHelpers.getLocationSelectedMessage(req, res, choice);
        lineMessages.unshift(selectedMessages);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
        storage.setConversationLevel(userId, 5);
    },
    _getPendingMessages: function(event, storage, userId, req, res) {
        return [
            {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.API_AUTOCOMPLETE_PENDING_MESSAGE')
            }
        ];
    },
    _getRelevantChoice: (userData) => {
        return userData.choice[2];
    },
    _getLineMessages: function(event, storage, userId, req, res){
        let userData = storage.getUserData(userId);
        let choice = this._getRelevantChoice(userData);

        return [
            {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.COUNTRY_CHOSEN_ASK_CITY_QUESTION', { name: choice.name })
            }
        ];
    },
    _getErrorMessages: function(event, storage, userId, req, res){
        let lineMessages = this._getLineMessages(event, storage, userId, req, res);
        let errorMessage = {
            type: 'text', text: req.__('INVALID_CHOICE_ENTERED')
        };

        lineMessages.unshift(errorMessage);
        return lineMessages;
    },
    _getNextLevel: () => { return 4; }
};


// ------------------------------------------
// Message: Carousel with suggestions without country
// Input: Any of the choices from Lv3
// Validation: none
// Extra Step: set UserData
// ------------------------------------------
hotelBookingTree[4] = {
    currentLevel: 4,
    onValidResponseAction: function(event, storage, userId, req, res){
        if(!locationHelpers.locationCarousel.isNoneOfTheAbove(event.message.text, req)){
            req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_4_VALID).send();

            let userData = storage.getUserData(userId);
            let suggestion = userData.suggestion[this.currentLevel];
            let choice = this._getChoiceFromSuggestions(event.message.text, suggestion);

            debug('[VALID] LEVEL %s VALID RESPONSE ACTION. Sending carousel of locations with country', this.currentLevel);

            storage.setUserChoice(userId, this.currentLevel, choice);
            storage.setConversationLevel(userId, 5);

            let lineMessages = hotelBookingTree[5]._getLineMessages(event, storage, userId, req, res);
            chatHelpers.sendReplyMessage(req, res, lineMessages);
        } else {
            this._onNoneOfSuggestionAction(req, res, storage);
        }
    },
    onInvalidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_4_INVALID).send();

        let lineMessages = this._getErrorMessages(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    isValidResponse: function(req, res, userData){
        let event = req.body.events[0];
        let suggestions = userData.suggestion[this.currentLevel];
        return _.some(suggestions, { label: event.message.text }) || locationHelpers.locationCarousel.isNoneOfTheAbove(event.message.text, req);
    },
    _onNoneOfSuggestionAction: function(req, res, storage) {
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_4_VALID_1).send();
        let event = req.body.events[0];
        let userId = event.source.userId;
        let previousLevel = 3;
        let lineMessages = hotelBookingTree[previousLevel]._getLineMessages(event, storage, userId, req, res);

        locationHelpers.locationCarousel.onNoneOfSuggestionAction(req, res, storage, previousLevel, lineMessages);
    },
    _getCityNameChoice: function(userData) {
        return userData.choice[1];
    },
    _getLineMessages: function(event, storage, userId, req, res){
        let userData = storage.getUserData(userId);
        let currentSuggestion = userData.suggestion[this.currentLevel];
        let choice = this._getCityNameChoice(userData);
        let messages = chatHelpers.locationAutocomplete.getMessage(currentSuggestion, req);

        messages.unshift({
            type: 'text',
            text: req.__('HOTEL_BOOK_TREE.LOCATION_AUTOCOMPLETE_RESULT_SEARCH_MESSAGE', {text: choice})
        });

        return messages;
    },
    _getErrorMessages: function(event, storage, userId, req, res){
        let lineMessages = this._getLineMessages(event, storage, userId, req, res);
        let errorMessage = {
            type: 'text',
            text: req.__('INVALID_CHOICE_ENTERED')
        };

        lineMessages.unshift(errorMessage);
        return lineMessages;
    },
    _getChoiceFromSuggestions: function(text, suggestion){
        return _.find(suggestion, {label : text});
    },
    _getNextLevel: () => { return 5; }
};


// ------------------------------------------
// Message: "Which dates do you want to go?"
// Input: date format
// Validation: Validate the date from the series of validation stuff
// Extra Step: set UserData
// ------------------------------------------
hotelBookingTree[5] = {
    currentLevel: 5,
    onValidResponseAction: function(event, storage, userId, req, res){
        let nlpRequest = apiaiHelpers.getTextRequest(userId, event.message.text);

        nlpRequest.then((response) => {
            debug('[API.AI SUCCESS] nlp for dates: %j', response);

            let param = response.result.parameters;
            let checkin_month = param.checkin_month || param.checkout_month;
            let checkout_month = param.checkout_month || param.checkin_month;
            let sCheckin = `${param.checkin_day}/${checkin_month}`;
            let sCheckout = `${param.checkout_day}/${checkout_month}`;

            if(this._isValidDates(sCheckin, sCheckout)) {
                req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_5_VALID).send();

                let choice = this._getDatesArrayForStore(sCheckin, sCheckout);
                debug('[VALID] LEVEL %s VALID RESPONSE ACTION. Sending question for check in and checkout', this.currentLevel);

                storage.setUserChoice(userId, this.currentLevel, choice);
                storage.setConversationLevel(userId, 7);

                let lineMessage =  this._getSelectedDatesMessage(req, res, storage.getUserData(userId));
                chatHelpers.pushMessage(req, res, [lineMessage]);
                hotelBookingTree[6].onShowDealsResponse(event, storage, userId, req, res);

            } else {
                this.onInvalidResponseAction(event, storage, userId, req, res);
            }
        })
        .catch((err) => {
            debug('[API.AI FAILURE] nlp for dates: %j', err);
            console.log(err);
        });
    },
    onInvalidResponseAction: function(event, storage, userId, req, res){
        debug('[INVALID] LEVEL %s INVALID RESPONSE ACTION. Sending error and trying again', this.currentLevel);
        let lineMessages = this._getErrorMessages(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    isValidResponse: function(req, res, userData){
        return true;
    },
    _isValidDates: function(pCheckin, pCheckout){
        let checkin = dateHelpers._getMoment(pCheckin);
        let checkout = dateHelpers._getMoment(pCheckout);
        let isValidDates = dateHelpers.isValidDates(checkin, checkout);
        let isCheckinBeforeCheckout = dateHelpers.isCheckinBeforeCheckout(checkin, checkout);
        let isDurationValid = dateHelpers.isDurationValid(checkin, checkout);
        let isInPast = dateHelpers.isInPast(checkin, checkout);

        return isValidDates && isCheckinBeforeCheckout && isDurationValid && !isInPast;
    },
    _getSelectedDatesMessage: (req, res, userData) => {
        let i = moment(userData.choice[5][0]).locale(userData.locale);
        let checkinStr = i.format('LL');
        let o = moment(userData.choice[5][1]).locale(userData.locale);
        let checkoutStr = o.format('LL');
        let duration = o.diff(i, 'days');

        return lineHelpers.getMessageText(
            req.__mf('DATES_SELECTED_MESSAGE', {
                checkin: checkinStr, checkout: checkoutStr, duration: duration
            })
        )
    },
    _getLineMessages: (event, storage, userId, req, res) => {
        return [
            {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.CHECKIN_CHECKOUT_DATE_QUESTION')
            }
        ];
    },
    _getErrorMessages: function(event, storage, userId, req, res){
        let lineMessages = this._getLineMessages(event, storage, userId, req, res);
        let errorMsg;

        let arr = event.message.text.split('-');
        let checkin = dateHelpers.getCheckInDate(arr);
        let checkout = dateHelpers.getCheckoutDate(arr);

        if(!dateHelpers.isValidDates(checkin, checkout)){
            req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_5_INVALID_1).send();

            errorMsg = {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.DATE_INVALID_MESSAGE')
            };
        } else

        if(!dateHelpers.isCheckinBeforeCheckout(checkin, checkout)){
            req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_5_INVALID_2).send();

            errorMsg = {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.CHECKOUT_BEFORE_CHECKIN_INVALID_MESSAGE')
            };
        } else

        if(!dateHelpers.isDurationValid(checkin, checkout)){
            req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_5_INVALID_3).send();

            errorMsg = {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.STAY_DURATION_INVALID_MESSAGE')
            };
        } else

        if(dateHelpers.isInPast(checkin, checkout)){
            req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_5_INVALID_4).send();

            errorMsg = {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.DATE_INVALID_MESSAGE')
            }
        }

        lineMessages.unshift(errorMsg);
        return lineMessages;
    },
    _getDatesArrayForStore: function(pCheckin, pCheckout) {
        let checkin = dateHelpers._getMoment(pCheckin);
        let checkout = dateHelpers._getMoment(pCheckout);

        if (checkin.isBefore(moment(), 'month')) {
            checkin.add(1, 'year'); checkout.add(1, 'year');
        }

        return [checkin.format('YYYY-MM-DD'), checkout.format('YYYY-MM-DD')];
    }
};


// ------------------------------------------
// Message: "Which do you prioritize?"
// Input: any of the choices from the list
// Validation: Validate the date from the series of validation stuff
// Extra Step: set UserData
// ------------------------------------------
hotelBookingTree[6] = { //UNUSED
    currentLevel: 6,
    onValidResponseAction: function(event, storage, userId, req, res) {},
    onInvalidResponseAction: function(event, storage, userId, req, res) {
        req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_6_INVALID).send();

        debug('[INVALID] LEVEL %s INVALID RESPONSE ACTION. Sending error message, and then asking again', this.currentLevel);

        let lineMessages = this._getErrorMessages(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    isValidResponse:  function(req, res, userData){
        let options = [
            req.__('HOTEL_BOOK_TREE.SHOW_BY_BUDGET_MESSAGE'),
            req.__('HOTEL_BOOK_TREE.SHOW_DEALS_MESSAGE')
        ];

        return _.includes(options, req.body.events[0].message.text);
    },
    onShowDealsResponse: function(event, storage, userId, req, res) {
        let _onNoDealsResponseAction = function(event, storage, userIds, req, res) {
            debug('LEVEL %s No Deals Available Action. Sending error message, and then asking again', this.currentLevel);

            let lineMessages = this._getNoDealsErrorMessages(event, storage, userId, req, res);
            chatHelpers.sendReplyMessage(req, res, lineMessages);
        };

        debug('LEVEL %s VALID RESPONSE ACTION. Sending deals carousel', this.currentLevel);

        let userData = storage.getUserData(userId);
        let promise = apiHelpers.getHotelAvailabilityForDeals({
            checkin: userData.choice[5][0],
            checkout: userData.choice[5][1],
            dest_type: _getCorrectLocationChoice(userData)['dest_type'],
            dest_id:  _getCorrectLocationChoice(userData)['dest_id'],
            currency_code: 'THB',
            lang: req.getLocale()
        });
        let pendingMessages = this._getPendingMessages(event, storage, userId, req, res);
        chatHelpers.pushMessage(req, res, pendingMessages);

        promise.then((response) => {
            if(response.hotels.length !== 0) {
                debug('LEVEL %s Deals are available for the search', this.currentLevel);

                storage.setUserSuggestion(userId, this.currentLevel, response);
                storage.setConversationLevel(userId, 7);

                hotelBookingTree[7]._getLineMessages(event, storage, userId, req, res)
                    .then((lineMessages) => {
                        chatHelpers.sendReplyMessage(req, res, lineMessages);
                    });
            } else {
                this._onNoDealsResponseAction(event, storage, userId, req, res);
            }
        }).catch((err) => {
            debug('[ERROR] LEVEL %s, ERROR Calling Deals Availability API', this.currentLevel);
        });
    },
    _onNoDealsResponseAction: function(event, storage, userId, req, res) {
        debug('LEVEL %s No Deals Available Action. Sending error message, and then asking again', this.currentLevel);

        let lineMessages = this._getNoDealsErrorMessages(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    _getLineMessages: (event, storage, userId, req, res) => {
        return [
            {
                "type": "template",
                "altText": req.__('HOTEL_BOOK_TREE.FILTERS_QUESTION_ALTEXT_140CH'),
                "template": {
                    "type": "buttons",
                    "title": req.__('HOTEL_BOOK_TREE.FILTERS_QUESTION_TITLE_40CH'),
                    "text": req.__('HOTEL_BOOK_TREE.FILTERS_QUESTION_TEXT_60CH'),
                    "actions": [
                        {
                            "type": "message",
                            "label": req.__('HOTEL_BOOK_TREE.SHOW_DEALS_LABEL_20CH'),
                            "text": req.__('HOTEL_BOOK_TREE.SHOW_DEALS_MESSAGE')
                        },
                        {
                            "type": "message",
                            "label": req.__('HOTEL_BOOK_TREE.SHOW_BY_BUDGET_LABEL_20CH'),
                            "text": req.__('HOTEL_BOOK_TREE.SHOW_BY_BUDGET_MESSAGE')
                        }
                    ]
                }
            }
        ];
    },
    _getPendingMessages: (event, storage, userId, req, res) => {
        return [
            {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.API_DEALS_PENDING_MESSAGE')
            }
        ];
    },
    _getNextLevel: function(text, req){
        if(text === req.__('HOTEL_BOOK_TREE.SHOW_DEALS_MESSAGE')) return 7;
        return 8;
    },
    _getNoDealsErrorMessages: function(event, storage, userId, req, res){
        let userData = storage.getUserData(userId);
        let lineMessages = this._getLineMessages(event, storage, userId, req, res);
        let srUrl = chatHelpers.getSrUrlWithDates(userData);

        let errorMessage = {
            type: 'template',
            altText: req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_NO_DEALS_MESSAGE_160CH'),
            template: {
                type: 'buttons',
                text: req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_NO_DEALS_MESSAGE_160CH'),
                actions: [
                    {
                        type: 'uri',
                        label: 'See more hotels',
                        uri: srUrl
                    }
                ]
            }
        };

        lineMessages.unshift(errorMessage);
        return lineMessages;
    },
    _getErrorMessages: function(event, storage, userId, req, res){
        let lineMessages = this._getLineMessages(event, storage, userId, req, res);

        let errorMessage = {
            type: 'text',
            text: req.__('INVALID_CHOICE_ENTERED')
        };

        lineMessages.unshift(errorMessage);
        return lineMessages;
    }
};


// ------------------------------------------
// Message: Carousel of deals from Booking.com
// Input:
// Validation: Validate the date from the series of validation stuff
// Extra Step: set UserData
// ------------------------------------------
hotelBookingTree[7] = {
    currentLevel: 7,
    isValidResponse: (req, res, userData) => {
        return req.body.events[0].message.text === req.__('HOTEL_BOOK_TREE.BUDGET_MESSAGE_SELECT_LABEL_ACTIONTEXT');
    },
    onValidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_7_VALID).send();
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_SEARCH_AGAIN).send();

        let lineMessages = hotelBookingTree[8]._getLineMessages(event, storage, userId, req, res);
        storage.setConversationLevel(userId, 8);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    onInvalidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_7_INVALID).send();

        let lineMessages = [this._getBudgetMessage(event, storage, userId, req, res)];
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    _getLineMessages: function(event, storage, userId, req, res){
        return new Promise((resolve, reject) => {

        let userData = storage.getUserData(userId);
        let suggestions = this._getHotelDealsSuggestions(userData);
            let locationChoice = _getCorrectLocationChoice(userData);

        chatHelpers.deals.getMessage(suggestions, req, locationChoice)
            .then((messages) => {
                messages.push(this._getBudgetMessage(event, storage, userId, req, res));

                resolve(messages);
            });
        });
    },
    _getHotelDealsSuggestions: function(userData){
        return userData.suggestion[6];
    },
    _getBudgetMessage: function(event, storage, userId, req, res){
        return {
            type: 'template',
            altText: req.__('HOTEL_BOOK_TREE.BUDGET_MESSAGE_SELECT_DESCRIPTION'),
            template: {
                type: 'buttons',
                text: req.__('HOTEL_BOOK_TREE.BUDGET_MESSAGE_SELECT_DESCRIPTION'),
                actions: [
                    {
                        type: 'message',
                        label: req.__('HOTEL_BOOK_TREE.BUDGET_MESSAGE_SELECT_LABEL_20CH'),
                        text: req.__('HOTEL_BOOK_TREE.BUDGET_MESSAGE_SELECT_LABEL_ACTIONTEXT')
                    }
                ]
            }
        }
    },
    _getErrorMessages: () => {}
};


// ------------------------------------------
// Message: "Whats your budget range?" (List of budget ranges)
// Input: any of the choices from the list
// Validation: Validate that the text matches any of the list
// Extra Step: set UserData
// ------------------------------------------
hotelBookingTree[8] = {
    currentLevel: 8,
    onValidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_8_VALID).send();

        let userData = storage.getUserData(userId);
        let choice = this._getChoice(event.message.text, req);
        let tier = CONSTANTS.BUDGET_TIERS[choice];
        let duration = this._getDurationOfStay(userData.choice[5][0], userData.choice[5][1]);

        storage.setUserChoice(userId, this.currentLevel, choice);

        let totalMin = tier.MIN * duration;
        let totalMax = tier.MAX ? tier.MAX * duration : null;

        let opts = {
            totalMin: totalMin, totalMax: totalMax,
            checkin: userData.choice[5][0], checkout: userData.choice[5][1],
            dest_type:  _getCorrectLocationChoice(userData)['dest_type'],
            dest_id:  _getCorrectLocationChoice(userData)['dest_id'],
            limit: 4, currencyCode: "THB"
        };

        debug('[VALID] LEVEL %s VALID RESPONSE ACTION. Calling API, computing min and max price. Options for call as follows: %O', this.currentLevel, opts);

        let promise = apiHelpers.getHotelAvailabilityForBudget(opts);
        let pendingMessages = this._getPendingMessages(event, storage, userId, req, res);
        chatHelpers.pushMessage(req, res, pendingMessages);

        promise.then((response) => {
            debug('[VALID] LEVEL %s VALID RESPONSE ACTION. Calling Availability API. Returned: %O', this.currentLevel, response);

            if(response.hotels.length !== 0){
                let nextLevel = this._getNextLevel();
                storage.setConversationLevel(userId, nextLevel);
                storage.setUserSuggestion(userId, this.currentLevel, response);

                hotelBookingTree[nextLevel]._getLineMessages(event, storage, userId, req, res)
                    .then((lineMessages) => {
                        chatHelpers.sendReplyMessage(req, res, lineMessages);
                    });
            } else {
                debug('[VALID] LEVEL %s VALID RESPONSE ACTION. Calling Availability API. Returned empty: %O', this.currentLevel, response);
                this._onNoAvailableHotelsForTier(event, storage, userId, req, res);
            }
        }).catch((err) => {
            debug('[ERROR] LEVEL %s, ERROR Calling Availability API. Returned: %O', this.currentLevel, err);
        });

    },
    onInvalidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_8_INVALID).send();

        let lineMessages = this._getErrorMessages(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    isValidResponse: (req, res, userData) => {
        let options = [];

        _.each(CONSTANTS.BUDGET_TIERS, (tier, key) => {
            options.push(
                key !== 'TIER5' ?
                    req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_TITLE_40CH', {min: tier.MIN, max:tier.MAX}) :
                    req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIERMAX_TITLE_40CH', {min: tier.MIN})
            );
        });

        return _.includes(options, req.body.events[0].message.text);
    },
    _onNoAvailableHotelsForTier: function(event, storage, userId, req, res) {
        debug('[INVALID] LEVEL %s INVALID RESPONSE ACTION. No hotels available. Asking again', this.currentLevel);

        let lineMessages = this._getNoHotelsAvailableMessage(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    _getChoice: function(text, req){
        let i = _.findIndex(this._getCarouselColumns(req), { title: text }) + 1;
        return 'TIER'+i.toString();
    },
    _getPendingMessages: (event, storage, userId, req, res) => {
        return [
            {
                type: 'text',
                text: req.__('HOTEL_BOOK_TREE.API_BUDGET_TIER_PENDING_MESSAGE')
            }
        ];
    },
    _getDurationOfStay: function(checkinDate, checkoutDate) {
        let checkin = moment(checkinDate, 'YYYY-MM-DD');
        let checkout = moment(checkoutDate, 'YYYY-MM-DD');

        return checkout.diff(checkin, 'days');
    },
    _getCarouselColumns: function(req) {
        let columns = [];

        _.each(CONSTANTS.BUDGET_TIERS, (tier, key) => {
            let tierTitleText = key !== 'TIER5' ?
                req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_TITLE_40CH', {min: tier.MIN, max:tier.MAX})
                : req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIERMAX_TITLE_40CH', {min: tier.MIN});

            let tierMsgText = key !== 'TIER5' ?
                req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_TITLE_40CH', {min: tier.MIN, max:tier.MAX})
                : req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIERMAX_TITLE_40CH', {min: tier.MIN});

            columns.push({
                title: tierTitleText,
                text: req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_TEXT_60CH'),
                actions: [
                    {
                        type: 'message',
                        label: req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_SELECT_LABEL_20CH'),
                        text: tierMsgText
                    }
                ]
            });
        });

        return columns;
    },
    _getLineMessages: function(event, storage, userId, req, res){
        let carousel = {
            "type": "template",
            "altText": req.__('HOTEL_BOOK_TREE.BUDGET_CAROUSEL_TIER_ALTEXT_140CH'),
            "template": {
                "type": "carousel",
                "columns": this._getCarouselColumns(req)
            }
        };

        let initialQuestion = {
            type: 'text',
            text: req.__('HOTEL_BOOK_TREE.BUDGET_MESSAGE_PLEASE_SELECT_TEXT')
        };

        return [initialQuestion, carousel];
    },
    _getNoHotelsAvailableMessage: function(event, storage, userId, req, res) {
        let lineMessages = this._getLineMessages(event, storage, userId, req, res);
        let userData = storage.getUserData(userId);
        let srUrl = chatHelpers.getSrUrlWithDates(userData);

        let errorMessage = {
            type: 'template',
            altText: req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_NO_HOTELS_IN_TIER_160CH'),
            template: {
                type: 'buttons',
                text: req.__('HOTEL_BOOK_TREE.HOTEL_CAROUSEL_AVAILABILITY_NO_HOTELS_IN_TIER_160CH'),
                actions: [
                    {
                        type: 'uri',
                        label: req.__('HOTEL_BOOK_TREE.SEE_MORE_HOTEL_LABEL_20CH'),
                        uri: srUrl
                    }
                ]
            }
        };

        lineMessages.unshift(errorMessage);
        return lineMessages;
    },
    _getErrorMessages: function(event, storage, userId, req, res){
        let lineMessages = this._getLineMessages(event, storage, userId , req, res);

        let errorMessage = {
            type: 'text',
            text: req.__('INVALID_CHOICE_ENTERED')
        };

        lineMessages.unshift(errorMessage);
        return lineMessages;
    },
    _getNextLevel: () => { return 9 }
};


// ------------------------------------------
// Message: Hotel Carousel for budget (Hotels under the budget)
// Input: any
// Validation: none
// Extra Step: set UserData
// ------------------------------------------
hotelBookingTree[9] = {
    currentLevel: 9,
    isValidResponse: (req, res, userData) => {
        return req.body.events[0].message.text === req.__('HOTEL_BOOK_TREE.SEARCH_AGAIN_ACTION_TEXT_300CH');
    },
    onValidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_9_VALID).send();
        req.visitor.event('chat trigger', 'valid', gaMapping.HOTEL_BOOK_SEARCH_AGAIN).send();

        storage.setConversationLevel(userId, 1);
        storage.removeUserChoices(userId);

        let lineMessages = hotelBookingTree[1]._getLineMessages(event, storage, userId, req, res);
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    onInvalidResponseAction: function(event, storage, userId, req, res){
        req.visitor.event('chat trigger', 'invalid', gaMapping.HOTEL_BOOK_9_INVALID).send();

        let lineMessages = [this._getThankYouMessage(event, storage, userId, req, res)];
        chatHelpers.sendReplyMessage(req, res, lineMessages);
    },
    _getLineMessages: function(event, storage, userId, req, res){
        return new Promise((resolve, reject) => {
            let userData = storage.getUserData(userId);
            let selectedTier = userData.choice[8];
            let response = this._getHotelsBudgetSuggestions(userData);
            let locationChoice = _getCorrectLocationChoice(userData);

            chatHelpers.budget.getMessage(response, selectedTier, req, locationChoice)
                .then((messages) => {
                    let i = moment(response.checkin), checkinStr = i.format('LL');
                    let o = moment(response.checkout), checkoutStr = o.format('LL');
                    let duration = o.diff(i, 'days');
                    let locationName =  _getCorrectLocationChoice(userData)['name'];
                    let budgetMin = CONSTANTS.BUDGET_TIERS[selectedTier].MIN;
                    let budgetMax = CONSTANTS.BUDGET_TIERS[selectedTier].MAX;

                    let isTier5 = selectedTier === 'TIER5';

                    let otherTierText = req.__mf('HOTEL_BOOK_TREE.BUDGET_MESSAGE_REQUEST_DESCRIPTION_TEXT',{
                        checkin: checkinStr, checkout: checkoutStr,
                        duration: duration, location: locationName,
                        budget_min: budgetMin, budget_max: budgetMax
                    });

                    let tier5Text = req.__mf('HOTEL_BOOK_TREE.BUDGET_MESSAGE_REQUEST_DESCRIPTION_TEXT_TIER5',{
                        checkin: checkinStr, checkout: checkoutStr,
                        duration: duration, location: locationName,
                        budget_min: budgetMin
                    });

                    let text = isTier5 ? tier5Text : otherTierText;
                    messages.unshift({ type: "text", text: text });
                    messages.push(this._getThankYouMessage(event, storage, userId, req, res));

                    resolve(messages);
                });
        });
    },
    _getHotelsBudgetSuggestions: (userData) => {
        return userData.suggestion[8];
    },
    _getThankYouMessage: function(event, storage, userId, req, res){
        return {
            type: 'template',
            altText: req.__('HOTEL_BOOK_TREE.THANK_YOU_MESSAGE'),
            template: {
                type: 'buttons',
                text: req.__('HOTEL_BOOK_TREE.THANK_YOU_MESSAGE'),
                actions: [
                    {
                        type: 'message',
                        label: req.__('HOTEL_BOOK_TREE.SEARCH_AGAIN_LABEL_20CH'),
                        text: req.__('HOTEL_BOOK_TREE.SEARCH_AGAIN_ACTION_TEXT_300CH')
                    }
                ]
            }
        }
    },
    _getErrorMessages: () => {}
};

module.exports = hotelBookingTree;