'use strict';

const debug = require('debug')('linebot:botjs');
const express = require('express');
const compression = require('compression');
const line = require('node-line-bot-api');
const bodyParser = require('body-parser');
const i18n = require('i18n');
const moment = require('moment');
const constants = require('./constants');
const CONFIG = process.env.NODE_ENV === 'production' ? constants['PROD'] : constants['DEV'];
const Storage = require('./Storage');
const timeout = require('connect-timeout');
const bot = express();
const ua = require('universal-analytics');
let storage = new Storage();

const validateAccessToken = require('./middleware/validateAccessToken');
const validateSignature = require('./middleware/validateSignature');
const branchDistribution = require('./middleware/branchDistribution');

const botInitializeLogic = require('./chatLogic/botInitializeLogic');
const apiAutocompleteLogic = require('./apiLogic/apiAutocompleteLogic');
const apiThemesAutocompleteLogic = require('./apiLogic/apiThemesAutocomplete');
const apiGetHotelAvailability = require('./apiLogic/apiGetHotelAvailability');
const apiGetHotelByPriceRange = require('./apiLogic/apiGetHotelByPriceRange');
const apiGetCities = require('./apiLogic/apiGetCities');
const apiGetDistrictHotels = require('./apiLogic/apiGetDistrictHotels');
const apiGetDistricts = require('./apiLogic/apiGetDistricts');
const apiGetImageByDestinationTag = require('./apiLogic/apiGetImageByDestinationTag');
const apiGetBlockAvailability = require('./apiLogic/apiGetBlockAvailability');

const scraperGetDsfPage = require('./apiLogic/scraper/scraperGetDsfPage');

i18n.configure({
    locales: constants.AVAILABLE_LOCALES,
    defaultLocale: CONFIG.DEFAULT_LOCALE,
    directory: __dirname + '/locales',
    objectNotation: true,
    updateFiles: false
});

bot.use(
    bodyParser.json({
        verify (req, res, buf) {
            req.rawBody = buf;
        }
    })
);

bot.use(timeout('10s'));
bot.use(i18n.init);
bot.use(compression());
bot.use(function timeoutMiddleware(req, res, next){
    if(!req.timedout){ next(); return; }
    debug('request has timedout');
});
bot.use(ua.middleware(CONFIG.GA_UAID, {cookieName: '_ga'}));

bot.get('/', (req, res, next) => { res.status(200).send('LineBot is working'); });

bot.post('/webhook',
    validateAccessToken, validateSignature,
    (req, res, next) => { botInitializeLogic(req, res, next, storage); },
    (req, res, next) => { branchDistribution(req, res, next, storage); });

bot.post('/api/autocomplete/location', apiAutocompleteLogic);
bot.post('/api/autocomplete/themes', apiThemesAutocompleteLogic);
bot.post('/api/getBlockAvailability', apiGetBlockAvailability);
bot.post('/api/getHotelAvailability', apiGetHotelAvailability);
bot.post('/api/getHotelByPriceRange', apiGetHotelByPriceRange);
bot.post('/api/getCities', apiGetCities);
bot.post('/api/getImageByDestinationTag', apiGetImageByDestinationTag);

bot.post('/scraper/getDsfPage', scraperGetDsfPage);
bot.post('/api/getDistrictHotels', apiGetDistrictHotels);
bot.post('/api/getDistricts', apiGetDistricts);

bot.use('/images', express.static('images'));

module.exports = bot;